# Cylinders
This is the application that runs the cylinders website at cylinders.library.ucsb.edu.

### Getting Started

#### Prerequisites
First you will need Docker installed.

If on Mac:
- If you have Homebrew, you can use
`brew cask install docker` or if you don't have Home Brew, you may download Docker using https://www.docker.com/products/docker-desktop

### How to set up a development environment
Before you begin, clone the Cylinders repository to your local drive.
- Make sure Docker is running and that you have your Terminal open in the Cylinders repository folder.
- To start the cylinders website, you can use `docker-compose up`
- Then you can check if it's running on localhost. To do this, type "http://localhost:8888" into your browser.

### Troubleshooting
`docker ps` allows you to see what container processes are running.
- After running this command, you should be able to see something similar to:
```
CONTAINER ID        IMAGE                  COMMAND                  CREATED             STATUS              PORTS                  NAMES
ce21895bbeaf        cylinders_cylinders    "docker-php-entrypoi…"   21 hours ago        Up 14 minutes       0.0.0.0:8888->80/tcp   cylinders-webserver
d7c5afd45038        memcached:1.5-alpine   "docker-entrypoint.s…"   21 hours ago        Up 14 minutes       11211/tcp              cylinders-memcached
```

`docker images` will list any images that were built.
```
REPOSITORY            TAG                 IMAGE ID            CREATED             SIZE
cylinders_cylinders   latest              115a659eb614        21 hours ago        819MB
<none>                <none>              e4669a3aa11f        21 hours ago        819MB
php                   7.3-apache          932f67007b71        7 days ago          410MB
memcached             1.5-alpine          0dbf6b4c454b        5 months ago        9.19MB
```
