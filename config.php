<?php
//filename:config.php

//set the default timezone for use by all php functions
date_default_timezone_set('America/Los_Angeles');

// for development purposes we want to see most errors in the browser
error_reporting(E_ALL & ~E_WARNING);
ini_set('display_errors', '0');

//define server from which Z3950 query results are fetched via YAZ
$z3950_server = "libcat-dev-2476.library.ucsb.edu:9991/sba01pub";

// server from which we make the sitemap requets
define('SITEMAP_API_SERVER', '10.3.1.171');
define('SITEMAP_API_KEY', "02c39976-6dda-11e5-8f09-10ddb1dbc601");

//define constants for MEMCACHED_HOST and MEMCACHED_PORT
//	define('MEMCACHED_HOST', 'memcached1.library.ucsb.edu');  // prod
define('MEMCACHED_HOST', 'localhost');
define('MEMCACHED_PORT', '11211');
?>
