<?php
require_once('config/main.php');
require_once('functions.php');
$query_type = 'call_number';
// calculate time remaining in day which will be used to set expiration of cached data
$seconds_until_end_of_day = strtotime('tomorrow') - time();

/** memcached connection **/
// Establish connection to memcached server
// constants stored in config/main.php
$memcache = new Memcached();
$mcServerAdded = $memcache->addServer(MEMCACHED_HOST, MEMCACHED_PORT);

// check for memcached results
if($mcServerAdded == true){
  // we have a memcached server connection
  //initialize $cylinder_of_day variable
  $cylinder_of_day = NULL;

  //retrieve cached COTD record data if it exists
  $cylinder_of_day = $memcache->get("cylinder_of_day_record");
  // the value of $cylinder_of_day is a big ol XML string of around 5000 chars in length
  if($cylinder_of_day === false){
    // memcache->get returned false indicating that no COTD record found in cache
  if(DEVELOPMENT === true){error_log("nothing stored in memcached with key of cylinder_of_day_record");}
    }else{
  if(DEVELOPMENT === true){error_log("something stored in memcached with key of cylinder_of_day_record");}
    }

  //retrieve cached COTD number
  $cylinder_of_day_number = $memcache->get("cylinder_of_day_number");
  if($cylinder_of_day_number === false){
    // the Cylinder number for the COTD is not cached
  if(DEVELOPMENT === true){error_log("Cylinder of the Day number not cached in memcached");}
    }else{
  if(DEVELOPMENT === true){error_log("Cylinder of the Day number from memcached:". $cylinder_of_day_number);}
  }

}else{
  error_log("unable to connect to memcached");
}

// if we don't have a cached COTD, fetch a new one
if($cylinder_of_day === false){
  /*
  file: COTD.txt lists cylinder numbers
  1 per line.
  */
  $cyl_data = file("data/COTD.txt");
  $numCyls = count($cyl_data);
  $line = trim($cyl_data[mt_rand(0, ($numCyls - 1))]);
  $query_term = $line;
  $query_term_padded = str_pad($query_term, 4, '0', STR_PAD_LEFT);
  $cylinder_of_day = fetch_sru_results(build_sru_query($query_type, $query_term_padded));
 
  //tell Memcached to store $cylinder_of_day data
  $memcache->set("cylinder_of_day_record", $cylinder_of_day, $seconds_until_end_of_day);
  //tell Memcached to store $cylinder_of_day Number
  $memcache->set("cylinder_of_day_number", $query_term_padded, $seconds_until_end_of_day);
  $r_of_day_num = $query_term_padded;
} //end if cylinder_of_day is false 


// If record has been grabbed from memcache or by SRU, generate cylofday
// Check if it's set /and/ true, since if memcache gets corrupted it
// can apparently get set to (bool)false
//$r_of_day = fetch_sru_results(build_sru_query($query_type, $query_term));
//echo ( "gettype(r_of_day()" . gettype($r_of_day)."<br>"); //debug

if(isset($cylinder_of_day) && $cylinder_of_day){
  //create a simplexml Object from the xml returned by SRU
  $cylinder_of_day = str_replace('xmlns=', 'ns=', $cylinder_of_day);// fix for Jira issue DEV-1261
  $xml = simplexml_load_string($cylinder_of_day);

  // Create a prefix/namespace context for the XPath query; necessary
  // to access nodes since the xml from z39.50 has a custom namespace
  $xml->registerXPathNamespace('record', 'http://www.loc.gov/MARC21/slim');

  //prepare xpath queries
  $xquery_title = "descendant::datafield[@tag='245']";
  $xquery_author = "descendant::datafield[@tag='511']";
  $xquery_pubname = "descendant::datafield[@tag='028']";
  $xquery_cylnum = "descendant::datafield[@tag='AVA']";
  $xquery_pubyear = "descendant::datafield[@tag='260']";

  //query simplexml object
  $info_title = $xml->xpath($xquery_title);
  $info_author = $xml->xpath($xquery_author);
  $info_pubname = $xml->xpath($xquery_pubname);
  $info_cylnum = $xml->xpath($xquery_cylnum);
  $info_pubyear = $xml->xpath($xquery_pubyear);

  //assign info to variables for use in template file
  $title = $info_title[0]->subfield[0];
  $author = $info_author[0]->subfield;
  $pubname = $info_pubname[0]->subfield[1];
  //the cylinder number requires a substring of the result
  $cylnum = (string)$info_cylnum[0]->subfield[5];
  $cylnum = substr($cylnum, 9);
  $pubyear = $info_pubyear[0]->subfield[1] . " " . $info_pubyear[0]->subfield[2];

  //assign variables to Smarty
  $smarty->assign('cylofday_title', $title);
  $smarty->assign('cylofday_author', $author);
  $smarty->assign('cylofday_pubname', $pubname);
  $smarty->assign('cylofday_cylnum', $cylnum);
  $smarty->assign('cylofday_pubyear', $pubyear);

} //end if(isset($cylinder_of_day))
