{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: cakewalks.tpl -->
<div class="content text">
  <h1>Cakewalks and Rags</h1>
  <p>
    <a href="thematic_playlists.xml"><img src="/images/rss-podcast.png" alt="Podcast"></a></p>

  <p>
    The cakewalk is a syncopated march-like piece that originated in the
    19th century as a dance performed by black American slaves to parody the
    behavior of their white owners. Ragtime, a similar musical form, juxtaposed
    a syncopated melodic line against a straight, march-like bass line. Cakewalks
    and rags became hugely popular in the 1890s and remained popular through
    World War I. Ragtime originated as an instrumental form, often played
    on banjo or piano. Rag songs, commonly performed by Edward Meeker, Billy
    Murray, and other popular vocalists, share some of the features of ragtime
    such as syncopation, but this playlist  features the more characteristic
    instrumental rags and cakewalks.  </p>
  <p>
    Before the influence of African-American music became widespread, Anglo-American
    popular music was mostly not syncopated and was &#147;square,&#148; like
    a <a href="search.php?nq=1&query_type=call_number&query=3947">church hymn</a>. Today, much of American popular music is syncopated, a feature
    that descends from these early rags and cakewalks. <em>- David Seubert,
    UC Santa Barbara.</em>
  </p>
  <ul>
    <li>
      <span class="small-text"> Eli Green's cake walk / Vess L. Ossman.
      <a href="search.php?nq=1&query_type=call_number&query=4880">Columbia Phonograph Co.: 3856</a>. between 1904 and 1909. </span>
    </li>
    <li>
      <span class="small-text"> Hezekiah cake walk / National Promenade Band.
      <a href="search.php?nq=1&query_type=call_number&query=1026">Edison Blue Amberol: 2836</a>. 1916.</span>
    </li>
    <li>
      <span class="small-text"> Ma rag time baby / Peerless Orchestra.
      <a href="search.php?nq=1&query_type=call_number&query=0054">Edison Record: 700</a>. 1899.</span>
    </li>
    <li>
      <span class="small-text"> A bunch of rags / Vess L. Ossman. <a href="search.php?nq=1&query_type=call_number&query=4882">Columbia Phonograph Co.: 3861</a>. between 1904 and 1909.</span></li>
    <li>
      <span class="small-text">Frozen Bill / New York Military Band.
      <a href="search.php?nq=1&query_type=call_number&query=3640">Edison Standard Record: 10208</a>. 1909.</span>
    </li>
    <li>
      <span class="small-text">Black and white rag / American Symphony Orchestra.
      <a href="search.php?nq=1&query_type=call_number&query=3498">Edison Standard Record: 10047</a>. 1909.</span>
    </li>
    <li>
      <span class="small-text">Down home rag / Van Eps Trio.
      <a href="search.php?nq=1&query_type=call_number&query=0849">Edison Blue Amberol: 2377</a>. 1914.</span>
    </li>
    <li>
      <span class="small-text">American cake walk / John J. Kimmel.
      <a href="search.php?nq=1&query_type=call_number&query=3104">Edison Gold Moulded Record: 9341</a>. 1906.</span>
    </li>
    <li>
      <span class="small-text">Gondolier and temptation rags / Fred Van Eps and Albert Benzler.
      <a href="search.php?nq=1&query_type=call_number&query=5171">U.S. Everlasting Record: 1260</a>. 1909.</span>
    </li>
    <li>
      <span class="small-text">The international cakewalk / Fred Van Eps.
      <a href="search.php?nq=1&query_type=call_number&query=2684">Edison Gold Moulded Record: 8236</a>. circa 1902.</span>
    </li>
    <li>
      <span class="small-text">Georgia camp-meeting / Edison Military Band.
      <a href="search.php?nq=1&query_type=call_number&query=2414">Edison Gold Moulded Record: 122</a>. circa 1902.</span>    </li>
    <li>
      <span class="small-text">Peaceful Henry / Edison Concert Band.
      <a href="search.php?nq=1&query_type=call_number&query=2707">Edison Gold Moulded Record: 8562</a>. 1903.</span>
    </li>
  </ul>
</div><!-- end .content -->
<!-- End of file: cakewalks.tpl -->
{include file="footer.tpl"}
