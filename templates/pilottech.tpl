{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: pilottech.tpl -->
<div class="content text">
      <img src="/images/station.jpg" class="pure-img"  alt="Digital Audio Workstation">
<h1>
      Digitizing the Cylinders</h1>

<p>In order to transfer the cylinders to digital format and
subsequently the web, the Library's Archeophone, 
a specially made machine designed by Henri Chamoux, was used. The
Archeophone is a universal cylinder player that uses electrical
reproduction and modern styli (such as Stanton or Shure cartridges)
to play back any of the varieties of cylinders made including 2 and 4
minute standard, intermediate and concert cylinders. The Archeophone
allows for very fine control over the playback speed, correction of
eccentricity, playback at half speed, and when used concurrently with
the computer program Sound Forge (or another digital audio editing
software program), transferring a cylinder to the digital .wav format
is usually straightforward and quick. Correcting eccentricity is the
most time consuming portion of the process. All cylinders were played
back on the Archeophone, using either a two minute stylus or a four
minute stylus as appropriate. Cylinders were played at standard Edison 
speeds, typically 160 for black wax and Blue Amberol cylinders. Brown wax
cylinders were pitched by ear and usually ended up being recorded at 140rpm. 
Actual cylinder recording speeds may vary and thus our 
playback speeds may not be entirely accurate. However, attempting
to correctly pitch each individual cylinder would not only be incredibly 
time consuming, but would probably not result in a greater percentage of accurately 
pitched cylinders. Each cylinder was captured as a .wav
file on the library's Digital Audio Workstation. Some transfers sound
better than others, but after consulting with Chamoux we feel that a
wider variety of styluses to match with individual cylinders would be
helpful.</p>

<h4>Compressing the Audio for Web Delivery</h4>

<p>After recording the cylinders as a .wav file, we chose a set of
standard file formats that would satisfy the need for durability and
posterity, ubiquity and public accessibility in making the files
available over the web. While we could deliver uncompressed audio
over the web, with modern compression technology there is really no
need. Selecting a compression codec (a codec, or COmpression
DECompression algorithm reduces the size of files) was initially
somewhat daunting, due to the great number of audio file formats and
compression codecs in existence and the potential of the technologies
becoming obsolete in a few years. After careful consideration, we
chose two file formats for web delivery of audio--mp3 and Quicktime.
We ruled out Realmedia and Windows Media for two reasons: both
require streaming servers to deliver streaming content on the web and
the files can't be converted back to editable formats. Streaming
servers are necessary for projects with many simultaneous users such
as streaming audio course reserves or webcasting of radio which may
have dozens or hundreds of simultaneous users. For delivery of
digitized audio in a library context with a few users accessing many
different files at different times, we believe a streaming server
would be an added expense and maintenance issue and it would be
unlikely that hundreds of people would be listening to these files
simultaneously. Windows Media and Realmedia also are not fully
compatible across platforms like Quicktime and the files can't be
"decoded" back into files like .wav or other editable files. While
Microsoft and others do this to protect copyright holders, we felt it
was unacceptable for a project using public domain material where
people have a right to do whatever they want with the audio.
Quicktime also offers the use of a streaming server, but also allows
progressive download, where the file begins to play as soon as a
portion of the file has begun to download. We chose the progressive
download format and in most cases it will give users the appearance
of streaming, though there is no streaming server to manage the flow
in the case of network problems which Realserver and Quicktime server
both do. With progressive download Quicktime files, the only server
needed is a http server (a regular web server) which the library
already maintains.</p>

<p><img class="pure-img"  src="/images/compressing.jpg" alt="Compressing audio files">
   We
wanted a downloadable file that people could load onto their
computers, and mp3 was chosen for its near ubiquity for distribution
of non-streamed audio. For streamed audio we tested 7 different
compression codecs. As it turned out, the mp3 compression codec
turned out to be by far the best compromise between file size and
quality for streaming audio as well. As a result, we also chose mp3
compression for the Quicktime files. In the process of testing we
made about 250 test files at different bit rates, sampling rates, and
with different codecs. Some of the more revealing examples from our
tests concerning the various compression codecs are offered below. We
were surprised that some of the highly lauded codecs such as the
QDesign codec, did not handle the noise inherent in the cylinder
recordings and produced unacceptable artifacts in the compressed
audio.</p>

<p>In compressing the files from .wav format we have used Cleaner 5
software to compress the files. For mp3 file compression we have
chosen the Fraunhoefer mp3 Codec at a sampling rate of 22.5 kHz with
a data rate of 56 kbits/sec. Through extensive comparison, we found
that there was nothing to be gained by using higher sampling rate
(cylinders just don't contain any musical information over 11Khz) and
the 56 kbits/sec data rate produced a file of acceptable quality.
Higher bit rates such as 96, 128 or even 256 which are clearly
beneficial when encoding modern stereo recordings proved to be of no
value in encoding the cylinder recordings.</p>

<p>For the streaming audio format, two Quicktime file format options
were made for those who want instantaneous playback: a larger,
higher-quality broadband version in Quicktime format using an mp3
codec at a sampling rate of 22.5 kHz with a data rate of 56 kbit/sec
(the same as the mp3 format file), and a smaller, lower-quality 56k
modem dial-up connection version in Quicktime format using an mp3
codec at a sampling rate of 16 kHz at a data rate of 20 kbits/sec. We
found this file would stream over a 56K modem connection, with fairly
good quality.</p>

<p>Additionally, we set up Cleaner 5 to normalize the audio at 96%,
and encode them on the slowest setting even though we could detect no
difference between the slower and faster encoding of the files. The
files were encoded as batches overnight so we didn't care if the
computer took longer to compress the files! Maybe somebody else could
tell the difference even if we couldn't. However, there was a
noticeable decrease in quality using the fastest compression
speed.</p>

<h4>Sound Quality</h4>

<p>The listener may find the sound quality on some of the cylinders
to be quite poor at times, especially with the brown wax-cylinders.
Although we have tried to select both interesting and high-quality
cylinders, it is important to keep in mind that in comparison to
modern mediums like LPs or CDs, or even electrically recorded 78s,
the cylinder format had its limitations and furthermore, time has not
always been kind to many cylinders. Certainly, there are a variety of
noise reduction methods, both hardware and software, but we found
that when applying them to our cylinder collection, it not only
reduced the background noise and hiss, but took out some of the music
as well and in some cases left artifacts. Therefore, the cylinders
presented are essentially unaltered from their original sound. If we
had access to Cedar noise reduction equipment or another system of
sufficiently high quality, we would have made at least one cleaned up
and de-noised file for web delivery.</p>

<img src="/images/cataloging.jpg" class="pure-img"  alt="Cataloged Cylinders">

<h4>Cataloging</h4>

<p>Without cataloging the collection, there is no point in digitizing
it. People need to have an access point to the bibliographic
information on the cylinders. Item level cataloging is an important
component of UCSB's program for managing our collections of historic
audio recordings. Syracuse University's Belfer Audio Archive has
cataloged many of their cylinders and while there is overlap between
the collections, a large percentage of our collection is unique. We
used and expanded on their source copy whenever possible. We upgrade
cataloging to the standards set for cataloging of historical sounds
recordings at UCSB, including complete tracings for composers and
performers, uniform titles when necessary and subject headings. All
cylinders digitized for this project are cataloged in Pegasus,
the UCSB Libraries' online catalog. In the advanced search mode, the
patron can limit their search to just cylinder recordings. </p>

<p>Additionally, links are provided in the catalog record which take
the library patron directly to the audio file. One of the largest
deterrents to using a collection of historical sound recordings such
as this is that because of the fragility of the media, patrons do not
handle the recordings and library staff have to make a copy of each
requested item. For all but the most dedicated or desperate
researchers, this is usually too much work. Library patrons are
increasingly becoming used to online journals, e-books and electronic
indexes. To have to use a physical object, let alone one that may
require a wait of several days to be copied and access, is becoming
too much for the patron who simply wants to hear a particular style
of music, or an old pop song, or an example of early 20th century
performance practice.</p>

<hr>


<h4>Selected samples of audio files compressed Cleaner 5</h4>

<p>Test of compression codecs available in Quicktime</p>

<ul>
   <li>Mace 3:1 compression (1747K) <a href="pilot/tests/CUSB-Cyl0008-MACE31.mov">CUSB-Cyl0008-MACE31.mov</a></li>
   <li>Mace 6:1 compression (875K) <a href="pilot/tests/CUSB-Cyl0008-MACE61.mov">CUSB-Cyl0008-MACE61.mov</a></li>
   <li>Mp3 22Khz sampling rate, 56 kbits (1420K) <a href="pilot/tests/CUSB-Cyl0008-mp3.mov">CUSB-Cyl0008-mp3.mov</a></li>
   <li>Q design music 22 KHz 48 kbits/sec (595K) <a href="pilot/tests/CUSB-Cyl0008-QD1.mov">CUSB-Cyl0008-QD1.mov</a></li>
   <li>Q design music 2 - 22 KHz 48 kbits/sec (1422K) <a href="pilot/tests/CUSB-Cyl0008-QD2.mov">CUSB-Cyl0008-QD2.mov</a></li>
   <li>Qualcomm Pure Voice (1147K) <a href="pilot/tests/CUSB-Cyl0008-QPV.mov">CUSB-Cyl0008-QPV.mov</a></li>
   <li>U law (5235K) <a href="pilot/tests/CUSB-Cyl0008-ULaw.mov">CUSB-Cyl0008-ULaw.mov</a></li>
   <li>A law (5235K) <a href="pilot/tests/CUSB-Cyl0008-ALaw.mov">CUSB-Cyl0008-ALaw.mov</a></li>
   <li>IMA (2782K) <a href="pilot/tests/CUSB-Cyl0008-IMA.mov">CUSB-Cyl0008-IMA.mov</a>	</li>
</ul>
<p>Test of mp3 at various sampling and bit rates</p>
<ul>
   <li>44Khz mono, 96 kbit/sec (2731K) <a href="pilot/tests/16/CUSB-CYL0016a-MT1a.mp3">CUSB-CYL0016a-MT1.mp3</a></li>
   <li>44Khz mono, 96 kbit/sec 11 Khz low pass (2731K) <a href="pilot/tests/16/CUSB-CYL0016a-MT1d.mp3">CUSB-CYL0016a-MT1c.mp3</a></li>
   <li>44Khz mono, 96 kbit/sec 15 Khz low pass (2731K) <a href="pilot/tests/16/CUSB-CYL0016a-MT1b.mp3">CUSB-CYL0016a-MT1d.mp3</a></li>
   <li>44Khz mono, 128 kbit/sec (3638K) <a href="pilot/tests/16/CUSB-CYL0016a-MT2.mp3">CUSB-CYL0016a-MT2.mp3</a></li>
   <li>44Khz mono, 128 kbit/sec 11 Khz low pass (3638K) <a href="pilot/tests/16/CUSB-CYL0016a-MT2c.mp3">CUSB-CYL0016a-MT2c.mp3</a></li>
   <li>44Khz mono, 128 kbit/sec 15 Khz low pass (3638K) <a href="pilot/tests/16/CUSB-CYL0016a-MT2d.mp3">CUSB-CYL0016a-MT2d.mp3</a></li>
   <li>44Khz mono, 192 kbit/sec (5461K) <a href="pilot/tests/16/CUSB-CYL0016a-MT3.mp3">CUSB-CYL0016a-MT3.mp3</a></li>
   <li>44Khz mono, 192 kbit/sec 11 Khz low pass (5461K) <a href="pilot/tests/16/CUSB-CYL0016a-MT3c.mp3">CUSB-CYL0016a-MT3c.mp3</a></li>
   <li>44Khz mono, 192 kbit/sec 15 Khz low pass (5461K) <a href="pilot/tests/16/CUSB-CYL0016a-MT3d.mp3">CUSB-CYL0016a-MT3d.mp3</a></li>
   <li>16 Khz mono, 24 kbit/sec (700K) <a href="pilot/tests/CUSB-CYL0013a-MT16.mp3">CUSB-CYL0013a-MT16.mp3</a></li>
   <li>22.5 Khz, mono, 48 kbit/sec (1393K) <a href="pilot/tests/CUSB-CYL0013a-MT22.mp3">CUSB-CYL0013a-MT22.mp3</a></li>
   <li>24 Khz mono, 56 kbit/sec (1632K) <a href="pilot/tests/CUSB-CYL0013a-MT24.mp3">CUSB-CYL0013a-MT24.mp3</a></li>
   <li>32 Khz mono, 96 kbit/sec (2798K) <a href="pilot/tests/CUSB-CYL0013a-MT32.mp3">CUSB-CYL0013a-MT32.mp3</a></li>
   <li>24 Khz mono, 48 kbit/sec (1425K) <a href="pilot/tests/CUSB-CYL0008-24-48.mp3">CUSB-CYL0008-24-48.mp3</a></li>
   <li>22 Khz mono, 56 kbit/sec (1654K) <a href="pilot/tests/CUSB-CYL0008-22-56.mp3">CUSB-CYL0008-22-56.mp3</a></li>
   <li>22 Khz mono, 48 kbit/sec (1418K) <a href="pilot/tests/CUSB-CYL0008-22-48.mp3">CUSB-CYL0008-22-48.mp3</a></li>
   <li>24 Khz mono, 56 kbit/sec (1662K) <a href="pilot/tests/CUSB-CYL0008-24-56.mp3">CUSB-CYL0008-24-56.mp3</a></li>
</ul>
</p>

<p>Quicktime file with Qdesign codec</p>
<ul>
   <li>11Khz mono, 12 kbit/sec <a href="pilot/tests/CUSB-CYL0025-QD56.mov">CUSB-CYL0025-QD56.mov</a></li>
</ul>
<p>Quicktime files with mp3 compression at various sampling rates and bit rates</p>
<ul>
   <li>11 Khz mono, 16 kbit/sec <a href="pilot/tests/CUSB-Cyl008-QTMp3test1.mov">CUSB-CYL0008-QTMP3test-01.mov</a></li>
   <li>11 Khz mono, 18 kbit/sec <a href="pilot/tests/CUSB-Cyl008-QTMp3test2.mov">CUSB-CYL0008-QTMP3test-02.mov</a></li>
   <li>11 Khz mono, 20 kbit/sec <a href="pilot/tests/CUSB-Cyl008-QTMp3test3.mov">CUSB-CYL0008-QTMP3test-03.mov</a></li>
   <li>12 Khz mono, 18 kbit/sec <a href="pilot/tests/CUSB-Cyl008-QTMp3test4.mov">CUSB-CYL0008-QTMP3test-04.mov</a></li>
   <li>12 Khz mono, 20 kbit/sec <a href="pilot/tests/CUSB-Cyl008-QTMp3test5.mov">CUSB-CYL0008-QTMP3test-05.mov</a></li>
   <li>12 Khz mono, 24 kbit/sec <a href="pilot/tests/CUSB-Cyl008-QTMp3test6.mov">CUSB-CYL0008-QTMP3test-06.mov</a></li>
   <li>16 Khz mono, 20 kbit/sec <a href="pilot/tests/CUSB-Cyl008-QTMp3test7.mov">CUSB-CYL0008-QTMP3test-07.mov</a></li>
   <li>16 Khz mono, 24 kbit/sec <a href="pilot/tests/CUSB-Cyl008-QTMp3test8.mov">CUSB-CYL0008-QTMP3test-08.mov</a></li>
   <li>16 Khz mono, 32 kbit/sec <a href="pilot/tests/CUSB-Cyl008-QTMp3test9.mov">CUSB-CYL0008-QTMP3test-09.mov</a></li>
   <li>22 Khz mono, 24 kbit/sec <a href="pilot/tests/CUSB-Cyl008-QTMp3test10.mov">CUSB-CYL0008-QTMP3test-10.mov</a></li>
   <li>22 Khz mono, 32 kbit/sec <a href="pilot/tests/CUSB-Cyl008-QTMp3test11.mov">CUSB-CYL0008-QTMP3test-11.mov</a></li>
	<li>22 Khz mono, 40 kbit/sec <a href="pilot/tests/CUSB-Cyl008-QTMp3test12.mov">CUSB-CYL0025-QTMP3test-12.mov</a></li>
   <li>22 Khz mono, 48 kbit/sec <a href="pilot/tests/CUSB-Cyl008-QTMp3test13.mov">CUSB-CYL0008-QTMP3test-13.mov</a></li>
   <li>22 Khz mono, 56 kbit/sec <a href="pilot/tests/CUSB-Cyl008-QTMp3test14.mov">CUSB-CYL0025-QTMP3test-14.mov</a></li>
   <li>22 Khz mono, 56 kbit/sec <a href="pilot/tests/CUSB-Cyl008-QTMp3test15.mov">CUSB-CYL0025-QTMP3test-15.mov</a></li>
</ul>
  </div>
</div>
<!-- end .content -->
<!-- End of file: pilottech.tpl -->   
{include file="footer.tpl"}
