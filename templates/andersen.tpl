{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: accordion.tpl -->
<div class="content text">
  <h1>Arreola Family Collection of Mexican and Cuban Cylinders in the Lynn Andersen Collection</h1>
  <div><img src="../images/rss-podcast.png" width="80" height="15" alt=""/></div>

  <p>Lynn Andersen (1937-2015), a collector of early phonograph recordings, was particularly interested in foreign recordings and acquired a collection of <a href="/search.php?nq=1&query_type=keyword&query=lynn+andersen+mexican">Mexican</a> and <a href="/search.php?nq=1&query_type=keyword&query=lynn+andersen+cuba">Cuban</a> cylinders that had originally been assembled by the Arreola family of Florence, Arizona Territory, before 1910. These cylinders have a unique provenance in that they have stayed together for over 100 years, first with the Arreola family, later being sold (or given) to noted Los Angeles collector Ray Phillips, and later sold to Lynn Andersen then of Bisbee Arizona. In 2016 they were acquired by UCSB and will now stay together indefinitely and be accessible to the public.  </p>
  <p>Andersen performed pioneering research on Edison's Mexican cylinders and assembled primary source documents on Edison's early field trips to Mexico before 1910 and wrote articles for the <em>Soundbox</em>, a collector's magazine where many    collectors published their research. These documents are presented here, along with a playlist of Mexican and Cuban cylinders originally compiled by Andersen for presentations to collectors, community groups, and schools, that we are now presenting as a podcast. </p>
  <p>The research and study on these early recordings is far from complete, partly because they are so difficult to find. UCSB's Cylinder Audio Archive now has one of the largest collections of commercial <a href="/search.php?nq=1&query_type=keyword&query=mexican">Mexican</a> and <a href="/search.php?nq=1&query_type=keyword&query=cuban">Cuban</a> cylinder recordings, which are now online for listening and study.</p>
  <p><em>- David Seubert, Curator</em></p>
  <h3><br>
    Playlist  </h3>
  <p><em>Coming soon!</em></p>
  <h3>Resources</h3>
<ul>
    <li>Lynn Andersen's articles on Mexican and Cuban cylinders from the <a href="andersen/SoundboxArticles.pdf">September  and November 1994 (vol XII, nos. 1-2)</a> and <a href="andersen/SoundboxArticles2.pdf">January 1995 (vol XII, no 3)</a> issues of the <em><a href="http://worldcatlibraries.org/wcpa/oclc/32888687">Soundbox</a></em>, the newsletter of the <a href="http://www.antiquephono.org/">Antique Phonograph Society</a>.</li>
    <li><a href="../EPMMexican.pdf">Articles</a> on Edison's Mexican cylinders compiled from the <em><a href="http://worldcatlibraries.org/wcpa/oclc/4254820">Edison Phonograph Monthly</a></em> (a trade publication), 1904-1911.</li>
    <li><a href="../SydneyCarterMexican.pdf">Sydney Carter's listing of Mexican cylinders</a> from his 1964 publication <em><a href="http://worldcatlibraries.org/wcpa/oclc/50595784">The Complete Catalogue of the Edison Two-Minute Wax Cylinder Records: Containing all the United States issues from 1888 Onwards</a></em> (Abbotts Close, Worthing, England: [S.H. <span id="normalb">Carter</span>], 1964-1965).</li>
    <li><a href="andersen/FonogramasEdison.pdf">Fonogramas Edison</a>, undated trade catalog listing of Edison 2-minute Mexican Blue Amberol cylinders.</li>
    <li>Business records concerning Mexican cylinders, primarily between Walter Stevens, manager of the Foreign Department, and W. E. Gilmore, president of the National Phonograph Co, 1900-19. 
      <ul>
        <li><a href="andersen/unknowntoGilmore5111900.pdf">Letter from unknown sender (possibly Walter Stevens) to W. E. Gilmore on May 1, 1900</a> regarding the failure of Edison  representative travelling through Mexico to secure  enough business.</li>
        <li><a href="andersen/StevenstoGilmore1161902.pdf">Letter from Walter Stevens to W. E. Gilmore on November 6, 1902</a> detailing largest Edison clients in Mexico from 1901 to 1902.</li>
        <li><a href="andersen/StevenstoGilmore1151903.pdf">Letter from Walter Stevens to W. E. Gilmore on November 5, 1903</a> regarding business with Mexican client Espinosa y Alcalde from 1901 to 1903 and potential to expand Mexican market.</li>
        <li><a href="andersen/CabanastoStevens3241904.pdf">Letter from Rafael Cabañas to Walter Stevens on March 24, 1904</a> summarizing  Columbia records he forwarded from Mexico to provide Edison officials in the US a sense of Columbia's Mexican repertoire and sound quality.</li>
        <li><a href="andersen/CabanastoStevens451904.pdf">Letter from Rafael Cabañas to Walter Stevens on April 5, 1904</a> regarding arrangements for Edison's first recording studio in Mexico City, including shipment of equipment from the US, arrival of a recording engineer, and performers and repertoire to be recorded.</li>
        <li><a href="andersen/StevenstoGilmore4181904.pdf">Letter from Walter Stevens to W. E. Gilmore on April 18, 1904</a>  detailing number of masters to be recorded at Mexico City studio.</li>
        <li><a href="andersen/StevenstoGilmore4291904.pdf">Letter from Walter Stevens to W. E. Gilmore on April 29, 1904</a></a> regarding entry into Mexican market, including: advertising, price cutting, jobbers, and customs fees.</li>
        <li><a href="andersen/CabanastoStevens581904.pdf">Letter from Rafael Cabañas to Walter Stevens on May 8, 1904</a> regarding progress of recording at new studio, delivery of masters to New York, and possibility of recording Italian opera for Mexican market.</li>
        <li><a href="andersen/StevenstoGilmore5171904.pdf">Letter from Walter Stevens to W. E. Gilmore on May 17, 1904</a> regarding announcement in Mexican press about new studio and providing an update on price cutting in among Mexican dealers.</li>
        <li><a href="andersen/StevenstoGilmore8301904.pdf">Letter from Walter Stevens to W. E. Gilmore on August 30, 1904</a> regarding a signed letter from Thomas Edison and a selection of Mexican records to be presented to President Porfirio Diaz  on Mexican Independence Day.</li>
        <li><a href="andersen/StevenstoSchermerhorn971904.pdf">Letter from Walter Stevens to J. R. Schermerhorn on September 7, 1904</a> including a translation of the letter for President Porfirio Diaz.</li>
        <li><a href="andersen/unknowntoGilmore1121905.pdf">Excerpt of a letter from unknown sender (possibly Walter Stevens) to W. E. Gilmore on January 12, 1905</a> with a chart detailing increase in business in Mexico.</li>
        <li><a href="andersen/CabanastoStevens1231905.pdf">Letter from Rafael Cabañas to Walter Stevens on January 23, 1905</a> reporting on Edison business in Mexico, including: expanding repertoire to attract higher class customers, competition from Victor and Columbia, price cutting, price standardization, and expanding market outside of Mexico City.</li>
        <li><a href="andersen/StevenstoGilmore321905.pdf">Letter from Walter Stevens to W. E. Gilmore on March 2, 1905</a> regarding sending Cabañas to Cuba and Puerto Rico.</li>
        <li><a href="andersen/unknowntoCabanas7291906.pdf">Letter from unknown music publisher representative to Rafael Cabañas on July 29, 1906</a> explaining the necessity of having an agent in Milan to improve Edison's opera offerings.<br>
        </li>
      </ul>
    </li>
  </ul>
</div><!-- end .content -->
<!-- End of file: accordion.tpl -->
{include file="footer.tpl"}
