{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: accordion.tpl -->
<div class="content text">
  <h1>Squeezebox Cylinder Recordings</h1>
  <div>
    <a href="thematic_playlists.xml"><img class="podcast-logo" src="/images/rss-podcast.png" alt="Podcast"></a>
  </div>

  <h3>Introduction</h3>
  <p>
    It's a pleasure to welcome   Bruce Triggs, creator of the <a href="http://accordionnoir.org/">Accordion Noir radio show</a>,
    as a guest curator for this playlist. Accordion Noir Radio is a non-profit devoted to the belief that the accordion is just another instrument; the group hosts
    podcasts of accordion music and also sponsors an accordion festival. I share Bruce's enthusiasm for the accordion and I also believe
    it is one of the great instruments of the early sound recording  era, as some of the most interesting ethnic, folk and popular music was
    performed by accordionists.  </p>
  <p>
    This is part one of a two-part series on the accordion and highlights 10 recordings from the UCSB Cylinder Audio Archive. Let us know what you think.  </p>
  <p>
    <em>&mdash;<a href="mailto:seubert@library.ucsb.edu">David Seubert</a>, UC Santa Barbara Library</em>
  </p>
  <h3>Curator's notes</h3>
  <p>
    Austrian Cyrill Demian filed a patent for his &ldquo;accordion&rdquo; in Vienna on May 6, 1829.  Because of the relatively late invention
    of this family of instruments, it is  possible to document much of its mechanical development as well as the  influence it had on
    contemporary musical styles. By the time the first cylinder  recordings were released in the 1890s, the instrument was little more than
    60 years old, so these early recordings document a young instrument still  very much in development. &ldquo;Traditional&rdquo;
    folk artists were only a generation  removed from those who first picked up the instrument. These recordings demonstrate  basic folk
    playing mixed with growing technical virtuosity by folk and  professional musicians as heard on the blossoming vaudeville stage. Some of
    these recordings had great influence on musicians on both sides of the Atlantic, leading more to take up the accordion in folk  music,
    and particularly spreading the sound of the newly invented piano-accordion. Overall, these beautiful  early squeezebox recordings remind
    us of both the permanence and the fragility  of culture, art, and memory.
  </p>
  <p>
    <em>&mdash;Bruce Triggs, Vancouver, British Columbia</em>
  </p>

  <h4>
    Medley of Irish reels. John J. Kimmel.
    <a href="search.php?nq=1&query_type=call_number&query=3689">Edison Standard Record: 10284</a>. 1909.
  </h4>
  <p>
    We begin with the classic &ldquo;Edison  shout&rdquo; announcement (used until about mid-1908) for our first artist,
    <a href="/search.php?nq=1&query_type=keyword&query=kimmel">Mr. John  J. Kimmel</a>
    (sometimes misspelled &ldquo;Kimmbel&rdquo;). Kimmel produced more of the  cylinders in the archive than any other accordionist.
    He recorded in various Celtic  styles, along with popular marches and tunes. Although he was both U.S.-born and German&ndash;not Irish&ndash;Kimmel
    was especially influential with Irish and Celtic  musicians, as his were some of the first recordings available.
  </p>
  <p>
    <em>Further information on Kimmel: <a href="http://www.folkways.si.edu/albumdetails.aspx?itemid=121">http://www.folkways.si.edu/albumdetails.aspx?itemid=121</a></em>
  </p>
  <h4>
    Italian army march / Richard Eilenberg. Guido Deiro.
    <a href="search.php?nq=1&query_type=keyword&query=0789">Edison Blue Amberol: 1774</a>. 1913.
  </h4>
  <p>
    The first and greatest proponent of the piano-accordion was  undoubtedly the vaudeville star
    <a href="/search.php?nq=1&query=guido+deiro&query_type=keyword">Guido Deiro</a>.
    Along with his business-savvy  brother Pietro, Deiro premiered and popularized the piano-accordion all over North America. G. Deiro
    was an American accordion  celebrity, receiving top billing on vaudeville stages across the continent as  one of the most highly
    paid entertainers in North America.  Performing solo, without an orchestra or band, he earned up to $600 per week
    ($5,000&ndash;$12,000 in today's  dollars). Most colorfully, he secretly married a young  performer named Mae West, who
    later went on to wider fame. Deiro recorded on Columbia and had more accordion firsts on radio and film,
    but his light faded with the end of vaudeville.
  </p>
  <p>
    Most lastingly, G. Deiro brought the piano-accordion to the  attention of the American public. The newly developed
    piano-accordion was  promoted as a modern rejection of various &ldquo;old-country&rdquo; designs. This helped to
    standardize the many European button-keyboard layouts, allowing North American  accordion dealers to sell a single
    style to Finnish, German, Italian, Russian,  Belgian, and French accordion players without having to stock each regional format.
  </p>
  <p>
    <em>Further information on Deiro: <a href="http://www.guidodeiro.com/">http://www.guidodeiro.com/</a></em>
  </p>

  <h4>
    Poppies / Neil Morét. Alexander Prince.
    <a href="search.php?nq=1&query_type=call_number&query=2600">Edison Gold Moulded Record: 13764</a>. 1908.
  </h4>
  <p>
    <a href="/search.php?nq=1&query=alexander+prince&query_type=keyword">Alexander Prince</a>
    was a highly regarded &ldquo;duet&rdquo; concertina player. Along  with Kimmell, Prince was the most prolific early squeezebox
    recording artist. He  played <a href="http://www.concertina.com/maccann-duet/">Maccann duet concertina</a>
    (one of the three most popular concertina  formats).
  </p>
  <p>
    According to concertina scholar <a href="http://www.concertina.com/williams/">Wes Williams</a>, Prince was born in Scotland and made many
    recordings on cylinders and later for Columbia  discs. He was said to have broken his leg as a child, and his father,
    who owned  a music shop, gave him a concertina to amuse himself with. A few years later he  was playing at the London
    Pavilion and the Victorian icon, the Crystal Palace.
  </p>
  <p>
    The concertina is a member of the accordion&rsquo;s &ldquo;free-reed&rdquo; family (which also includes the harmonica).
    Charles Wheatstone developed the  concertina in England at the same time that  others in Europe were working on
    instruments that became the accordion. There are several main types of concertinas: Wheatstone&rsquo;s
    English style (from England), the Anglo style  (based on a diatonic system from Germany), and  the &ldquo;duet&rdquo; style.
  </p>
  <p>
    The &ldquo;duet&rdquo; concertina Prince played had bass notes on the left  hand and treble on the right. This makes complex
    countermelodies and  counterpoint &ldquo;duets&rdquo; with oneself possible and facilitated his transcriptions  of orchestral works.
  </p>
  <p>
    To see clips of a modern duet concertina player, visit:<br>
      <a href="http://www.concertina.com/callaghan/reuben-shaw-video/index.htm">Garland Films  presents &quot;Reuben Shaw &ndash; Duet Concertina Player</a>&rdquo;
  </p>

  <h4>
    Oi ya nestchastay (Malo russkaya piesnia) / Mykola Vitaliiovych Lysenko. Alexander Sashko ; A. Iranova. <a href="search.php?nq=1&query_type=call_number&query=8015">Edison Blue Amberol: 11234</a>. 1922.
  </h4>
  <p>
    This track is  likely  played on a &ldquo;garmon,&rdquo; the Russian name for a diatonic button accordion. These
    are played widely in Eastern Europe, and it  seems quite likely that a Ukrainian song would use one.
  </p>
  <p>
    It can be difficult to determine what instrument is used on these  recordings. Often, the record producers
    didn&rsquo;t spell artists&rsquo; names correctly,  casting doubt on their ability to differentiate between types of accordions.
  </p>
  <p>
    Decades later, the African-American folk singer Lead Belly was  often listed as playing a concertina, when he was in
    fact one of the last  African Americans representing a pre-blues, pre-jazz button accordion tradition  that dated back
    to before the end of slavery. Sadly, we have no cylinder  recordings from that tradition, as it went almost completely
    unrecorded by  folklorists and commercial recorders, and the last of its practitioners died in  the 1970s.
  </p>

  <h4>
    Medley of Irish jigs / N/A. Patrick Scanlon and Dennis L. Smith.
    <a href="search.php?nq=1&query_type=call_number&query=5598">Edison Blue Amberol: 3361</a>. 1918.
  </h4>
  <p>
    Little information has turned up on Patrick Scanlon except that he recorded later for  Columbia, and his records were collected by  fiddlers at least as far away as Ohio.
  </p>
  <h4>
    Tarantella Siciliana / Frank Lucanese. Three Vagrants.
    <a href="search.php?nq=1&query_type=call_number&query=1130">Edison Blue Amberol: 4187</a>. 1921.
  </h4>
  <p>
    <a href="/search.php?nq=1&query=Three+Vagrants&query_type=keyword">Three Vagrants</a>
    were a vaudeville trio with Frank Lucanese on accordion. Promotional  photos also show a large harp-guitar, an interesting
    and now uncommon novelty instrument.  One photo from 1928 includes Josephine Bergamasco as accordionist in the trio. Though it
    seems that would have been from a later version of the band, I believe she  would be the only documented woman accordionist to
    play in any of these musical  groups.
  </p>
  <p>
    <em>Further information can be found at:
      <a href="http://www.nicklucas.com/threevagrants.html">http://www.nicklucas.com/threevagrants.html</a></em>
  </p>

  <h4>
    Wedding of the winds  / John T. Newcomer. Pietro Frosini.
    <a href="search.php?nq=1&query_type=call_number&query=1638">Edison Amberol: 103</a>. 1909.
  </h4>
  <p>
    Immigrating from Sicily in 1905, <a href="/search.php?nq=1&query=frosini&query_type=keyword">Frosini</a>
    was probably the most successful and skillful holdout against the piano-accordion  in America. His chromatic button accordion
    (capable of playing all the notes on a chromatic scale, with each button  playing the same note going in and out) was custom
    built with dummy piano keys  so it wouldn&rsquo;t appear he was playing an &ldquo;old fashioned&rdquo; button accordion.
  </p>
  <p>
    <em>Further information can be found at: <a href="http://www.accordionusa.com/fe_03_07.htm">http://www.accordionusa.com/fe_03_07.htm</a></em>
  </p>

  <h4>
    Russian kamarinskaja [Kamarinskai] / Mikhail Ivanovich Glinka. Accordion solo.
    <a href="search.php?nq=1&query_type=call_number&query=4929">Columbia Phonograph Co.: 65061</a>. 1904-1909.
  </h4>
  <p>
    An example of an unknown accordionist and whistler (unless  someone might provide a translation of the Russian?).
    Whistling was quite  popular in the early recording industry. The fact that companies sold virtuoso
    whistling records must mean that people appreciated a good example of music  they were familiar with before
    radios and pocket music players.
  </p>

  <h4>
    Lándlery Tyrolske: valcik [Walzer, Ländler und Ecossaisen, piano, D. 145; arr.] / Franz Schubert. Ceské Trio z Prahy.
    <a href="search.php?nq=1&query_type=call_number&query=1297">Edison Blue Amberol: 9852</a>. 1913.
  </h4>
  <p>
    Recording companies eventually realized the profit potential in  immigrant communities, whose members would spend money
    for something that  reminded them of &ldquo;home.&rdquo; Often starting by simply guessing what would sell in a  given
    ethnic community, companies eventually found local distributors within  ethnic neighborhoods who were able to suggest
    artists their customers wanted to  hear—like this recording from Edison's &quot;Bohemian,&quot; or Czech, series.
  </p>
  <p>
    During the decades before the Great Depression devastated the  industry, &ldquo;ethnic&rdquo; artists were a growing
    part of most companies&rsquo; recording  catalogs. Later, in the '30s and '40s, ethnic music reemerged with crossover
    pop artists like Frankie Yankovich and polka hits by wartime stars such as the  Andrews Sisters. All can be traced back
    to early recordings like this one.
  </p>

  <h4>
    Nautical airs. Alexander Prince.
    <a href="search.php?nq=1&query_type=call_number&query=1732">Edison Amberol: 245</a>. 1909.
  </h4>
  <p>
    It has been noted that, despite popular imagery, the &ldquo;golden age  of piracy&rdquo; ended in 1750, so no pirate
    ever heard a concertina or an accordion,  much less played one. Concertinas and accordions were, however, very popular
    among sailors during the Industrial Revolution, spreading by sail and steam  across the globe in a matter of decades.
    By 1845, ships had carried them to America, where blackface minstrels played accordions only  16 years after the
    instrument was invented. Minstrel performers later spread  their mocking racial stereotypes internationally, bringing
    an early Tin Pan Alley  repertoire to audiences as disparate as blacks in South   Africa and traditional Irish players,
    who incorporated tunes heard  in their port cities into their own music.
  </p>
  <p>
    <em>For further information see:
    <a href="http://www.angloconcertina.org/concertina_at_sea.html">http://www.angloconcertina.org/concertina_at_sea.html</a></em>
  </p>
  <hr>
  <h3>Other Sources and Further Reading</h3>
  <p>
    The Columbia Master Book Discography: Principal U.S. matrix  series, 1910&ndash;1924&nbsp;by Tim Brooks, Brian A. L. Rust
  </p>
  <p>
    Howard Sacks: From the Barn to the Bowery and Back Again: Musical  Routes in Rural Ohio, 1800&ndash;1929
    [Phillips  Barry Lecture, October 2000] The Journal of American Folklore Vol. 116, No. 461  (Summer, 2003), pp. 314&ndash;338
  </p>
  <p>
    Ethnic Recordings in America, a  Neglected Heritage. American   Folklife Center,  (US) Library of Congress, 1982.
  </p>
  <p>
    Passion for Polka: Old-Time Ethnic Music in America:  Victor Greene, University   of California Press, Berkeley,  1992.
  </p>
  <p>
    Leadbelly  and His Windjammer: Examining the African American Button Accordion  Tradition, Jared Snyder.
    American Music Vol. 12, No. 2 (Summer, 1994),  pp. 148-166.&nbsp;
  </p>
</div><!-- end .content -->
<!-- End of file: accordion.tpl -->
{include file="footer.tpl"}
