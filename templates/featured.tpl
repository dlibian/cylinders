{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: featured.tpl -->
<div class="content text">
	<h2>UCSB Cylinder Radio</h2>
  <p align="center">
  	<b>Listen to the program</b> (currently unavailable check back soon for version 2.0!)
  </p>
  <p>
  	Two years and nearly 6000 cylinders later, your humble audio technician 
    has good reason to believe that, in the following 170 recordings, essentially 
    the entire gamut of musical styles and performers of the cylinder era 
    can be heard. From pop songs to instrumental pieces, opera arias to vaudeville 
    routines, a sincere attempt has been made to offer representative exemplars 
    of the music that emanated from homes and halls circa 1890-1929. (All 
    the tunes listed here can also be heard randomly by clicking the play 
    button in the featured cylinder section of the home page.) So sit back, 
    crank up the machine, um, er, stereo, and enjoy all that once was the 
    cylinder recording. <i>- Noah Pollaczek, UCSB</i>
  </p>
  <ul>
    <li>Pagliacci. Vesti la giubba / Ruggiero Leoncavallo. 
      Florencio Constantino. <a href="search.php?nq=1&query_type=call_number&query=1">Edison Amberol: 30035</a>. 1910. </li>
    <li>Clarinet squawk. Louisiana Five. <a href="search.php?nq=1&query_type=call_number&query=8">Edison Blue Amberol: 3896</a>. 1920. </li>
    <li>What a time / Polk Miller. Polk Miller and his Old 
      South Quartet. <a href="search.php?nq=1&query_type=call_number&query=10">Edison Amberol: 391</a>. 1910. </li>
    <li>Just before the battle mother / George Frederick Root. 
      Will Oakland. <a href="search.php?nq=1&query_type=call_number&query=13">Edison Amberol: 297</a>. 1909. </li>
    <li>Calvary / Paul Rodney. James F. Harrison. <a href="search.php?nq=1&query_type=call_number&query=26">Edison Amberol: 62</a>. 1908. </li>
    <li>Heart bowed down / Michael William Balfe. William 
      Tuson. <a href="search.php?nq=1&query_type=call_number&query=33">Edison Gold Moulded Record: 8455</a>. 1903. </li>
    <li>Flanagan's troubles in a restaurant / Steve Porter. 
      Steve Porter. <a href="search.php?nq=1&query_type=call_number&query=37">Edison Gold Moulded Record: 9495</a>. 1907. </li>
    <li>Medley of straight jigs. John J. Kimmel. <a href="search.php?nq=1&query_type=call_number&query=38">Edison Gold Moulded Record: 966</a>. 1907. </li>
    <li>Jerusalem mournin'. Polk Miller and his Old South 
      Quartet. <a href="search.php?nq=1&query_type=call_number&query=42">Edison Standard Record: 10334</a>. 1910. </li>
    <li>Forza del destino. O tu che in seno agli angeli / 
      Giuseppe Verdi. Carlo Albani. <a href="search.php?nq=1&query_type=call_number&query=47">Edison Amberol: 30042</a>. 1911. </li>
    <li>Pescatori di Perle. Mi par d&#146;udir ancora [P&ecirc;cheurs 
      de perles. Je crois entendre encore] / Georges Bizet. Aristodemo Giorgini. 
      <a href="search.php?nq=1&query_type=call_number&query=48">Edison Amberol: 30032</a>. 1911. </li>
    <li>Don't take my darling boy away / Albert Von Tilzer. 
      Harry Anthony and Helen Clark. <a href="search.php?nq=1&query_type=call_number&query=113">Edison Blue Amberol: 2622</a>. 1915. </li>
    <li>Messiah. Comfort ye, my people / George Frideric Handel. 
      Reed Miller. <a href="search.php?nq=1&query_type=call_number&query=126">Edison Blue Amberol: 2498</a>. 1914. </li>
    <li>Buck dance medley / John J. Kimmel. John J. Kimmel. 
      <a href="search.php?nq=1&query_type=call_number&query=184">Edison Blue Amberol: 2384</a>. 1914. </li>
    <li>Stick to your mother, Tom / Ernest J. Symons. Will 
      Oakland. <a href="search.php?nq=1&query_type=call_number&query=210">Edison Blue Amberol: 2380</a>. 1914. </li>
    <li>On parade. National Guard Fife and Drum Corps. <a href="search.php?nq=1&query_type=call_number&query=249">Edison Blue Amberol: 1804</a>. 1913. </li>
    <li>I love you California / A.F. Frankenstein. Knickerbocker 
      Quartet and Elizabeth Spencer. <a href="search.php?nq=1&query_type=call_number&query=260">Edison Blue Amberol: 1838</a>. 1913. </li>
    <li>She's my Daisy / Sir Harry Lauder. Sir Harry Lauder. 
      <a href="search.php?nq=1&query_type=call_number&query=264">Edison Blue Amberol: 1817</a>. 1913. </li>
    <li>Uncle Josh's huskin' bee (take 2) / Cal Stewart. Cal 
      Stewart. <a href="search.php?nq=1&query_type=call_number&query=282">Edison Blue Amberol: 1866</a>. 1913. </li>
    <li>The teddy bears' picnic / John W. Bratton. American 
      Symphony Orchestra (West Orange, N.J). <a href="search.php?nq=1&query_type=call_number&query=290">Edison Blue Amberol: 1867</a>. 1913. </li>
    <li>He'd have to get under: get out and get under / Maurice 
      Abrahams. Billy Murray. <a href="search.php?nq=1&query_type=call_number&query=330">Edison Blue Amberol: 2194</a>. 1914. </li>
    <li>Souvenir / Franz Drdla. Demetrius Constantine Dounis. 
      <a href="search.php?nq=1&query_type=call_number&query=380">Edison Blue Amberol: 2010</a>. 1913. </li>
    <li>There's a mother always waiting you, at home, sweet 
      home / James Thornton. Will Oakland. <a href="search.php?nq=1&query_type=call_number&query=392">Edison Blue Amberol: 2030</a>. 1913. </li>
    <li>I sent my wife to the Thousand Isles / Harry Von Tilzer. 
      Billy Murray. <a href="search.php?nq=1&query_type=call_number&query=499">Edison Blue Amberol: 2999</a>. 1916. </li>
    <li>Morte d'Otello [Otello. Niun mi tema] / Giuseppe Verdi. 
      Florencio Constantino. <a href="search.php?nq=1&query_type=call_number&query=504">Edison Blue Amberol: 28140</a>. 1913. </li>
    <li>Ah! non credevi tu [Mignon. Elle ne croyait pas] / 
      Ambroise Thomas. Florencio Constantino. <a href="search.php?nq=1&query_type=call_number&query=507">Edison Blue Amberol: 28150</a>. 1913. </li>
    <li>Tambourin chinois / Fritz Kreisler. Mary Zentay. <a href="search.php?nq=1&query_type=call_number&query=559">Edison Blue Amberol: 28246</a>. 1916. </li>
    <li>Lucia di Lammermoor. Fra poco a me ricovero / Gaetano 
      Donizetti. Guido Ciccolini. <a href="search.php?nq=1&query_type=call_number&query=583">Edison Blue Amberol: 28273</a>. 1917. </li>
    <li>Whispering hope / Septimus Winner. Helen Clark and 
      Harry Anthony. <a href="search.php?nq=1&query_type=call_number&query=607">Edison Blue Amberol: 1518</a>. 1912. </li>
    <li>Medley of Hawaiian airs no. 2. Helen Louise and Frank 
      Ferera. <a href="search.php?nq=1&query_type=call_number&query=625">Edison Blue Amberol: 2941</a>. 1916. </li>
    <li>Two jolly sailors / Steve Porter. Steve Porter and 
      Byron G. Harlan. <a href="search.php?nq=1&query_type=call_number&query=729">Edison Blue Amberol: 1759</a>. 1913. </li>
    <li>Uncle Josh keeps house / Cal Stewart. Cal Stewart. 
      <a href="search.php?nq=1&query_type=call_number&query=734">Edison Blue Amberol: 1714</a>. 1913. </li>
    <li>William Tell: fantasie [Guillaume Tell. Selections; 
      arr.] / Gioacchino Rossini. Charles Daab. <a href="search.php?nq=1&query_type=call_number&query=774">Edison Blue Amberol: 1730</a>. 1913. </li>
    <li>Old folks at home; arr. / Stephen Collins Foster. 
      Andr&eacute; Benoist. <a href="search.php?nq=1&query_type=call_number&query=811">Edison Blue Amberol: 1908</a>. 1913. </li>
    <li>Down home rag / Wilbur C. Sweatman. Van Eps Trio. 
      <a href="search.php?nq=1&query_type=call_number&query=849">Edison Blue Amberol: 2377</a>. 1914. </li>
    <li>Medley of Hawaiian airs no. 1. Helen Louise and Frank 
      Ferera. <a href="search.php?nq=1&query_type=call_number&query=880">Edison Blue Amberol: 2917</a>. 1916. </li>
    <li>I want to go back to Michigan / Irving Berlin. Billy 
      Murray. <a href="search.php?nq=1&query_type=call_number&query=925">Edison Blue Amberol: 2507</a>. 1914. </li>
    <li>Aloha oe / Liliuokalani, Queen of Hawaii. William 
      Smith and Walter K. Kolomoku. <a href="search.php?nq=1&query_type=call_number&query=985">Edison Blue Amberol: 2701</a>. 1915. </li>
    <li>Tha&iuml;s. M&eacute;ditation; arr. / Jules Massenet. 
      Albert Spalding. <a href="search.php?nq=1&query_type=call_number&query=1070">Edison Blue Amberol: 28102</a>. 1912. </li>
    <li>Air for G string [Suites, orchestra, BWV 1068, D major. 
      Air; arr.] / Johann Sebastian Bach. Joel Belov. <a href="search.php?nq=1&query_type=call_number&query=1089">Edison Blue Amberol: 4010</a>. 1920. </li>
    <li>Bo-la-bo / George Fairman. Lopez and Hamilton's Kings 
      of Harmony Orchestra. <a href="search.php?nq=1&query_type=call_number&query=1098">Edison Blue Amberol: 4020</a>. 1920. </li>
    <li>Haste to the wedding. John J. Kimmel. <a href="search.php?nq=1&query_type=call_number&query=1135">Edison Blue Amberol: 4194</a>. 1921. </li>
    <li>Afghanistan / William Wilander. Lopez and Hamilton's 
      Kings of Harmony Orchestra. <a href="search.php?nq=1&query_type=call_number&query=1142">Edison Blue Amberol: 4043</a>. 1920. </li>
    <li>All over this world. Fisk Jubilee Singers. <a href="search.php?nq=1&query_type=call_number&query=1158">Edison Blue Amberol: 4045</a>. 1920. </li>
    <li>What's the good of kicking, let's go 'round with a 
      smile / Walter Donaldson. Maurice Burkhart. <a href="search.php?nq=1&query_type=call_number&query=1174">Edison Blue Amberol: 4109</a>. 1920. </li>
    <li>Return of spring / Emile Waldteufel. Three Vagrants. 
      <a href="search.php?nq=1&query_type=call_number&query=1191">Edison Blue Amberol: 4152</a>. 1921. </li>
    <li>The ring and the rose / Kitty Berger. Kitty Berger. 
      <a href="search.php?nq=1&query_type=call_number&query=1218">Edison Blue Amberol: 4286</a>. 1921. </li>
    <li>Annie, my own / Henry Scharf. Harry Raderman's Jazz 
      Orchestra. <a href="search.php?nq=1&query_type=call_number&query=1232">Edison Blue Amberol: 4217</a>. 1921. </li>
    <li>Why did you do it? / George Jessel. Georgia Melodians. 
      <a href="search.php?nq=1&query_type=call_number&query=1276">Edison Blue Amberol: 4903</a>. 1924. </li>
    <li>Predilecta: vals / Carlos Curti. Trio Instrumental 
      Arriaga. <a href="search.php?nq=1&query_type=call_number&query=1327">Edison Blue Amberol: 22027</a>. 1913. </li>
    <li>Peggy / Neil Moret. Lopez and Hamilton's Kings of 
      Harmony Orchestra. <a href="search.php?nq=1&query_type=call_number&query=1377">Edison Blue Amberol: 4006</a>. 1920. </li>
    <li>Will it ever be the same again / Joseph Cooper. Billy 
      Jones. <a href="search.php?nq=1&query_type=call_number&query=1398">Edison Blue Amberol: 4051</a>. 1920. </li>
    <li>Roamin' in the gloamin' / Sir Harry Lauder. Sir Harry 
      Lauder. <a href="search.php?nq=1&query_type=call_number&query=1407">Edison Blue Amberol: 23003</a>. 1912-1914. </li>
    <li>Laughing song / Polk Miller. Polk Miller and his Old 
      South Quartet. <a href="search.php?nq=1&query_type=call_number&query=1441">Edison Blue Amberol: 2176</a>. 1913. </li>
    <li>Tra, la, la, la! / Irving Berlin. Billy Murray. <a href="search.php?nq=1&query_type=call_number&query=1511">Edison Blue Amberol: 2136</a>. 1913. </li>
    <li>Tosca. E lucevan le stelle / Giacomo Puccini. Leo 
      Slezak. <a href="search.php?nq=1&query_type=call_number&query=1530">Edison Blue Amberol: 28146</a>. 1913. </li>
    <li>Faust. Salve dimora [Faust. Salut, demeure chaste 
      et pure] / Charles Gounod. Alessandro Bonci. <a href="search.php?nq=1&query_type=call_number&query=1543">Edison Royal Purple Amberol: 29003</a>. 1918. </li>
    <li>Breakfast in bed / Sir Harry Lauder. Sir Harry Lauder. 
      <a href="search.php?nq=1&query_type=call_number&query=1554">Edison Blue Amberol: 23017</a>. 1913. </li>
    <li>Polonaise in D major [Polonaise brillante, no. 1] 
      / Henri Wieniawski. Albert Spalding. <a href="search.php?nq=1&query_type=call_number&query=1687">Edison Amberol: 177</a>. 1909. </li>
    <li>Poet and peasant [Dichter und Bauer. Ouvert&uuml;re; 
      arr.] / Franz von Suppé. Pietro Frosini. <a href="search.php?nq=1&query_type=call_number&query=1700">Edison Amberol: 192</a>. 1909. </li>
    <li>The baseball girl. Miss Ray Cox. <a href="search.php?nq=1&query_type=call_number&query=1704">Edison Amberol: 196</a>. 1909. </li>
    <li>Hungarian dances: G minor and A major [Ungarische 
      T&auml;nze. Selections; arr.] / Johannes Brahms. Albert Spalding. <a href="search.php?nq=1&query_type=call_number&query=1710">Edison Amberol: 203</a>. 1909. </li>
    <li>Carnival of Venice [Carnevale di Venezia; arr.] / 
      Nicol&ograve; Paganini. Ollivotti Troubadours. <a href="search.php?nq=1&query_type=call_number&query=1750">Edison Amberol: 302</a>. 1909. </li>
    <li>Waltz / Samuel Siegel. Samuel Siegel and Roy H. Butin. 
      <a href="search.php?nq=1&query_type=call_number&query=1764">Edison Amberol: 316</a>. 1909. </li>
    <li>In the gloaming / Annie Fortescue Harrison. Will Oakland. 
      <a href="search.php?nq=1&query_type=call_number&query=1765">Edison Amberol: 320</a>. 1909. </li>
    <li>Lead kindly light / John Bacchus Dykes . Knickerbocker 
      Quartet. <a href="search.php?nq=1&query_type=call_number&query=1775">Edison Amberol: 341</a>. 1909. </li>
    <li>Amoureuse / Rudolphe Berger. Pietro Frosini. <a href="search.php?nq=1&query_type=call_number&query=1779">Edison Amberol: 347</a>. 1909. </li>
    <li>Ich trinke nicht mehr. Grete Wiedecke. <a href="search.php?nq=1&query_type=call_number&query=1847">Edison Goldguss Walze: 15513</a>. 1906. </li>
    <li>My south polar expedition / Ernest Henry Shackleton. 
      Sir Ernest Henry Shackleton. <a href="search.php?nq=1&query_type=call_number&query=1859">Edison Amberol: 473</a>. 1910. </li>
    <li>Home, sweet home / John Howard Payne. Knickerbocker 
      Quartet. <a href="search.php?nq=1&query_type=call_number&query=1878">Edison Amberol: 598</a>. 1910. </li>
    <li>The premier / Edward Llewellyn. Arthur S. Witcomb 
      and the United States Marine Band. <a href="search.php?nq=1&query_type=call_number&query=1910">Edison Amberol: 536</a>. 1910. </li>
    <li>Come Josephine in my flying machine / Fred Fisher. 
      Ada Jones and Billy Murray. <a href="search.php?nq=1&query_type=call_number&query=1943">Edison Amberol: 655</a>. 1911. </li>
    <li>Cradle song (take 1). James Scott Skinner. <a href="search.php?nq=1&query_type=call_number&query=1975">Edison Amberol: 604</a>. 1910. </li>
    <li>The crushed tragedian / Len Spencer. Ada Jones and 
      Len Spencer. <a href="search.php?nq=1&query_type=call_number&query=1988">Edison Amberol: 670</a>. 1911. </li>
    <li>Dragons de Villars. Ouverture; arr. / Aim&eacute;Maillart. 
      Batterie-fanfare de la Garde r&eacute;publicaine. <a href="search.php?nq=1&query_type=call_number&query=2019">Edison Amberol: 714</a>. 1911. </li>
    <li>Down at Finnegan's jamboree. Charles D'Almaine. <a href="search.php?nq=1&query_type=call_number&query=2021">Edison Amberol: 718</a>. 1911. </li>
    <li>I want to sing in opera: The siren / Worton David. 
      Bob Roberts. <a href="search.php?nq=1&query_type=call_number&query=2127">Edison Amberol: 906</a>. 1911. </li>
    <li>Uncle Josh in a barber shop / Cal Stewart. Cal Stewart. 
      <a href="search.php?nq=1&query_type=call_number&query=2129">Edison Amberol: 909</a>. 1911. </li>
    <li>Peter on the sea; The ole ark. Fisk Jubilee Singers. 
      <a href="search.php?nq=1&query_type=call_number&query=2164">Edison Amberol: 978</a>. 1912. </li>
    <li>Roll Jordan roll. Fisk Jubilee Singers. <a href="search.php?nq=1&query_type=call_number&query=2165">Edison Amberol: 980</a>. 1912. </li>
    <li>The band of Gideon. Fisk Jubilee Singers. <a href="search.php?nq=1&query_type=call_number&query=2166">Edison Amberol: 983</a>. 1912. </li>
    <li>The ghosts' dance / Ernest Dunkels. National Military 
      Band. <a href="search.php?nq=1&query_type=call_number&query=2244">Edison Amberol: 12244</a>. 1910. </li>
    <li>Ben Bolt / Nelson Kneass. Eleonora de Cisneros. <a href="search.php?nq=1&query_type=call_number&query=2265">Edison Amberol: 28017</a>. 1912. </li>
    <li>Damnazione di Faust. Invocazione delle rose [Damnation 
      de Faust. Voici des roses] / Hector Berlioz. Angelo Scandiani. <a href="search.php?nq=1&query_type=call_number&query=2290">Edison Amberol: 7523</a>. 1910-1912. </li>
    <li>Forza del destino. O tu che in seno agli angeli / 
      Giuseppe Verdi. Carlo Albani. <a href="search.php?nq=1&query_type=call_number&query=2292">Edison Amberol: 30042</a>. 1910. </li>
    <li>Wie Eiskalt ist dies Handchen: La Boheme [Boh&egrave;me. 
      Che gelida manina. German] / Giacomo Puccini. Leo Slezak. <a href="search.php?nq=1&query_type=call_number&query=2322">Edison Amberol: B158</a>. 1909. </li>
    <li>Gegruesst sei mir (cavatine) [Faust. Salut, demeure 
      chaste et pure] / Charles Gounod. Leo Slezak. <a href="search.php?nq=1&query_type=call_number&query=2337">Edison Amberol: 40041</a>. 1911. </li>
    <li>Elisir d'amore. Furtiva lagrima / Gaetano Donizetti. 
      Aristodemo Giorgini. <a href="search.php?nq=1&query_type=call_number&query=2358">Edison Amberol: 30037</a>. 1910. </li>
    <li>Carmen. Romance de la fleur [Carmen. Fleur que tu 
      m'avais jet&eacute;e] / Georges Bizet. Florencio Constantino. <a href="search.php?nq=1&query_type=call_number&query=2367">Edison Amberol: B184</a>. 1910. </li>
    <li>Oh! Don't it tickle you?. Will F. Denny. <a href="search.php?nq=1&query_type=call_number&query=2470">Edison Gold Moulded Record: 7186</a>. 1902?. </li>
    <li>The widow Dooley. Ada Jones and Len Spencer. <a href="search.php?nq=1&query_type=call_number&query=2484">Edison Standard Record: 10017</a>. 1908. </li>
    <li>Annie Laurie / Lady John Douglas Scott. Edison Male 
      Quartet. <a href="search.php?nq=1&query_type=call_number&query=2493">Edison Gold Moulded Record: 2201</a>. 1902?. </li>
    <li>Parody on widow's plea for her son. Will F. Denny. 
      <a href="search.php?nq=1&query_type=call_number&query=2510">Edison Gold Moulded Record: 6602</a>. 1902?. </li>
    <li>The Wedding o' Sandy MacNab / Sir Harry Lauder. Sir 
      Harry Lauder. <a href="search.php?nq=1&query_type=call_number&query=2532">Edison Gold Moulded Record: 13742</a>. 1908. </li>
    <li>Fou the noo / Sir Harry Lauder. Sir Harry Lauder. 
      <a href="search.php?nq=1&query_type=call_number&query=2533">Edison Gold Moulded Record: 13743</a>. 1908. </li>
    <li>Der musikalische Clown / Carl L&uuml;dicke. Carl L&uuml;dicke. 
      <a href="search.php?nq=1&query_type=call_number&query=2560">Edison Goldguss Walze: 15465</a>. 1907. </li>
    <li>Nur a Bier will i hab'n / August Junker. August Junker. 
      <a href="search.php?nq=1&query_type=call_number&query=2566">Edison Goldguss Walze: 15742</a>. 1907. </li>
    <li>Die Welt ist wie ein Huhnerstall / Carl Bretschneider. 
      Gustav Sch&ouml;nwald. <a href="search.php?nq=1&query_type=call_number&query=2584">Edison Goldguss Walzes: 15553</a>. 1907. </li>
    <li>Hey, Donal! / Sir Harry Lauder. Sir Harry Lauder. 
      <a href="search.php?nq=1&query_type=call_number&query=2599">Edison Gold Moulded Record: 13741</a>. 1908. </li>
    <li>Souvenir de Carmen / Victor Mass&eacute;. B&eacute;rard. 
      <a href="search.php?nq=1&query_type=call_number&query=2606">Cylindres Edison Moul&eacute;s sur Or: 18050</a>. 1908. </li>
    <li>Bay State quickstep. Vess L. Ossman. <a href="search.php?nq=1&query_type=call_number&query=2638">Edison Gold Moulded Record: 7955</a>. 1902?. </li>
    <li>Social and industrial justice / Theodore Roosevelt. 
      Theodore Roosevelt. <a href="search.php?nq=1&query_type=call_number&query=2682">Edison Blue Amberol: 3709</a>. 1919. </li>
    <li>Hickory Bill / Len Spencer. Len Spencer and Fred Van 
      Eps. <a href="search.php?nq=1&query_type=call_number&query=2713">Edison Gold Moulded Record: 8580</a>. 1907. </li>
    <li>Hawaiian twilight / Carl D. Vandersloot. Waikiki Hawaiian 
      Orchestra. <a href="search.php?nq=1&query_type=call_number&query=2773">Edison Blue Amberol: 4138</a>. 1920. </li>
    <li>Cavalleria rusticana. Intermezzo; arr. / Pietro Mascagni. 
      Edith Helena. <a href="search.php?nq=1&query_type=call_number&query=2803">Edison Gold Moulded Record: 9556</a>. 1907. </li>
    <li>Rock of ages / Thomas Hastings. Albert Campbell and 
      James F. Harrison. <a href="search.php?nq=1&query_type=call_number&query=2810">Edison Gold Moulded Record: 8839</a>. 1904. </li>
    <li>An autumn evening / Samuel Siegel. Samuel Siegel and 
      M. Loyd Wolf. <a href="search.php?nq=1&query_type=call_number&query=2829">Edison Gold Moulded Record: 9014</a>. 1905. </li>
    <li>Take a car / Ed Rose. Arthur Collins and Byron G. 
      Harlan. <a href="search.php?nq=1&query_type=call_number&query=2846">Edison Gold Moulded Record: 9055</a>. 1905. </li>
    <li>Kinderscenen. Tra&uuml;merei; arr. / Robert Schumann. 
      Hans Kronold. <a href="search.php?nq=1&query_type=call_number&query=2886">Edison Gold Moulded Record: 9149</a>. 1905. </li>
    <li>My country 'tis of thee. Edison Male Quartet. <a href="search.php?nq=1&query_type=call_number&query=2955">Edison Gold Moulded Record: 8410</a>. 1903. </li>
    <li>Sing, smile, slumber [S&eacute;r&eacute;nade; arr.] 
      / Charles Gounod. Bohumir Kryl. <a href="search.php?nq=1&query_type=call_number&query=2958">Edison Gold Moulded Record: 8418</a>. 1904-1908. </li>
    <li>Santiago / A. Corbin. Albert Benzler. <a href="search.php?nq=1&query_type=call_number&query=2971">Edison Gold Moulded Record: 8473</a>. 1906. </li>
    <li>The minstrel boy. James C. McAuliffe. <a href="search.php?nq=1&query_type=call_number&query=2973">Edison Gold Moulded Record: 8487</a>. 1904-1908. </li>
    <li>The auto race / Jean M. Missud. Edison Concert Band. 
      <a href="search.php?nq=1&query_type=call_number&query=2980">Edison Gold Moulded Record: 8856</a>. 1905. </li>
    <li>The Dixie rube / Thos. S. Allen. Edison Military Band. 
      <a href="search.php?nq=1&query_type=call_number&query=3045">Edison Gold Moulded Record: 9241</a>. 1906. </li>
    <li>The umpire is a most unhappy man / Joseph Edgar Howard. 
      Edward M. Favor. <a href="search.php?nq=1&query_type=call_number&query=3107">Edison Gold Moulded Record: 9352</a>. 1906. </li>
    <li>You'll have to get off and walk / David Reed. Will 
      F. Denny. <a href="search.php?nq=1&query_type=call_number&query=3235">Edison Gold Moulded Record: 9568</a>. 1907. </li>
    <li>Upper ten and lower five / James Thornton. Edward 
      M. Favor and Edward Meeker. <a href="search.php?nq=1&query_type=call_number&query=3292">Edison Gold Moulded Record: 9775</a>. 1908. </li>
    <li>Sheriff's sale of a stranded circus. Len Spencer and 
      Gilbert Girard. <a href="search.php?nq=1&query_type=call_number&query=3294">Edison Gold Moulded Record: 9779</a>. 1908. </li>
    <li>I'm afraid to come home in the dark / Egbert Van Alstyne. 
      Billy Murray. <a href="search.php?nq=1&query_type=call_number&query=3295">Edison Gold Moulded Record: 9780</a>. 1908. </li>
    <li>Si and Sis, the musical spoons. Ada Jones and Len 
      Spencer. <a href="search.php?nq=1&query_type=call_number&query=3313">Edison Gold Moulded Record: 9815</a>. 1908. </li>
    <li>Kerry Mills' barn dance / Kerry Mills. Edison Symphony 
      Orchestra. <a href="search.php?nq=1&query_type=call_number&query=3353">Edison Gold Moulded Record: 9909</a>. 1908. </li>
    <li>The tariff question / William Jennings Bryan. William 
      Jennings Bryan. <a href="search.php?nq=1&query_type=call_number&query=3355">Edison Gold Moulded Record: 9918</a>. 1908. </li>
    <li>Guaranty of bank deposits / William Jennings Bryan. 
      William Jennings Bryan. <a href="search.php?nq=1&query_type=call_number&query=3356">Edison Gold Moulded Record: 9921</a>. 1908. </li>
    <li>Don't take me home / Harry Von Tilzer. Ed Morton. 
      <a href="search.php?nq=1&query_type=call_number&query=3371">Edison Gold Moulded Record: 9949</a>. 1908. </li>
    <li>Republican and Democratic treatment of trusts / William 
      Howard Taft. William Howard Taft. <a href="search.php?nq=1&query_type=call_number&query=3414">Edison Gold Moulded Record: 9998</a>. 1908. </li>
    <li>Theodore / Vincent Bryan. Edward M. Favor. <a href="search.php?nq=1&query_type=call_number&query=3429">Edison Gold Moulded Record: 9630</a>. 1907. </li>
    <li>I'd rather two-step than waltz, Bill (take 2) / Benjamin 
      Hapgood Burt. Billy Murray. <a href="search.php?nq=1&query_type=call_number&query=3433">Edison Gold Moulded Record: 9634</a>. 1907. </li>
    <li>I'm in love with the slide trombone / Seymour Furth. 
      Ada Jones. <a href="search.php?nq=1&query_type=call_number&query=3443">Edison Gold Moulded Record: 9652</a>. 1907. </li>
    <li>Ev'rything's funny to me (take 2) / Theodore H. Northrup. 
      Sallie Stembler. <a href="search.php?nq=1&query_type=call_number&query=3529">Edison Standard Record: 10081</a>. 1909. </li>
    <li>Sterling Castle and Harvest home. William Craig. <a href="search.php?nq=1&query_type=call_number&query=3571">Edison Standard Record: 10120</a>. 1909. </li>
    <li>Uncle Josh at a baseball game / Cal Stewart. Cal Stewart. 
      <a href="search.php?nq=1&query_type=call_number&query=3613">Edison Standard Record: 10169</a>. 1909. </li>
    <li>Go easy Mabel / J. Fred. Helf. Edward Meeker. <a href="search.php?nq=1&query_type=call_number&query=3616">Edison Standard Record: 10173</a>. 1909. </li>
    <li>Scotch reels. Alexander Prince. <a href="search.php?nq=1&query_type=call_number&query=3636">Edison Standard Record: 10200</a>. 1909. </li>
    <li>Medley of Irish reels. John J. Kimmel. <a href="search.php?nq=1&query_type=call_number&query=3689">Edison Standard Record: 10284</a>. 1909. </li>
    <li>I'm looking for something to eat / Billie Taylor. 
      Stella Mayhew. <a href="search.php?nq=1&query_type=call_number&query=3701">Edison Standard Record: 10298</a>. 1909. </li>
    <li>Sun of my soul / Peter Ritter. Harry Anthony and James 
      F. Harrison. <a href="search.php?nq=1&query_type=call_number&query=3715">Edison Standard Record: 10322</a>. 1910. </li>
    <li>The birlin' reels. James Scott Skinner. <a href="search.php?nq=1&query_type=call_number&query=3768">Edison Standard Record: 10402</a>. 1910. </li>
    <li>The hoosier slide / Hale Ascher VanderCook. National 
      Military Band (London, England). <a href="search.php?nq=1&query_type=call_number&query=3773">Edison Standard Record: 10409</a>. 1910. </li>
    <li>Nix on the glow-worm, Lena! / Harry Carroll. Billy 
      Murray. <a href="search.php?nq=1&query_type=call_number&query=3798">Edison Standard Record: 10437</a>. 1910. </li>
    <li>Florentiner-Marsch / Julius Fu&#269;&iacute;k. Sousa 
      Band. <a href="search.php?nq=1&query_type=call_number&query=3875">Edison Standard Record: 10546</a>. 1911. </li>
    <li>The armourer's song [Robin Hood. Selections] / Reginald 
      De Koven. Frank C. Stanley. <a href="search.php?nq=1&query_type=call_number&query=3936">Indestructible Record: 698</a>. 1908. </li>
    <li>Blitz and Blatz at the sea shore. Bob Roberts and 
      Fred Duprez. <a href="search.php?nq=1&query_type=call_number&query=3999">Indestructible Record: 1160</a>. 1909. </li>
    <li>Miss Trombone / Henry Fillmore. Indestructible Military 
      Band. <a href="search.php?nq=1&query_type=call_number&query=4062">Indestructible Record: 1473</a>. 1911. </li>
    <li>With shot and shell / Benjamin Bilse. United States 
      Marine Band. <a href="search.php?nq=1&query_type=call_number&query=4238">Edison Standard Record: 10531</a>. 1911. </li>
    <li>Auld lang syne / Robert Burns. Frank C. Stanley. <a href="search.php?nq=1&query_type=call_number&query=4410">Indestructible Record: 1267</a>. 1910. </li>
    <li>Air des contrebandiers [Carmen. &Eacute;coute, compagnon] 
      / Georges Bizet. Lucien Muratore. <a href="search.php?nq=1&query_type=call_number&query=4527"> Cylindres Edison Moul&eacute;s Sur Or: 17590</a>. 1907-1909. </li>
    <li>Rigoletto. Questa o quella per me pari sono / Giuseppe 
      Verdi. Romeo Berti. <a href="search.php?nq=1&query_type=call_number&query=4582">Edison Gold Moulded Record: B. 18</a>. 1906. </li>
    <li>Tarantella [Soir&eacute;es musicales. Danza.] / Gioacchino 
      Rossini. Giuseppe Campanari. <a href="search.php?nq=1&query_type=call_number&query=4600">Edison Gold Moulded Record: B. 46</a>. 1907. </li>
    <li>A tanto amor: La Favorita [Favorite. Pour tant d'amour] 
      / Gaetano Donizetti. Mario Ancona. <a href="search.php?nq=1&query_type=call_number&query=4604">Edison Gold Moulded Record: B. 50</a>. 1907. </li>
    <li>Il fior che avevi a me tu dato [Carmen. Fleur que 
      tu m'avais jet&eacute;e] / Georges Bizet. Angiolo Pintucci. <a href="search.php?nq=1&query_type=call_number&query=4612">Edison Gold Moulded Record: B. 112</a>. 1908. </li>
    <li>Chargez / Charles Merelly. B&eacute;rard. <a href="search.php?nq=1&query_type=call_number&query=4646">Cylindres Edison Moul&eacute;s Sur Or: 17782</a>. 1907. </li>
    <li>Serenata de los maridos [Boccaccio. Selections; arr.] 
      / Franz von Supp&eacute;. Antonio Vargas. <a href="search.php?nq=1&query_type=call_number&query=4658">Edison Gold Moulded Record: 12152</a>. ca. 1902. </li>
    <li>Macario Romero. Rafael Herrera Robinson and Senor 
      Picazo. <a href="search.php?nq=1&query_type=call_number&query=4668">Edison Gold Moulded Record: 18638</a>. 1905. </li>
    <li>Hawthorne Club. Samuel Siegel. <a href="search.php?nq=1&query_type=call_number&query=4671">Columbia Phonograph Co.: 31389</a>. 1904-1909. </li>
    <li>The colored major. Vess L. Ossman and William B. Farmer. 
      <a href="search.php?nq=1&query_type=call_number&query=4672">Columbia Phonograph Co.: 31590</a>. 1904-1909. </li>
    <li>Independentia march / Robert Browne Hall. Joseph Belmont. 
      <a href="search.php?nq=1&query_type=call_number&query=4686">Columbia Phonograph Co.: 31840</a>. 1904-1909. </li>
    <li>Darling Nellie Gray / Benjamin Russel Hanby. Columbia 
      Quartette. <a href="search.php?nq=1&query_type=call_number&query=4787">Columbia Phonograph Co.: 32836</a>. 1905. </li>
    <li>Brother Masons. Frank Williams. <a href="search.php?nq=1&query_type=call_number&query=4788">Columbia Phonograph Co.: 32849</a>. 1906. </li>
    <li>A barnyard serenade. Len Spencer and Alfred Holt. 
      <a href="search.php?nq=1&query_type=call_number&query=4817">Columbia Phonograph Co.: 33000</a>. 1906. </li>
    <li>Nobody: Abyssinia / Bert Williams. Bert Williams. 
      <a href="search.php?nq=1&query_type=call_number&query=4821">Columbia Phonograph Co.: 33011</a>. 1906. </li>
    <li>Stars and stripes forever march / John Philip Sousa. 
      Sousa Band. <a href="search.php?nq=1&query_type=call_number&query=4857">Columbia Phonograph Co.: 532</a>. 1904-1909. </li>
    <li>Under the double eagle [Unter dem Doppeladler] (copy 
      2) / Josef Franz Wagner. Gilmore's Band. <a href="search.php?nq=1&query_type=call_number&query=4870">Columbia Phonograph Co.: 1564</a>. 1904-1909. </li>
    <li>A Bunch of rags. Vess L. Ossman. <a href="search.php?nq=1&query_type=call_number&query=4882">Columbia Phonograph Co.: 3861</a>. 1904-1909. </li>
    <li>Medley of popular airs. Charles P. Lowe and the Columbia 
      Symphony Orchestra. <a href="search.php?nq=1&query_type=call_number&query=4914">Columbia Phonograph Co.: 15503</a>. 1904-1909. </li>
    <li>Zacatecas marcha / Genaro Codina. Banda Espa&ntilde;ola. 
      <a href="search.php?nq=1&query_type=call_number&query=4916">Columbia Phonograph Co.: 40322</a>. 1904-1909. </li>
    <li>Hipp hipp hurrah. Orchester m. Gesang. <a href="search.php?nq=1&query_type=call_number&query=4927">Columbia Phonograph Co.: 55031</a>. 1904-1909. </li>
    <li>Russian kamarinskaja [Kamarinskai] / Mikhail Ivanovich 
      Glinka. Accordion solo. <a href="search.php?nq=1&query_type=call_number&query=4929">Columbia Phonograph Co.: 65061</a>. 1904-1909.</li>
  </ul>

</div><!-- end .content -->
<!-- End of file: featured.tpl -->   
{include file="footer.tpl"}