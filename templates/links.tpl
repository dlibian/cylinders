{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: links.tpl -->
<div class="content text">
	<h1>Recommended Websites</h1>
	<p>
		(See <a href="history-bibliography.php">cylinder bibliography</a>
		for recommended books)
	</p>
	<h3>General Cylinder Recording History</h3>
	<ul>
	  <li>
	  	<a href="http://www.gracyk.com/cylinders.shtml">Phonograph Cylinders: A Beginner's Guide (Tim Gracyk)</a>
	  </li>
	  <li>
	  	<a href="http://www.tinfoil.com/earlywax.htm">Early Recorded Sounds and Wax Cylinders History (Glenn Sage)</a>
	  </li>
	  <li>
	  	<a href="http://inventors.about.com/library/inventors/bledisondiscphpgraph.htm"> History of the Edison Cylinder Phonograph (About.com)</a>
	  </li>
	  <li>
	  	<a href="http://www.aes.org/aeshc/docs/recording.technology.history/notes.html">Recording Technology History (Steve Schoenherr)</a>
	  </li>
	</ul>
	<h3> Digitized and Web-Accessible Early Recordings</h3>
	<ul>
	  <li>
	  	<a href="http://www.nps.gov/edis/photosmultimedia/the-recording-archives.htm">Edison National Historic Site</a>
	  </li>
	  <li>
	  	<a href="http://www.collectionscanada.ca/gramophone/index-e.html">Library and Archives Canada -Virtual Gramophone</a>
	  </li>
	  <li>
	  	<a href="http://victor.library.ucsb.edu/"></a><a href="http://www.loc.gov/jukebox/">Library of Congress - National Jukebox</a>
	  </li>
	  <li><a href="http://memory.loc.gov/">Library of Congress American Memory Project</a>
	    <ul>
	      <li>
	      	<a href="http://memory.loc.gov/ammem/berlhtml/berlhome.html">Emile Berliner and the Birth of the Recording Industry</a>
	      </li>
	      <li>
	      	<a href="http://www.loc.gov/collection/edison-company-motion-pictures-and-sound-recordings/about-this-collection/">Inventing Entertainment: the Early Motion Pictures and Sound Recordings of the Edison Companies</a>
	      </li>
	    </ul>
	  </li>
	  <li>
	  	<a href="http://www.pas.org/resources/research/gerhardt-cylinder-recordings">Percussive Arts Society-Gerhardt Collection</a>
	  </li>
	  <li>
	  	<a href="https://library.syr.edu/scrc/collections/digitalasset/cylinders.php">Syracuse University - Belfer Cylinders Digital Collection</a>
	  </li>
	  <li>
	  	<a href="http://www.tinfoil.com/">Tinfoil.com</a>
	  </li>
	  <li>
	  	<a href="http://adp.library.ucsb.edu/">UC Santa Barbara - Discography of American Historical Recordings</a>
	  </li>
	  <li>
	  	WFMU - <a href="http://wfmu.org/playlists/te">Thomas Edison's Attic </a>and <a href="http://wfmu.org/playlists/AP">Antique Phonograph Music</a>
	  </li>
	</ul>
	<h3>Phonographs and Historic Instruments</h3>
	<ul>
	  <li>
	  	<a href="http://www.archeophone.org/warcheophone_specifications.php">The Archeophone (Henri Chamoux)</a>
	  </li>
	  <li>
	  	<a href="http://www.christerhamp.se/phono/">Phonograph Makers' Pages (Christer Hamp)</a>
	  </li>
	</ul>
	<h3> Online Cylinderographies (&quot;Discographies&quot; of Cylinders)</h3>
	<ul>
	  <li>
	    <a href="http://www.truesoundtransfers.de/disco.htm">The Truesound Online Discography Project</a> (Edison cylinders)
	  </li>
	  <li>
	    <a href="https://archive.org/details/KastlemusickColumbiaCylinders">Columbia
            &quot;XP&quot; Cylinder Records (1901-1909)</a> (PDF format)
	    </li>
	</ul>
	<h3> Archives with Cylinder Recordings</h3>
	<ul>
		<li><a href="http://www.loc.gov/folklife/">American Folklife Center at the Library of Congress</a> </li>
    <li>
    	<a href="https://theautry.org/research-collections/library-and-archives-autry">Libraries and Archives of the Autry (Lummis Collection)</a>
    </li>
    <li>
    	<a href="http://www.bnf.fr/en/collections_and_services/arch_son_eng/s.records.html">Biblioth&egrave;que nationale de France Sound Archives</a>
    </li>
	  <li>
	  	<a href="http://www.bl.uk/soundarchive">British Library, National Sound Archive</a>
	  </li>
	  <li>
	  	<a href="http://hearstmuseum.berkeley.edu/collections/media">Phoebe A. Hearst Museum of Anthropology</a>
	  </li>
	  <li>
	  	<a href="http://www.indiana.edu/%7Elibarchm/">Indiana University Archives of Traditional Music</a>
	  </li>
	  <li>
	  	<a href="http://www.loc.gov/rr/record/">Library of Congress Recorded Sound Reference Center</a>
	  </li>
	  <li>
	  	<a href="http://www.collectionscanada.gc.ca/music-performing-arts/index-e.html">Library and Archives Canada</a>
	  </li>
	  <li>
	  	<a href="http://www-sul.stanford.edu/depts/ars/">Stanford University Archive of Recorded Sound</a>
	  </li>
	  <li>
	  	<a href="https://library.syr.edu/scrc/collections/preservation/belfer-lab.php">Syracuse University-Belfer
          Audio Laboratory</a>
	  </li>
	</ul>
	<h3> Organizations</h3>
	<ul>
	  <li>
	  	<a href="http://www.arsc-audio.org/">Association for Recorded Sound Collections (ARSC)</a>
	  </li>
	  <li>
	  	<a href="http://www.iasa-web.org/">International Association of Sound and Audiovisual Archives (IASA)</a>
	  </li>
	</ul>
	<h3>Preserving and Handling Sound Recordings</h3>
	<ul>
	  <li>
	  	<a href="http://www.loc.gov/preserv/care/record.html">Preserving Your Sound Recordings (Library of Congress)</a>
	  </li>
	  <li>
	  	<a href="http://cool.conservation-us.org/bytopic/audio/">Audio Preservation (CoOL)</a>
	  </li>
	</ul>
	<h3>Websites with Historical Information About Edison</h3>
	<ul>
	  <li>
	  	<a href="http://www.nps.gov/edis/index.htm">Edison National Historic Site</a>
	  </li>
	  <li>
	  	<a href="http://www.tomedison.org/">Edison Birthplace Museum</a>
	  </li>
	  <li>
	  	<a href="http://edison.rutgers.edu/">The Thomas A. Edison Papers at Rutgers University</a>
	  </li>
	  <li>
	  	<a href="https://www.thehenryford.org/collections-and-research/digital-collections/artifact/179489/">Thomas A. Edison and the Menlo Park Laboratory at the Henry Ford</a>
	  </li>
	</ul>
</div><!-- end .content -->
<!-- End of file: links.tpl -->
{include file="footer.tpl"}
