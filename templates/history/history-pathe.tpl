{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: history-pathe.tpl -->
<div class="content text">
  <h2>Path&eacute; Cylinders</h2>

  <figure>
    <img src="/images/pathesalon.jpg" title="Pathé Salon Cylinder" alt="Mme. Gregoire / Pierre Jean de Béranger. Andre Maréchal. Pathé: 3426. 1902 or 1903." /><figcaption>Mme. Gregoire / Pierre Jean de Béranger. Andre Maréchal. <a href="search.php?nq=1&query_type=call_number&query=6056">Pathé: 3426</a>. 1902 or 1903.</figcaption>
  </figure>

  <p>
    Path&eacute; was a French company that produced both motion
    pictures and sound recordings in the late 19th and early 20th centuries.
    The history of Path&eacute; cylinders is not well documented in English,
    but it is believed that the company may have produced as many titles as Columbia or Edison,
    though they were not widely imported and are relatively scarce in the
    United States. Path&eacute;'s recording process was unique in that the
    recordings were originally made on large &quot;master cylinders&quot;
    and then dubbed to various disc and cylinder formats. The dubbing process
    sometimes resulted in inferior sound compared to that of Edison cylinders.
    In addition to standard 2 1/4&quot; x 4&quot; cylinders, Path&eacute;
    also manufactured 3 1/2&quot; x 4&quot; Salon cylinders, pictured above. Unlike the musical repertoire
    and performers found on American cylinders which differed very little from company to company,
    the repertoire on Path&eacute; cylinders is exclusively French and totally
    different from that of the U.S. companies. Click <a href="search.php?nq=1&query=pathe&query_type=keyword">here</a>
    for examples of Path&eacute; cylinders in the collection.
  </p>  <div class="pagination">
  	<span class="previous">Previous: <a href="history-lambert.php" title="Previous">Lambert Cylinders</a></span>
    <i class="small-screen fa fa-ellipsis-h"></i>
  	<span class="next">Next: <a href="history-edisonbell.php" title="Next">Edison Bell Cylinders</a></span>
  </div>
</div><!-- end .content -->
<!-- End of file: history-pathe.tpl -->
{include file="footer.tpl"}
