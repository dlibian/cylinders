{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: history-everlasting.tpl -->
<div class="content text">
  <h2>U.S. Everlasting Cylinders</h2>

  <figure>
    <img src="/images/useverlasting.jpg" title="US Everlasting Cylinder" alt="I&#39;ve got your number / Ada Jones. U.S. Everlasting Records: 385. 1908." /><figcaption>I've got your number / Ada Jones. <a href="search.php?nq=1&query_type=call_number&query=6632">U.S. Everlasting Records: 385</a>. 1908.</figcaption>
  </figure>

  <p>
    The U.S. Phonograph Company of Cleveland, Ohio, produced both two- and
    four-minute cylinders under its own label&mdash;<a href="/search.php?nq=1&query_type=keyword&amp;query=U.S.+everlasting+record">U.S. Everlasting Records</a>&mdash;and under the Lakeside label for Montgomery Ward
    department stores. Between 1908 and 1912, more than 1,000 titles were
    released in three series: popular, foreign language, and grand opera.  </p>
  <p>
    Like Edison Blue Amberols and Indestructible cylinders, Everlasting cylinders
    were made of celluloid, but their sound quality is superior to both of them.
  </p>
  <div class="pagination">
  	<span class="previous">Previous: <a href="history-indestructible.php" title="Previous">Indestructible Cylinders</a></span>
    <i class="small-screen fa fa-ellipsis-h"></i>
  	<span class="next">Next: <a href="history-lambert.php" title="Next">Lambert Cylinders</a></span>
  </div>
</div><!-- end .content -->
<!-- End of file: history-everlasting.tpl -->
{include file="footer.tpl"}
