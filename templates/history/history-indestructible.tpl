{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: history-indestructible.tpl -->
<div class="content text">
  <h2>Indestructible Records</h2>

  <figure>
    <img src="/images/indestructible.jpg" title="Indestructible Record" alt="Hawiian hula. Helen Louise and Frank Ferera. Indestructible Record: 3407. 1917." /><figcaption>Hawiian hula. Helen Louise and Frank Ferera. 
    <a href="search.php?nq=1&query_type=call_number&query=4135">Indestructible Record: 3407</a>. 1917.</figcaption>
  </figure>

  <p>
    The Indestructible Phonograph Co. of Albany, New York, produced
    molded celluloid cylinders beginning in 1907. The company made both two-
    and four-minute cylinders, and the repertoire was similar to that of Edison
    and Columbia cylinders. The cylinders had a thick cardboard core and metal
    rim ends to keep them rigid. Production continued until 1922, and cylinders
    were issued both under the 
    <a href="search.php?nq=1&query=Indestructible+Record&query_type=keyword">Indestructible Record</a> label and also for Sears, Roebuck, and Co. as Oxford Indestructible
    Records, as pictured above. From 1908 to 1912 they were also distributed by the Columbia
    Phonograph Co.
  </p>
  <p>
    Regardless of the brand under which they were sold, Indestructible
    cylinders all have the same content for a given catalog number and
    carry no company identification on the cylinder itself,  so if they have become separated from
    their original boxes, the different brands are indistinguishable.
  </p>  <div class="pagination">
  	<span class="previous">Previous: <a href="history-busybee.php" title="Previous">Busy-Bee Cylinders</a></span>
    <i class="small-screen fa fa-ellipsis-h"></i>
  	<span class="next">Next: <a href="history-everlasting.php" title="Next">U.S Everlasting Cylinders</a></span>
  </div>
</div><!-- end .content -->
<!-- End of file: history-indestructible.tpl -->
{include file="footer.tpl"}
