{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: history-bibliography.tpl -->
<div class="content text">
  <h2>Further Reading</h2>
  <ul>
    <li><p>Anton, Julien. <a href="http://worldcatlibraries.org/wcpa/oclc/74666929">Henri Lioret: clockmaker and phonograph pioneer</a> [Paris, France?: Phonogalerie?, 2006?] (English translation by Mark Yates, also available in the original <a href="http://worldcatlibraries.org/wcpa/oclc/74496776">French edition</a>).</p></li>
    <li>
      <p>Brady, Erika. <a href="http://worldcatlibraries.org/wcpa/oclc/41035475">A spiral way: how the phonograph changed ethnography</a>. Jackson, Miss.: University Press of Mississippi, 1999.</p>
    </li>
    <li><p>Deakins, Duane D. <a href="http://worldcatlibraries.org/wcpa/oclc/9678038">Comprehensive cylinder record index</a> (5 vol) Stockton, CA, 1957-</p></li>
    <li><p>Dethlefson, Ronald. <a href="http://worldcatlibraries.org/wcpa/oclc/46888574">Edison Blue Amberol recordings</a>. (2 vol) Woodland Hills, CA: Stationery X-Press, 1997-1999.</p></li>
    <li><p>Dethlefson, Ronald and George A. Copeland. <a href="http://worldcatlibraries.org/wcpa/oclc/57599849">Edison, Lambert concert records &amp; Columbia Grand records and related phonographs: or, &quot;the 5-inch cylinder book&quot;</a> Los Angeles, CA : Mulholland Press, 2004.</p></li>
    <li>
      <p><a href="http://worldcatlibraries.org/wcpa/oclc/4254820">Edison phonograph monthly (1903&ndash;1916)</a>. Orange, N.J. National Phonograph Co. Reprint by Wendell Moore, 1976.</p>
    </li>
    <li>
      <p>Fabrizio, Timothy C. and Paul, George F. <a href="http://worldcatlibraries.org/wcpa/oclc/36207534">The talking machine: An illustrated compendium, 1877&ndash;1929</a>. Atglen, PA: Schiffer Pub., 1997.</p>
    </li>
    <li>
      <p>Frow, George. <a href="http://worldcatlibraries.org/wcpa/oclc/34068412">Edison cylinder phonograph companion, 1877&ndash;1929</a>. Woodland Hills, CA : Stationary X-Press, 1994.</p>
    </li>
    <li><p>Gibson, Gerald. <a href="http://worldcatlibraries.org/wcpa/oclc/41473726">Cylinder audio recordings: an annotated bibliography</a>. Washington, DC, Library of Congress, Preservation Directorate, 1999.</p></li>
    <li><p>Hughbanks, Leroy. <a href="http://worldcatlibraries.org/wcpa/oclc/2935458">Talking wax, or, the story of the phonograph: simply told for general readers</a>. New York: Hobson Book Press, 1945.</p></li>
    <li>
      <p>Koenigsberg, Allen. <a href="http://worldcatlibraries.org/wcpa/oclc/17022377">Edison cylinder records, 1889&ndash;1912: with an illustrated history of the phonograph</a>. 2nd ed. Brooklyn, N.Y.: APM Press, 1987.</p>
    </li>
    <li><p>Millard, A. J. <a href="http://worldcatlibraries.org/wcpa/oclc/61260115">America on record: A history of recorded sound</a>. 2nd ed. New York, N.Y.: Cambridge University Press, 2005.</p></li>
    <li><p>Nauck, Kurt R. and Allan Sutton. <a href="http://worldcatlibraries.org/wcpa/oclc/763461499">Indestructible and U-S everlasting cylinders: an illustrated history and cylinderography</a>. Denver: Mainspring Press, 2011.</p></li>
    <li><p>Rondeau, René. <a href="http://worldcatlibraries.org/wcpa/oclc/48236318">Tinfoil phonographs: the dawn of recorded sound</a>. Corte Madera, CA. : R. Rondeau, 2001.</p></li>
    <li><p>Schoenherr, Steve. Recording Technology History. <a href="http://www.aes.org/aeshc/docs/recording.technology.history/notes.html" class="uri">http://www.aes.org/aeshc/docs/recording.technology.history/notes.html</a>. Accessed 8/17/2012.</p></li>
    <li><p>Shambarger, Peter. &quot;Cylinder Records: An Overview.&quot; <a href="http://worldcatlibraries.org/wcpa/oclc/15355304">ARSC Journal</a>. Volume 26, no. 2. Fall 1995.</p></li>
    <li>
      <p>Sutton, Allan.<a href="http://worldcatlibraries.org/wcpa/oclc/869568479">Edison Amberol cylinders: U.S. and foreign issues, 1908&ndash;1913</a>. Denver: Mainspring Press, 2013.</p>
    </li>
    <li>
      <p>Sutton, Allan. <a href="http://worldcatlibraries.org/wcpa/oclc/636010461">Edison Blue Amberol cylinders: U.S., special, and foreign issues (1912&ndash;1929)</a>. Denver: Mainspring Press, 2009.</p>
    </li>
    <li><p>Wile, Raymond R. &quot;Cylinder Record Materials.&quot; <a href="http://worldcatlibraries.org/wcpa/oclc/15355304">ARSC Journal</a>. Volume 26, no. 2. Fall 1995.</p></li>
  </ul>

  <div class="pagination">
  	<span class="previous">Previous: <a href="history-sterling.php" title="Next">Sterling Cylinders</a></span>
  </div>

</div><!-- end .content -->
<!-- End of file: history-bibliography.tpl -->
{include file="footer.tpl"}
