{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: history-sterling.tpl -->
<div class="content text">
	<h2>Sterling Cylinders</h2>

  <figure>
    <img src="/images/sterling.jpg" title="Sterling Cylinder" alt="When other lips [Bohemian girl. Then you&#39;ll remember me] / Balfe, M. W. Ernest Pike. Sterling Record: 414. 1906." /><figcaption>When other lips [Bohemian girl. Then you'll remember me] / Balfe, M. W. Ernest Pike. <a href="search.php?nq=1&query_type=call_number&query=5032">Sterling Record: 414</a>. 1906.</figcaption>
  </figure>

  <p>
	Upon the expiration of Edison Bell's patents in England, various manufacturers began
  	selling cylinders in England, including Sterling.  Sterling  was founded in 1904 by
  	Louis Sterling, an American who had moved to England and was employed by the British Zonophone Co.
	He resigned in 1904 and started the Sterling Co.; in 1905 he formed a  partnership with Russell Hunting,
	who became recording director of the company, renamed the Russell Hunting Co. Ltd., the manufacturer of
  	Sterling cylinders. Hunting was the   American entertainer famous for his recording of
  	<a href="/search.php?nq=1&query_type=call_number&query=5348">Casey at the Bat</a>
	who had moved to England in 1901 and become recording director for Edison Bell.  </p>
  <p>
  	<a href="search.php?nq=1&query=sterling+carter&query_type=keyword">Sterling cylinders</a>
  	are typically about 1/4&quot; longer than standard cylinders. In advertisements, the Sterling
  	cylinders were always shown next to the comparatively short and stumpy  standard cylinder.
  </p>
  <p>
  	A noted and unusual recording issued by Sterling was a
  	<a href="/search.php?nq=1&query_type=keyword+&query=pinafore+sterling">1907 recording</a>
  	of Gilbert and Sullivan's <em>H.M.S. Pinafore</em>  on ten cylinders. Very few copies survive,
  	but five of the ten cylinders are in the collection.
  </p>
  <div class="pagination">
  	<span class="previous">Previous: <a href="history-edisonbell.php" title="Previous">Edison Bell Cylinders</a></span>
    <i class="small-screen fa fa-ellipsis-h"></i>
  	<span class="next">Next: <a href="history-bibliography.php" title="Next">Further reading</a></span>
  </div>
    <!--page body text ends here-->
</div><!-- end .content -->
<!-- End of file: history-sterling.tpl -->
{include file="footer.tpl"}
