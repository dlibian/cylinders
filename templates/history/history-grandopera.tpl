{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: history-grandopera.tpl -->
<div class="content text">
  <h2>Edison Grand Opera Cylinders</h2>

  <figure>
    <img src="/images/edisongrandopera.jpg" title="Edison Grand Opera Cylinder" alt="L&#39;Africaine [Africaine. O paradis sorti de l&#39;onde] / Meyerbeer. Sung by Florencio Constantino. Edison Amberol: B-178. 1910." /><figcaption>L'Africaine [Africaine. O paradis sorti de l'onde] / Meyerbeer. Sung by Florencio Constantino. <a href="search.php?nq=1&query_type=call_number&query=2330">Edison Amberol: B-178</a>. 1910.</figcaption>
  </figure>

  <p>
    While well-known overtures and classical  works figured prominently in Edison's Gold-Moulded and Amberol cylinders, performances by professionally trained opera singers were largely issued on several special series of 
<a href="search.php?nq=1&query=Edison+Grand+Opera&query_type=keyword&sortBy=cnum&sortOrder=ia">
Grand Opera Cylinders</a>. The Edison &quot;B&quot; series started as a series of 113 two-minute Gold-Moulded cylinders and eventually continued as a series of four-minute Amberol records. Later series of four-minute Grand Opera cylinders included the 30000, 35000, and 40000  blocks of numbers. Famous performers of the day are found here, including <a href="search.php?nq=1&query=florencio&query_type=keyword">Florencio Constantino</a>, <a href="search.php?nq=1&query=slezak&query_type=keyword">Leo Slezak</a>,  <a href="search.php?nq=1&nq=1&query_type=keyword+&query=Josephine+Jacoby">Josephine Jacoby</a>, and other  well-known opera singers of the time.</p>
  <div class="pagination">
  	<span class="previous">Previous: <a href="history-goldmoulded.php" title="Previous">Edison Amberol Gold-Moulded  Cylinders</a></span>
    <i class="small-screen fa fa-ellipsis-h"></i>
  	<span class="next">Next: <a href="history-blueamberol.php" title="Next">Edison Blue Amberol Cylinders</a></span>
  </div>
</div><!-- end .content -->
<!-- End of file: history-grandopera.tpl -->
{include file="footer.tpl"}
