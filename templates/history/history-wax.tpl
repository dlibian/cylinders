{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: history-wax.tpl -->
<div class="content text">
  <h2>The Earliest Wax Cylinders (1887&ndash;1894)</h2>

  <figure>
    <img src="/images/northamerican.jpg" title="North American Cylinder" alt="Bell buoy / J. W. Myers. North American Phonograph Co [no number]. 1891 or 1892" /><figcaption>Bell buoy / J. W. Myers. <a href="https://www.library.ucsb.edu/OBJID/Cylinder9812">North American Phonograph Co [no number]</a>. 1891 or 1892</figcaption>
  </figure>

    <p>
      After Edison's pioneering work in tinfoil recording, his laboratory focused
      on the invention of the incandescent light bulb and the development of
      the underlying infrastructure that would ultimately support its widespread
      adoption. Where Edison left off, Alexander Graham Bell, his cousin Chichester,
      and Charles Sumner Tainter filled the void and began tinkering with Edison's
      design. By the mid-1880s, their efforts resulted in several key technological
      sound recording developments&mdash;in particular, the establishment of wax
      as the new medium of choice and the design of a cutting stylus that would
      etch a groove, as opposed to just making an indentation, as the tinfoil
      phonograph had done. These advances, which allowed for a more durable
      sound recording of better quality, were crucial for the subsequent development
      of the cylinder medium as well as the immediate commercial viability of
      the three men's cylinder machine, the Graphophone, initially sold through
      the nascent Columbia company. Edison, having initially been approached
      as a collaborator, rebuffed the trio, returning instead to improve upon
      his original invention by himself. By the time the Graphophone debuted
      in 1887, he picked up where the trio's developments left off, determined
      to do even better.
    </p>
    <p>
      Edison's work, as well as his canny marketing abilities, paid off.
      By the late 1890s, following several refinements to the wax medium and
      the cylinder phonograph machine, as well as protracted legal battles with
      Columbia, Edison's brown wax cylinders emerged under the North American
      Phonograph Company name. The cylinders established a previously unseen
      level of market dominance in regional markets across the country, in competition
      against Columbia and Emile Berliner's new disc format. As the legend goes,
      Edison believed the cylinder phonograph would be a smash hit with the
      business community as a convenient office dictation device.&nbsp;Yet the
      success of the musical cylinders quickly convinced Edison otherwise, and
      as the cylinder medium evolved, entertainment became the most lucrative
      use for early sound recording technology.
    </p>  <div class="pagination">
  	<span class="previous">Previous: <a href="history-tinfoil.php" title="Previous">Tinfoil Recordings</a></span>
    <i class="small-screen fa fa-ellipsis-h"></i>
  	<span class="next">Next: <a href="history-lioret.php" title="Next">Lioret Cylinders</a></span>
  </div>
</div><!-- end .content -->
<!-- End of file: history-wax.tpl -->
{include file="footer.tpl"}
