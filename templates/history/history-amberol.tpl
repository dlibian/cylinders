{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: history-amberol.tpl -->
<div class="content text">
  <h2>Edison Amberol Cylinders (1908&ndash;1912)</h2>

  <figure>
    <img src="/images/amberol.jpg" title="Edison Amberol" alt="Bonnie Leezie Lindsay / Harry Lauder. Edison Amberol Record: 12080. 1910." /><figcaption>Bonnie Leezie Lindsay / Harry Lauder. <a href="search.php?nq=1&query_type=call_number&query=6187">Edison Amberol Record: 12080</a>. 1910.</figcaption>
  </figure>

  <p>
    In 1908, Edison introduced the wax Amberol cylinder, boasting
    double the number of grooves on an equivalently sized wax cylinder and
    hence a playing time twice as long as that of the Gold-Moulded cylinder. In
    the coming decade, the four-minute, 200 TPI (threads-per-inch) cylinder
    would replace the previous industry standard of two-minute, 100 TPI cylinders. (For
    identification purposes, Edison Amberols, as opposed to Edison Gold-Moulded
    cylinders, are marked with the "4M" designation, followed by the recording
    number.) Although this new groove pitch allowed for a wider range
    of musical and expressive potential, it seems that many of the songs,
    marches, and vaudeville skits of the earlier cylinder era were simply
    given a facelift&mdash;a new verse here, a verbal or non-musical interlude
    there&mdash;in order to meet the longer time requirements of the new cylinder
    medium.
  </p>
  <p>
    The machines that had originally been designed to play two-minute
    cylinders also had to be refitted to accommodate the new groove pitch. The
    feed screw mechanism found in all acoustic machines of the time allowed
    the reproducing (playback) stylus to be brought into direct alignment
    with the cylinder grooves via a fixed feed screw undergirding it. Consequently,
    these new "4-minute" cylinders would not play back correctly on phonographs
    designed for "2-minute" cylinders. Edison subsequently manufactured a
    machine that enabled one to play cylinders of either groove pitch by simply
    toggling a switch that engaged a stylus designed for 100 or 200 TPI grooves&mdash;the
    Amberola 1A, introduced in 1909. In 1912, though, both the two-minute
    Gold-Moulded and four-minute Amberol recordings were retired, making way
    for the final significant recording medium of the cylinder era.
  </p>
  <p>
    Duane Deakins, an early discographer of cylinder records, lists the following numerical series of four-minute Amberol records issued by Edison:
  </p>
  <table class="stack">
    <tr>
      <td>Popular, Domestic</td>
      <td>1-1149</td>
    </tr>
    <tr>
      <td>Popular, Domestic (Special) </td>
      <td>A-K</td>
    </tr>
    <tr>
      <td>Popular, Domestic</td>
      <td>D1-D24</td>
    </tr>
    <tr>
      <td>Grand Opera</td>
      <td>B150-B175</td>
    </tr>
    <tr>
      <td>Portuguese, Italian, Hebrew</td>
      <td>5000</td>
    </tr>
    <tr>
      <td>Mexican</td>
      <td>6000</td>
    </tr>
    <tr>
      <td>Argentine, Italian</td>
      <td>7000</td>
    </tr>
    <tr>
      <td>Mexican, Spanish, Puerto Rican</td>
      <td>8000</td>
    </tr>
    <tr>
      <td>Bohemian, Norwegian</td>
      <td>9000</td>
    </tr>
    <tr>
      <td>Hebrew</td>
      <td>10000</td>
    </tr>
    <tr>
      <td>French-Canadian</td>
      <td>11000</td>
    </tr>
    <tr>
      <td>British</td>
      <td>12000</td>
    </tr>
    <tr>
      <td>German</td>
      <td>15000</td>
    </tr>
    <tr>
      <td>Italian</td>
      <td>17000</td>
    </tr>
        <tr>
      <td>Concert</td>
      <td>28001-28040</td>
    </tr>
    <tr>
      <td>Grand Opera</td>
      <td>30000-30047</td>
    </tr>
    <tr>
      <td>Grand Opera</td>
      <td>35000-35021</td>
    </tr>
    <tr>
      <td>Grand Opera</td>
      <td>40000-40043</td>
    </tr>
  </table>
  <br />  <div class="pagination">
  	<span class="previous">Previous: <a href="history-goldmoulded.php" title="Previous">Edison Gold-Moulded Cylinder Recordings</a></span>
    <i class="small-screen fa fa-ellipsis-h"></i>
  	<span class="next">Next: <a href="history-grandopera.php" title="Next">Edison Grand Opera Cylinders</a></span>
  </div>
</div><!-- end .content -->
<!-- End of file: history-amberol.tpl -->
{include file="footer.tpl"}
