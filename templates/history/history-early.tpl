{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: history-early.tpl -->
<div class="content text">
  <h2 class ="center-text">The Phonautograph and Precursors to Edison's Phonograph</h2>

  <figure>
    <img src="/images/phonautograph.jpg" alt="An 1859 Koenig phonautograph. (Courtesy of the Smithsonian Institution.)" /><figcaption>An 1859 Koenig phonautograph. (Courtesy of the Smithsonian Institution.)</figcaption>
  </figure>

  <p>
    While Thomas Edison is rightly credited with the invention
    of the cylinder phonograph and the means of playing back sound, his ideas were clearly influenced by numerous
    others  who preceded him. Leon Scott's phonautograph, invented
    as far back as 1857, had demonstrated that ambient sound waves could be
    traced as a visual image through the vibrations of a bristle on a sheet of soot-covered  paper, known as a phonautogram.
    Although it undoubtedly was a first step in recording history,
    the phonautograph was not imagined as a device to reproduce sound.
    Not until 2008 were researchers able to play back the sound waves captured on these phonautograms.
    You can now listen to the <a href="http://www.firstsounds.org/sounds/index.php">recordings</a>,
    thanks to the work of a group of American researchers.
  </p>
  <p>
    Likewise, the prescience of Charles Cros, who in 1877 suggested a novel
    method by which sound waves could be captured and then reproduced, was
    impressive, but his invention remained only a theoretical ideal. It was
    the particular genius of Edison that he was able to capitalize on these
    ideas and design a standard machine for recording and playing back the
    human voice, one that would ultimately prove commercially successful as
    well. But before that point would come, a few other developments would
    have to occur first.
  </p>
  <div class="pagination">
  	<span class="previous">Previous: <a href="history.php" title="Previous">Cylinder Recordings: A Primer</a></span>
    <i class="small-screen fa fa-ellipsis-h"></i>
  	<span class="next">Next: <a href="history-tinfoil.php" title="Next">Tinfoil Recordings</a></span>
  </div>

</div><!-- end .content -->
<!-- End of file: history-early.tpl -->
{include file="footer.tpl"}
