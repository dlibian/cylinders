{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: press.tpl -->
<div class="content text">
	<h1>What the Critics Are Saying</h1>
	<ul>
	  <li>
	  	11/16/2005 &quot;Treasure Trove&quot; (The Anachronist blog)
	    <p>&quot;The UCSB site is astonishing: an expertly designed, 
	      easily searchable trove of beautiful, weird, wonderful records, many 
	      of them extremely rare...I can't help but suspect that we're watching 
	      a turning of the scholarly tide; I think we'll see a lot more work 
	      on The Other Roots Music in the coming years.&quot;</p>
	  </li>
	  <li>
	  	11/16/2005 &quot;<a href="http://ilx.wh3rd.net/thread.php?msgid=6441497">Old cylinder recordings (1890-1920) archive now online...</a>&quot; (Discussion 
	    on <a href="http://ilx.wh3rd.net/newquestions.php?board=2">I Love Music</a>)
	  </li>
	</ul>
	<ul>
	  <li>
	  	11/17/2005 &quot;<a href="http://www.ukulelia.com/">Digital Archive of Cylinder Recordings</a>&quot; (Ukulelia weblog)
	  </li>
	</ul>
	<ul>
	  <li>
	  	11/17/2005 &quot;<a href="http://boingboing.net/2005/11/17/5000-music-cylinders.html">5000 music cylinders digitized and posted</a>&quot; (<a href="http://www.boingboing.net/">Boingboing.net</a>)
	  </li>
	</ul>
	<ul>
	  <li>
	  	11/17/2005 &quot;<a href="http://www.cbc.ca/insite/AS_IT_HAPPENS_TORONTO/2005/11/17.html">Wax Cylinder Collection</a>&quot; (CBC's &quot;<a href="http://www.radio.cbc.ca/programs/asithappens/index.html">AsIt Happens</a>&quot;)
	  </li>
	</ul>
	<ul>
	  <li>
	  	11/18/2005 &quot;<a href="http://cyberlaw.stanford.edu/blogs/levine/archives/003599.shtml">University of California, Santa Barbara Digitizes Over 5,000 Cylinders</a>&quot; (Center for the Internet and Society <a href="http://cyberlaw.stanford.edu/blogs/levine/">Levine Blog</a>)
	  </li>
	</ul>
	<ul>
	  <li>
	  	11/18/2005 &quot;<a href="http://slashdot.org/article.pl?sid=05/11/19/022236&tid=141&tid=126">5000 Cylinder Recordings Placed Online</a>&quot; (<a href="http://slashdot.org/">Slashdot</a> discussion)
	  </li>
	</ul>
	<ul>
	  <li>
	  	11/23/2005 &quot;<a href="http://blog.eogn.com/eastmans_online_genealogy/2005/11/what_did_our_an.html">What Did Our Ancestors Listen To?</a>&quot; (<a href="http://blog.eogn.com/eastmans_online_genealogy/">Eastman's Online Genealogy Newsletter</a>)
	  </li>
	</ul>
	<ul>
	  <li>
	  	12/7/2005 <a href="http://www.columbian.com/lifeHome/lifeHomeNews/12072005news54951.cfm">Genealogy Today: Podcasts bring genealogy to your home computer or iPod</a> (Clark County, Washington, <a href="http://www.columbian.com/index.cfm">Columbian</a>)
	  </li>
	</ul>
	<ul>
	  <li>
	  	12/14/2005 &quot;<a href="http://blogs.usatoday.com/techspace/2005/12/i_whistle_a_hap.html">Whistle a happy tune</a>&quot; (USA Today's <a href="http://blogs.usatoday.com/techspace/">Tech_Space</a> blog)
	  </li>
	</ul>
	<ul>
	  <li>
	  	12/21/2005 &quot;<a href="http://www.seattleweekly.com/music/0551/051221_music_cdreviews.php">Home Listening for the Holidays</a>&quot; (<a href="http://www.seattleweekly.com/">Seattle Weekly</a>)
	  </li>
	</ul>
	<ul>
	  <li>
	  	12/23/2005 &quot;<a href="http://www.cla.purdue.edu/academic/engl/sycamore/">Edison Amberol</a>&quot; (Sycamore Review)
	  </li>
	</ul>
	<ul>
	  <li>
	  	Jan/Feb 2006 &quot;Family Historians are accustomed to silent searching 
	    through history, but wouldn't it be nice if history actually said something 
	    to us?&quot; (<a href="http://www.ancestry.com/learn/library/magazines/default.aspx">Ancestry Magazine</a>)
	  </li>
	</ul>
	<ul>
	  <li>
	  	1/5/2006 &quot;<a href="http://www.slate.com/id/2133842/?nav=tap3">The Year in Culture: Judd Apatow, Noah Baumbach, Curtis Sittenfeld, and others on 2005's most notable cultural happenings</a>&quot; (<a href="http://www.slate.com/">Slate Magazine</a>)
	  </li>
	</ul>
	<ul>
	  <li>
	  	1/6/2006 &quot;<a href="http://asap.ap.org/stories/278259.s">Party Like it's 1899</a>&quot; 
	  	(Associated Press <a href="http://asap.ap.org/">ASAP</a> feature, also at 
	    <a href="http://www.austin360.com/watercooler/content/shared-gen/ap/asap/Entertainment/asap_Entertainment_Cylinder.html">Austin American-Statesman (Austin360.com)</a>, 
	    <a href="http://www.insidenova.com/servlet/Satellite?pagename=ISN/MGArticle/IMD_BasicArticle&c=MGArticle&cid=1128769148754">InsideNova.com</a>, 
	    <a href="http://www.tbt.com/tech/article16589.ece">Tampa Bay/St. Petersburg Times</a>, 
	    <a href="http://www.wacotrib.com/featr/content/shared-gen/ap/asap/Entertainment/asap_Entertainment_Cylinder.html">Waco Tribune-Herald</a>, 
	    <a href="http://www.chron.com/disp/story.mpl/ent/music/3575258.html">Houston Chronicle</a>, 
	    <a href="http://www.news-journal.com/featr/content/shared-gen/ap/asap/Entertainment/asap_Entertainment_Cylinder.html">Longview (TX) News-Journal</a>, 
	    <a href="http://www.reflector.com/featr/content/shared-gen/ap/asap/Entertainment/asap_Entertainment_Cylinder.html">Greenville (NC) Daily Reflector</a>, 
	    <a href="http://www.gjsentinel.com/featr/content/shared-gen/ap/asap/Entertainment/asap_Entertainment_Cylinder.html">Grand Junction Daily Sentinal</a>, 
	    <a href="http://www.rockymounttelegram.com/featr/content/shared-gen/ap/asap/Entertainment/asap_Entertainment_Cylinder.html">Rocky Mount (NC) Telegram</a>, 
	    <a href="http://www.lufkindailynews.com/featr/content/shared-gen/ap/asap/Entertainment/asap_Entertainment_Cylinder.html">Lufkin (TX) Daily News</a>, 
	    <a href="http://www.dailyadvance.com/featr/content/shared-gen/ap/asap/Entertainment/asap_Entertainment_Cylinder.html">Elizabeth City (NC) Daily Advance</a>, 
	    <a href="http://www.marshallnewsmessenger.com/featr/content/shared-gen/ap/asap/Entertainment/asap_Entertainment_Cylinder.html">Marshall (TX) News Messenger</a>, 
	    <a href="http://template.accessatlanta.com/entertainment/content/shared-gen/ap/asap/Entertainment/asap_Entertainment_Cylinder.html">Atlanta Journal-Constitution (AccessAtlanta.com)</a> et. al)
	  </li>
	</ul>
	<ul>
	  <li>
	  	1/20/2006 &quot;<a href="http://www.ucsbdailynexus.com/news/2006/10682.html">Library offers early recordings online</a>&quot; (UCSB <a href="http://www.dailynexus.com/">Daily Nexus</a>)
	  </li>
	</ul>
	<ul>
	  <li>
	  	2/2/2006 &quot;<a href="http://www.publicradio.org/columns/futuretense/2006/02/02.shtml">Preserving fragile cylinder recordings</a>&quot; (American Public Media's <a href="http://www.publicradio.org/columns/futuretense/">Future Tense</a>)
	  </li>
	</ul>
	<ul>
	  <li>
	  	2/12/2006 &quot;<a href="http://www.newspress.com/Top/Article/article.jsp?Section=LOCAL&ID=564680735852724986">UCSB puts historic recordings on Web</a>&quot; (<a href="http://www.newspress.com/">Santa Barbara News Press</a>)
	  </li>
	</ul>
	<ul>
	  <li>
	  	2/13/2006 &quot;Changing History&quot; (<a href="http://www.wsj.com/">Wall Street Journal</a>) 
	    <p>
	    	Historical material heading onto the Web isn't all 
	      documents and images, either. Last November, 5,000 digitized wax-cylinder 
	      recordings dating back to 1895 were posted online by the Cylinder 
	      Preservation and Digitization Project at the University of California 
	      at Santa Barbara. Among the recordings: Tin Pan Alley music, vaudeville 
	      performances and advertisements from that time.
	    </p>
	    <h4>Seldom Heard</h4>
	    <p>Rick Altman, a professor of cinema and comparative 
	      literature at the University of Iowa, says that the digitized cylinders 
	      have been a blessing for his research work. He recently downloaded 
	      routines by Russell Hunting, a comedian around the turn of the 20th 
	      century whose recordings, until now, were nearly inaccessible. Mr. 
	      Altman has written extensively about silent-movie-era performers who 
	      specialized in making sounds to match the action on the screen -- 
	      from chirping birds to foreign accents -- and says that many of these 
	      performers modeled their styles after Mr. Hunting's.
	    </p>
	    <p>
	    	&quot;I had to write about this without ever having 
	      heard him,&quot; Mr. Altman says. &quot;Now I'll have a better sense 
	      of what people were looking for.&quot;
	    </p>
	  </li>
	  <li>
	  	2/13/2006 &quot;<a href="http://www.contracostatimes.com/mld/cctimes/news/13859774.htm">Rare recordings available online</a>&quot; (<a href="http://www.contracostatimes.com/">Contra Costa Times</a>)
	  </li>
	</ul>
	<ul>
	  <li>
	  	2/13/2006 &quot;Brief news items from around California&quot; (<a href="http://www.sfgate.com/cgi-bin/article.cgi?f=/n/a/2006/02/12/state/n152659S50.DTL">San Francisco Chronicle</a>, 
	  	<a href="http://news.google.com/url?sa=t&ct=us/0-0&fp=43f0359162aef6e9&ei=ccLwQ9vyOIz8oQL56IWGBQ&url=http%3A//www.mercurynews.com/mld/mercurynews/news/breaking_news/13856741.htm&cid=0">San Jose Mercury News</a>, 
	  	<a href="http://www.sanluisobispo.com/mld/sanluisobispo/13856741.htm">San Luis Obispo Tribune</a>, 
	  	<a href="http://news.google.com/url?sa=t&ct=us/3-0&fp=43f0359162aef6e9&ei=ccLwQ9vyOIz8oQL56IWGBQ&url=http%3A//www.montereyherald.com/mld/montereyherald/news/13856741.htm&cid=0">Monterey County Herald</a>)
	  </li>
	</ul>
	<ul>
	  <li>
	  	2/14/2006 &quot;<a href="http://jscms.jrn.columbia.edu/cns/2006-02-14/stevens-jugbandsgodigital">Cylinder recordings from Edison's era find new life in podcasts</a>&quot; (Columbia News Service)
	  </li>
	</ul>
	<ul>
	  <li>
	  	2/21/2006 Morning News program (<a href="http://www.kclu.org/">KCLU</a>, Thousand Oaks, CA):
	  	<div class="qt-audio">
		  	<script language="javascript">
	    		QT_WriteOBJECT('http://stream.library.ucsb.edu/cylinders/KCLU.mov' , '50%', '95%', '', 'AUTOPLAY', 'False', 'SCALE', 'ToFit');
				</script>
			</div>
		</li>
	</ul>
	<ul>
	  <li>
	  	3/16/2006 <a href="http://www.wnyc.org/shows/soundcheck/episodes/2006/03/16">America's forgotten pop legacy</a>, Soundcheck, WNYC (New York). Interview with Jody Rosen, Music Critic, Slate.com.
	  </li>
	</ul>
	<ul>
	  <li>
	  	3/17/2006 &quot;<a href="http://www.iht.com/articles/2006/03/16/features/pop.php">Listeners rediscover Tin Pan Alley pop</a>&quot; (<a href="http://www.iht.com/">International Herald Tribune</a>)
	  </li>
	</ul>
	<ul>
	  <li>
	  	3/19/2006 &quot;<a href="http://www.nytimes.com/2006/03/19/arts/music/19rose.html">How pop sounded before it popped</a>&quot; (<a href="http://www.nytimes.com/">New York Times</a>)
	  </li>
	</ul>
	<ul>
	  <li>
	  	3/20/2006 &quot;<a href="http://www.wired.com/news/technology/0,70378-0.html">Archaic sounds caress modern ears</a>&quot; (<a href="http://www.wired.com/">Wired News</a>)
	  </li>
	</ul>
	<ul>
	  <li>3/22/2006 &quot;<a href="http://www.startribune.com/389/story/323864.html">Web search: It's all in the archive</a>&quot; (<a href="http://www.startribune.com/">Minneapolis StarTribune</a>)
	  </li>
	</ul>
	<ul>
	  <li>
	  	4/1/2006 &quot;<a href="http://www.will.uiuc.edu/willmp3/sidetrackapr06.mp3">Sidetrack: April 2006</a>&quot; (<a href="http://www.will.uiuc.edu/am/sidetrack/default.htm">WILL, Champaign-Urbana, IL</a>)
	  </li>
	</ul>
	<ul>
	  <li>
	  	4/2/2006 &quot;<a href="http://www.cbc.ca/thesundayedition/">Sunday Edition</a>&quot; (CBC Radio One)
	  </li>
	</ul>
	<ul>
	  <li>
	  	4/4/2006 &quot;<a href="http://www.columbiaspectator.com/vnews/display.v/ART/2006/04/04/443212b339867">Cooking the clocks</a>&quot; (<a href="http://www.columbiaspectator.com/">Columbia Spectator</a>)
	  </li>
	</ul>
	<ul>
	  <li>
	  	4/13/2006 &quot;<a href="http://www.csmonitor.com/2006/0413/p13s02-stct.html">Historians strive to save old sounds</a>&quot; (<a href="http://www.csmonitor.com/">Christian Science Monitor</a>)
	  </li>
	</ul>
	<ul>
	  <li>
	  	5/13/2006 &quot;<a href="http://www.thestar.com/NASApp/cs/ContentServer?pagename=thestar/Layout/Article_Type1&c=Article&cid=1147341619547&call_pageid=968867495754&col=969483191630">Online treasures for early music fans</a>&quot; (<a href="http://www.thestar.com/">Toronto Star</a>)
	  </li>
	</ul>
	<ul>
	  <li>
	  	5/16/2006 &quot;<a href="http://www.bbc.co.uk/radio/aod/networks/radio4/aod.shtml?radio4/frontrow_tue">Front Row</a>&quot; (BBC Radio 4)
	  </li>
	</ul>
	<ul>
	  <li>
	  	5/22/2006 &quot;<a href="http://www.telegraph.co.uk/news/main.jhtml?xml=/news/2006/05/22/wax22.xml&sSheet=/news/2006/05/22/ixnews.html">History of recording waxes lyrical on-line</a>&quot; (Daily Telegraph/Telegraph.co.uk)
	  </li>
	</ul>
	<ul>
	  <li>
	  	June 2006 &quot;Party Like It's 1899!&quot; (<a href="http://www.wordmagazine.co.uk/">The Word</a>)
	  </li>
	</ul>
	<ul>
	  <li>
	  	July 18, 2006. &quot;Vintage music available for download&quot; (<a href="http://www.wsj.com/">Wall Street Journal</a> also [Columbia, SC] <a href="http://www.thestate.com/mld/thestate/news/nation/15069939.htm">The State</a>)
	  </li>
	</ul>
	<ul>
	  <li>
	  	September 1, 2006. <a href="http://scout.wisc.edu/Reports/ScoutReport/2006/scout-060901-re.php#2">Scout Report Selection</a> (<a href="http://scout.wisc.edu/">Internet Scout Project</a>)
	  </li>
	</ul>
	<ul>
	  <li>
	  	April 13, 2007. &quot;Unlocking Nation's Musical Memories: Pre-1972 Copyright Confusion Keeps Archives Out of Reach.&quot; (Los Angeles Daily Journal)
	  </li>
	</ul>
	<ul>
	  <li>
	  	November 15, 2007. <a href="http://www.ocweekly.com/features/music-feedback/10-crucial-music-websites/28056/">10 Crucial Music Websites</a> (<a href="http://www.ocweekly.com/">OC Weekly</a>)
	  </li>
	</ul>
	<ul>
	  <li>
	  	June 16, 2008. &quot;<a href="http://www.time.com/time/specials/2007/article/0,28804,1809858_1809954_1811319,00.html">50 Best Websites of 2008</a>&quot; (<a href="http://www.time.com/time/specials/2007/article/0,28804,1809858_1809957,00.html">TIME Magazine</a>)
	  </li>
	</ul>
	<ul>
	  <li>
	  	September 10, 2008. "<a href="http://online.wsj.com/article/SB122100522362717305.html">Wax in My Ears: An Online Journey</a>" (<a href="http://online.wsj.com/public/us">Wall Street Journal</a>)
	  </li>
	</ul>
	<ul>
        <li>March 25, 2015. &quot;<a href="http://www.hollywoodreporter.com/news/steve-martin-doors-radiohead-albums-783734">Steve Martin, The Doors and Radiohead Albums Named to National Recording Registry</a>&quot; (<a href="http://www.hollywoodreporter.com">Hollywood Reporter</a>)</li>
        <li>March 29, 2015. <a href="http://www.newspress.com/Top/Article/article.jsp?Section=LOCAL&ID=567240931498131467">&quot;History in Wax</a>.&quot; <a href="http://www.newspress.com">(Santa Barbara News Press</a>)</li>
      </ul>
      <ul>
        <li>March 30, 2015. &quot;<a href="http://www.kclu.org/2015/03/30/rare-recordings-in-south-coast-collection-added-to-library-of-congress-registry/">Rare Recordings In South Coast Collection Added To Library Of Congress Registry</a>&quot; (<a href="http://www.kclu.org">KCLU</a>)</li>
      </ul>
	<p>
		For media inquiries, a <a href="http://www.ia.ucsb.edu/pa/display.aspx?pkey=1394">press release</a> is available online or contact <a href="javascript:mail('seubert','library.ucsb','edu')">David Seubert</a> at 805-893-5444 for further information.
	</p>
</div><!-- end .content -->
<!-- End of file: press.tpl -->   
{include file="footer.tpl"}