{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: radio.tpl -->
<div class="content text">
  <h1>Thematic Playlists</h1>
  <p>
    Don't know what to look for? Tired of searching and clicking
    on links? Let our expert curators guide your way through the repertoire
    on early cylinder recordings with  thematic playlists.  Playlists  are
    added on an irregular basis, featuring a preselected grouping of recordings unified
    around a theme. All programs are available as downloadable podcasts.  </p>
  <p>
    <a href="thematic_playlists.xml"><img src="/images/rss-podcast.png" alt="Podcast" width="80" height="15" border="0" class="podcast-logo"></a> Subscribe to the thematic playlists podcast   and automatically download new programs.  </p>

  <h3><a href="mexico.php">Mexican Cylinders: Of National Identity and Sound Recordings</a> <img src="/images/new-icon.jpg" alt="New" width="32" height="32" /></h3>
  <ul>
    <li>A playlist of early cylinder recordings  from Mexico. </li>
  </ul>
  <h3><a href="hillbilly.php">Early Hillbilly &amp; Old Time Music</a> <img src="/images/new-icon.jpg" alt="New" width="32" height="32" /></h3>
  <ul>
    <li>Country music on Edison cylinders from the 1920s.</li>
  </ul>
  <h3><a href="centraleurope.php">Central European Mix Tape</a> <img src="/images/new-icon.jpg" alt="New" width="32" height="32" /></h3>
  <ul>
    <li>Music from Central Europe before the World Wars. </li>
  </ul>
  <h3><a href="tahiti.php">Tahitian  Field Recordings</a> <img src="/images/new-icon.jpg" alt="New" width="32" height="32" /></h3>
  <ul>
    <li> A group of unusual custom Blue Amberol cylinders recorded in Tahiti. 	</li>
  </ul>
  <h3><a href="accordion.php">Squeezebox: The Accordion on Cylinders</a></h3>
  <ul>
    <li>
      The first installment of a two part series exploring the accordion and other free reed
    instruments recorded on early cylinders.</li>
  </ul>
  <h3><a href="incunabula2.php">Recorded Incunabula, Part 2</a></h3>
  <ul>
    <li>
      The second installment of the incunabula series, featuring more rare recordings from the
      collection of John Levin.    </li>
  </ul>
  <h3><a href="getty.php">Operatic Cylinders from the Getty Collection</a></h3>
  <ul>
    <li>
      Rare operatic cylinders from the collection of Gordon Getty.    </li>
  </ul>
  <h3><a href="incunabula.php">Recorded Incunabula 1891&ndash;1898</a></h3>
  <ul>
    <li>
      A program featuring some of the rarest, earliest, and most fascinating recordings you
    are ever likely to encounter, curated by collector John Levin.</li>
  </ul>
  <h3><a href="wwi.php">Popular Songs of World War I</a></h3>
  <ul>
    <li>
      In commemoration of the 90th anniversary of the United States' entry into World War I,
      listen to a selection of  songs from the Great War.    </li>
  </ul>
  <h3><a href="december1908.php">Advance List for December, 1908</a></h3>
  <ul>
    <li>
      Listen to the new releases from Edison's &quot;Advance List&quot; of December,  1908  new releases. Curated
    by Patrick Feaster, Indiana University. </li>
  </ul>
  <h3><a href="cakewalks.php">Cakewalks and Rags</a></h3>
  <ul>
    <li>
      The syncopated rhythms of cakewalks and rags presented here have
      had a lasting impact on American popular music. Curated by David
    Seubert, UC Santa Barbara </li>
  </ul>
  <h3><a href="deutschekomische.php">Deutsche komische Zylinder (German Comic Cylinders)</a></h3>
  <ul>
    <li>
      German comic skits on Edison cylinders from between 1904 and 1909. Curated
    by Ursula Clarke and Noah Pollaczek, UC Santa Barbara.</li>
  </ul>
  <h3><a href="americanvaudeville.php">American Vaudeville</a></h3>
  <ul>
    <li>
      Transport yourself back to the turn of the 20th century with a program
      featuring the top stars of vaudeville&mdash;and the performers that should have
    been. Curated by Samuel Brylawski, UC Santa Barbara, Editor, <a href="http://adp.library.ucsb.edu/">Discography of American Historical Recordings</a>.</li>
  </ul>
  <h3><a href="blackartists.php">Early Black Artists and Composers</a></h3>
  <ul>
    <li>
      Listen to pioneering recordings by African-American performers and
      composers. Curated by Tim Brooks, author of <a href="http://www.press.uillinois.edu/s04/brooks.html">Lost Sounds: Blacks and the Birth of the Recording Industry, 1890&ndash;1919.</a>
    (University of Illinois Press, 2004). </li>
  </ul>
  <h3><a href="moran.php">Operatic Cylinders from the William R. Moran collection</a></h3>
  <ul>
    <li>
      Listen to operatic arias recorded on some of the rarest cylinders
      made, including Edison B series and French and German operatic cylinders.
    Curated by David Seubert, UC Santa Barbara. </li>
  </ul>
  <h3><a href="audiotheater.php">Pioneers of Audio Theater</a></h3>
  <ul>
    <li>
      Some of the most creative early recordings had a narrative element
      and are now known as &quot;audio theater.&quot; This program presents
      20 of the most interesting examples from the collection. Curated
    by Patrick Feaster, Indiana University. </li>
  </ul>
  <h3><a href="speeches.php">Historical Speeches on Edison Cylinders</a></h3>
  <ul>
    <li>
      <p>
        Early historical speeches and recordings by Theodore Roosevelt, William
        Jennings Bryan, Ernest Shackleton, Sarah Bernhardt, William Howard
      Taft and others. Curated by David Seubert, UC Santa Barbara. </p>
    </li>
  </ul>
</div><!-- end .content -->
<!-- End of file: radio.tpl -->
{include file="footer.tpl"}
