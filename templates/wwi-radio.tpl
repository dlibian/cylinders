{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: wwi-radio.tpl -->
<div class="content text">
  <h1>Popular Songs of World War I</h1>
  <p>
    <a href="thematic_playlists.xml"><img src="/images/rss-podcast.png" alt="Podcast"></a>
    &nbsp;
    <a href="http://stream.library.ucsb.edu/WorldWarI.m3u"><img src="/images/rss-audiom3u.png"></a>
  </p>

  <ul>
    <li><a href="wwi.php">Introduction</a></li>
    <li><a href="/search.php?nq=1&query_type=subject&query=world+war">Find All World War I Cylinders</a></li>
    <li><a href="wwi-sources.php">Sources on WWI Songs</a></li>
  </ul>

  <p>
    On April 6th, 1917 the US Congress declared war on Germany at the urging of President Wilson. Shortly thereafter,
    Americans headed off to fight in the European theater of World War I. Ninety years later, and under very different
    circumstances, the war in Iraq enters its fifth year. And  as the present war has influenced the composition of
    popular music, the onset of WWI similarly stimulated songwriters of the 20th century's teens. Yet this analogy
    should only be taken so far: there is no recording of the era whose tone is remotely comparable to that of Neil
    Young's "Let's Impeach the President" in 2006. In fact, the jingoistic themes of WWI tunes, with  their ubiquitous
    anti-German sentiment, are perhaps more closely echoed in modern times in songs like Toby Keith's "Courtesy of the
    Red, White and Blue (The Angry American)." Regardless of the parallels one chooses to draw, it is hard to overstate
    the effects of WWI on the popular culture of that time. With America's entry into the war, a torrent of patriotic songs,
    sheet music, and  musical revues followed. These twenty-one cylinder recordings are a small sampling of this
    larger burst of war-fueled musical inspiration.
  </p>
  <p>
    The program has been  divided into six categories that broadly reflect some of the main themes of WWI music:
    Patriotic Americans Fighting Across the Foam; The Life of the Soldier, On and Off the Battlefield; Love During Wartime;
    The Fight Viewed from the Home Front; War Perpetuates the Status Quo; and Remembering the Troops.
  </p>

  <p>
    For a more in-depth look at the music history of WWI, see Glenn Watkins's
    <a href="http://worldcatlibraries.org/wcpa/oclc/56028697"><em>Proof Through the Night: Music and the Great War</em></a>,
    which served as a valuable resource in compiling this program. <em>&mdash;Noah Pollaczek, UCSB Cylinder
    Audio Archive  </em>  </p>

  <h3><em>Patriotic Americans Fighting Across the Foam</em></h3>

  <p>
    The program begins with the 1917 hit tune
    "<a href="/search.php?nq=1&query_type=keyword&query=over+there+cohan">Over There</a>"
    and its shout to "prepare, say a prayer, send the word, send the word to beware / we'll be over, we're coming over,
    and we won't come back 'til it's over over there." As a means of getting Americans geared up for war, the political
    significance is clear. Yet  a title from two years earlier,
    "<a href="/search.php?nq=1&query_type=call_number&query=0113">Don't Send My Darling Boy Away</a>,"
    reflects a sense of  uncertainty about America's contributions to the war. The same composer,
    <a href="/search.php?nq=1&query_type=keyword&query=albert+von+tilzer">Albert Von Tilzer</a>, would nonetheless pen
    "<a href="/search.php?nq=1&query_type=call_number&query=5520">What Kind of American Are You?</a>"
    with the onset of American hostilities against Germany&mdash;its you're-with-us-or-against-us message hammering home
    the speed at which composers, and perhaps the larger public, had fallen into line in supporting the war effort.
    The next three songs&mdash;
    "<a href="/search.php?nq=1&query_type=call_number&query=5761">We're All Going Calling on the Kaiser</a>,"
    "<a href="/search.php?nq=1&query_type=call_number&query=5700">Bing! Bang! Bing' Em on the Rhine</a>," and
    "<a href="/search.php?nq=1&query_type=call_number&query=5825">Tell That to the Marines</a>"&mdash;speak
    for themselves. With frequent derogatory references to "ol' Bill" and his "damn U-boat," they could perhaps even form the
    basis of a WWI musical subgenre of Kaiser trash-talk tunes.
    <a href="/search.php?nq=1&query_type=keyword&query=Arthur+Fields">Arthur Fields</a>,
    <a href="/search.php?nq=1&query_type=keyword&query=Billy+Murray">Billy Murray</a>,
    and the <a href="/search.php?nq=1&query_type=keyword&query=American+Quartet">American Quartet</a>
    (in which Murray sang tenor)  sing four of the six works, demonstrating their affinity for the genre that will
    reemerge throughout the program.
  </p>


  <figure>
    <img src="/images/clark_small.jpg" title="Helen Clark" alt="From Edison Amberol Records, November, 1920" /><figcaption><em>From Edison Amberol Records, November, 1920</em></figcaption>
  </figure>

  <ul>
    <li>
      <span class="small-text">Over there (take 1) / George M. Cohan. Billy Murray.
        <a href="/search.php?nq=1&query_type=call_number&query=4470">Edison Blue Amberol: 3275</a>. 1917.</span>
    </li>
    <li>
      <span class="small-text">Don't take my darling boy away / Albert Van Tilzer. Joseph A. Phillips and Helen Clark.
        <a href="/search.php?nq=1&query_type=call_number&query=0113">Edison Blue Amberol: 2622</a>. 1915.</span>
    </li>
    <li>
      <span class="small-text">What kind of an American are you? / Albert Von Tilzer. Helen Clark.
        <a href="/search.php?nq=1&query_type=call_number&query=5520">Edison Blue Amberol: 3252</a>. 1917.</span>
    </li>
    <li>
      <span class="small-text">We're all going calling on the Kaiser / James Alexander Brennan. Arthur Fields.
        <a href="/search.php?nq=1&query_type=call_number&query=5761">Edison Blue Amberol: 3568</a>. 1918.</span>
    </li>
    <li>
      <span class="small-text">Bing! Bang! Bing' em on the Rhine / Jack Mahoney. American Quartet.
        <a href="/search.php?nq=1&query_type=call_number&query=5700">Edison Blue Amberol: 3494</a>. 1918.</span>
    </li>
    <li>
      <span class="small-text">Tell that to the Marines / Jean Schwartz. Billy Murray.
        <a href="/search.php?nq=1&query_type=call_number&query=5825">Edison Blue Amberol: 3641</a>. 1919.</span>
    </li>
  </ul>

  <h3>The Life of the Soldier, On and Off the Battlefield</h3>
  <p>
    "Some day I'm going to murder the bugler" is how <a href="/search.php?nq=1&query_type=keyword&query=Irving+Berlin">Irving Berlin</a>
    envisions a frustrated recruit forced to rise and shine, in
    "<a href="/search.php?nq=1&query_type=call_number&query=5367">Oh! How I Hate to Get Up in the Morning</a>."
    Arthur Fields performs this tune as well as a duet with
    <a href="search.php?nq=1&query_type=keyword&query=Grace+Woods">Grace Woods</a> titled
    "<a href="/search.php?nq=1&query_type=call_number&query=5613">I Don't Want to Get Well</a>,"
    which depicts a soldier wishing to stay wounded in order to keep the attentions of a beautiful nurse.
    "<a href="/search.php?nq=1&query_type=call_number&query=5806">The Battle in the Air</a>"
    is an example of a &quot;descriptive specialty&quot;&mdash;a type of short narrative story recorded on early discs and
    cylinders&mdash;adapted to WWI (<a href="audiotheater.php">listen to a program</a>),
    including a dramatic climax in the form of an aerial dogfight. More thespian action by the American Quartet continues
    underwater in "<a href="/search.php?nq=1&query_type=call_number&query=5703">The Submarine Attack</a>."
  </p>

  <figure>
    <img src="/images/fields_small.jpg" title="Arthur Fields" alt="From Edison Amberol Records, November, 1920" /><figcaption><em>From Edison Amberol Records, November, 1920</em></figcaption>
  </figure>

  <ul>
    <li>
      <span class="small-text">The battle in the air / Theodore F. Morse. American Quartet.
        <a href="/search.php?nq=1&query_type=call_number&query=5806"> Edison Blue Amberol: 3618</a>. 1918.</span>
    </li>
    <li>
      <span class="small-text">Oh! How I hate to get up in the morning / Irving Berlin. Arthur Fields.
        <a href="/search.php?nq=1&query_type=call_number&query=5367">Edison Blue Amberol: 3639</a>. 1919.</span>
    </li>
    <li>
      <span class="small-text">I don't want to get well / Harry Jentes. Arthur Fields and Grace Woods.
        <a href="/search.php?nq=1&query_type=call_number&query=5613">Edison Blue Amberol: 3378</a>. 1918.</span>
    </li>
  </ul>

  <h3>Love During Wartime</h3>
  <p>
    The cylinder recording format reached the peak of its popularity in the  first ten years of the 20th century,
    a time when 19th  century popular music styles remained prevalent. One of the most widespread  genres of the
    era was the sentimental love song. However, 20th century  songwriters recognized the corniness of the genre
    and infused songs with gentle  humor. America's  entrance into the war provided composers with topical grist
    for their wit. In &quot;<a href="/search.php?nq=1&query_type=call_number&query=5782">If  He Can Fight Like He Can Love, Goodnight Germany</a>,&quot; Mary confidently  sings of her beau, just departed for the war.
    Asserting that if &quot;he's just  half as good in a trench, as he was in the park on a bench,&quot; she reminds
    listeners ninety years hence that their great-grandparents&rsquo; popular music, too,  included sexual innuendos.
    Unlike Mary, Rosie  Green, in &quot;<a href="/search.php?nq=1&query_type=call_number&query=5791">Oh! Frenchy</a>,
    &quot; heads to France  after enlisting as an American nurse and falls for a &quot;soldier from  Paree.&quot; The increasing role
    of women in the war effort enabled popular  composers to consider women in other situations than that of motherhood and &quot;
    spooning  under the silvery moon&quot;; all the same, Rosie Green's interest in &quot;Frenchy&quot;  is equal to that of working
    on the front lines, so perhaps not much of a leap  forward for gender parity.  Con Conrad enjoyed his work so much that he wrote
    a sequel, &quot;<a href="/search.php?nq=1&query_type=call_number&query=5928">Frenchy,  Come to Yankee Land</a>.
    &quot; The jaunty tune &quot;<a href="/search.php?nq=1&query_type=call_number&query=5708">Send  Me a Curl</a>
    &quot; returns to old-fashioned sentimental love song territory,  in which a soldier pines for a lock of his love's hair as
    a memento of their  happiness back home and away from the war.
  </p>

  <figure>
    <img src="/images/goodnight_small.jpg" title="If He Can Fight Like He Can Love" alt="Courtesy Lester Levy Collection of Sheet Music" /><figcaption>Courtesy <a href="http://levysheetmusic.mse.jhu.edu/">Lester Levy Collection of Sheet Music</a></figcaption>
  </figure>

  <ul>
    <li>
      <span class="small-text">If he can fight like he can love, goodnight Germany! / George W. Meyer. Elaine Gordon.
        <a href="/search.php?nq=1&query_type=call_number&query=5782">Edison Blue Amberol: 3593</a>. 1918. </span>
    </li>
    <li>
      <span class="small-text">Oh! Frenchy / Con Conrad. Arthur Fields.
        <a href="/search.php?nq=1&query_type=call_number&query=5791">Edison Blue Amberol: 3601</a>. 1918.</span>
    </li>
    <li>
      <span class="small-text">Send me a curl / Geoffrey O'Hara. American Quartet.
        <a href="/search.php?nq=1&query_type=call_number&query=5708">Edison Blue Amberol: 3507</a>. 1918.</span>
    </li>
  </ul>

  <h3>The Fight Viewed from the Home Front</h3>
  <p>
    A somewhat more jaundiced view of domestic bliss can be heard in
    "<a href="/search.php?nq=1&query_type=call_number&query=5616">I'd Feel At Home If They'd Let Me Join the Army</a>
    ," one man's ode to the military life as a means of escaping his wife.
    "<a href="/search.php?nq=1&query_type=call_number&query=0166">Sister Susie's Sewing Shirts for Soldier</a>s"
    ostensibly begins with the praiseworthy war contributions of one young woman. Her effort is regrettably in vain, though, as
    "some soldiers send epistles, say they'd sooner sleep on thistles, than the saucy, soft, short shirts for soldiers, sister
    Susie sews." And when not writing timeless love songs, the songwriters of the era would occasionally directly incorporate the
    issues of the day into their lyrics. The events of the 'teens are hard to miss in
    "<a href="/search.php?nq=1&query_type=call_number&query=5924">All Those in Favor Say Aye</a>,"
    when the lodge master played by Arthur Fields requests a show of hands on the war, Prohibition, the Salvation Army,
    and the Bolsheviks.
  </p>

  <figure>
    <img src="/images/murray_small.jpg" alt="From Edison Amberol Records, November, 1920" /><figcaption>From Edison Amberol Records, November, 1920</figcaption>
  </figure>

  <ul>
    <li>
      <span class="small-text">I'd feel at home if they'd let me join the army / Albert Gumble. M.J. O'Connell.
        <a href="/search.php?nq=1&query_type=call_number&query=5616">Edison Blue Amberol: 3381</a>. 1918.</span>
    </li>
    <li>
      <span class="small-text">Sister Susie's sewing shirts for soldiers / Herman Darewski. Billy Murray.
        <a href="/search.php?nq=1&query_type=call_number&query=0166">Edison Blue Amberol: 2530</a>. 1915.</span>
    </li>
    <li>
      <span class="small-text"> All those in favor say aye / Tom Kennedy. Arthur Fields.
        <a href="/search.php?nq=1&query_type=call_number&query=5924">Edison Blue Amberol: 3777</a>. 1919.</span>
    </li>
  </ul>

  <h3>War Perpetuates the Status Quo</h3>
  <p>
    Just as the sentimental ballad was refurbished with the addition of war-related thematic material, so too were
    the popular  genres whose lyrics are most likely to offend modern sensibilities&mdash;the "coon" song, the
    "Indian love-song,"  and the "rube" sketch. Before he became a star country singer,
    <a href="/search.php?nq=1&query_type=keyword&query=Vernon+Dalhart">Vernon Dalhart</a> performs in what was then
    a relatively common character type, as an African-American longing for the old southern days. In
    "<a href="/search.php?nq=1&query_type=call_number&query=5784">I'm Goin' to Fight My Way Right Back to Carolina</a>
    ," the historical falsehoods common to songs of this era have been reconfigured in the context of wartime. Likewise, in
    "<a href="/search.php?nq=1&query_type=call_number&query=5764">Indianola</a>," Billy Murray assumes the
    stereotype of the "redman chief" who hears the call of war and heads off to "tomahawk the Kaiser." In a similar fashion,
    the "rube" archetype frequently played by such performers as
    <a href="/search.php?nq=1&query_type=keyword&query=Cal+Stewart">Cal Stewart</a> in his Uncle Josh sketches is redeployed in
    "<a href="/search.php?nq=1&query_type=call_number&query=5602">Long Boy</a>,"
    which tells the story of a country bumpkin type who heads to war without really knowing why.
  </p>

  <figure>
    <img src="/images/longboy_small.jpg" title="Long boy" alt="Courtesy Lester Levy Collection of Sheet Music" /><figcaption>Courtesy <a href="http://levysheetmusic.mse.jhu.edu/">Lester Levy Collection of Sheet Music</a></figcaption>
  </figure>

  <ul>
    <li>
      <span class="small-text">I'm goin' to fight my way right back to Carolina  / Vernon Dalhart.
        <a href="/search.php?nq=1&query_type=call_number&query=5784">Edison Blue Amberol: 3594</a>. 1918.</span>
    </li>
    <li>
      <span class="small-text"> Indianola / S.R. Henry. Billy Murray.
        <a href="/search.php?nq=1&query_type=call_number&query=5764"> Edison Blue Amberol: 3571</a>. 1918.</span>
    </li>
    <li>
      <span class="small-text">Long boy / Barclay Walker. Steve Porter.
        <a href="/search.php?nq=1&query_type=call_number&query=5602">Edison Blue Amberol: 3365</a>. 1918</span>
    </li>
  </ul>

  <h3>Remembering the Troops</h3>
  <p>
    The program concludes with three tunes honoring &quot;the boys,&quot; as soldiers were commonly called  in popular song. In
    "<a href="/search.php?nq=1&query_type=call_number&query=1036">Young America, We're Strong For You</a>,"
    <a href="/search.php?nq=1&query_type=keyword&query=Irving+Kaufman">Irving Kaufman</a> pays tribute to the fighting men and
    women when he exclaims, "You're the rising generation, the backbone of the nation, Young America, we're strong for you!"
    <a href="/search.php?nq=1&query_type=keyword&query=James+F+Harrison">James F. Harrison</a> sings of the soldiers' safe return in
    "<a href="/search.php?nq=1&query_type=call_number&query=5406">When the Boys Come Home</a>,&quot; performed in
    full-throated declamatory vocal style. And in an unusual recording by the horn-averse pioneer of recorded sound, Thomas A. Edison
    asks us to remember the bravery of the American men and women, as well as the European allies, in
    "<a href="/search.php?nq=1&query_type=call_number&query=5907">Let Us Not Forget</a>."
  </p>

  <figure>
    <img src="/images/comehome_small.jpg" title="When the boys come home" alt="Courtesy Lester Levy Collection of Sheet Music" /><figcaption>Courtesy <a href="http://levysheetmusic.mse.jhu.edu/">Lester Levy Collection of Sheet Music</a></figcaption>
  </figure>

  <ul>
    <li>
      <span class="small-text">Young America, we're strong for you / William J. McKenna. Irving Kaufman.
        <a href="/search.php?nq=1&query_type=call_number&query=1036">Edison Blue Amberol: 2806</a>. 1916.</span>
    </li>
    <li>
      <span class="small-text">When the boys come home / Oley Speaks. James F. Harrison.
        <a href="/search.php?nq=1&query_type=call_number&query=5406">Edison Blue Amberol: 3090</a>. 1917.</span>
    </li>
    <li>
      <span class="small-text">Let us not forget. Thomas A. Edison.
        <a href="/search.php?nq=1&query_type=call_number&query=5907">Edison Blue Amberol: 3756</a>. 1919.</span>
    </li>
  </ul>

</div><!-- end .content -->
<!-- End of file: wwi-radio.tpl -->
{include file="footer.tpl"}
