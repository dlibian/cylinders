{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: index.tpl -->
<div class="content">
  <img class="large-screen pure-img" src="/images/cylinder.png" alt="home page image">

  <div class="fold">

    <div class="left">
      <div id="site-introduction"><p>The UCSB Library invites you to <a href="/browse.php">discover</a> and listen to its online archive of cylinder recordings; <a href="/donate.php">donate</a> to help the collection grow;  and learn <a href="/overview.php">about</a> how these sounds and songs create an audio history of American culture.</p></div>

      <div class="track-info audio-wrapper">
        <h2>Cylinder of the day</h2>
        <h3>
          <strong><a href="https://www.library.ucsb.edu/OBJID/Cylinder{$cylofday_cylnum}">“{$cylofday_title}”</a></strong>
          performed by
          <a href="/search.php?nq=1&query_type=author&query={$cylofday_author}" title="Search for this performer">{$cylofday_author}</a>
        </h3>
        <audio controls src="{$smarty.const.DOWNLOADS_BASE_URL}/{$cylofday_cylnum|substr:0:-3}000/{$cylofday_cylnum}/cusb-cyl{$cylofday_cylnum}d.mp3" type="audio/m4a"></audio>
        <div class="playback-controls">
          <button type="button" class="playButton">
            <i class="fa fa-play-circle fa-lg"></i><span class="hidden">Play</span></button>
          <div class="currentTime">0:00</div>
          <div class="bar"><div class="barProgress"></div></div>
          <div class="durationDisplay">0:00</div>
        </div>
        <!-- volume controller doesn't work on mobile -->
        <div class="volume-controls large-screen">
          <button type="button" class="muteButton">
            <i class="fa fa-volume-up fa-lg"></i><span class="hidden">Mute</span></button>
          <input class="volumeSlider" type="range" min="0" max="100" value="100" step="1">
        </div>
        <div class="download">
          <a href="{$smarty.const.DOWNLOADS_BASE_URL}/{$cylofday_cylnum|substr:0:-3}000/{$cylofday_cylnum}/cusb-cyl{$cylofday_cylnum}d.mp3">
            <i class="fa fa-cloud-download fa-lg"></i>Download</a></div>
        <div class="share">Share:
          <ul>
            <li><a href="https://twitter.com/intent/tweet?text=“{$cylofday_title}”%20is%20the%20Cylinder%20of%20the%20Day%20on%20the%20%23UCSB%20Cylinder%20Audio%20Archive&url=https://www.library.ucsb.edu/OBJID/Cylinder{$cylofday_cylnum}" title="Tweet about it!">
              <i class="fa fa-twitter-square fa-lg"></i><span class="large-screen">Twitter</span></a></li>
            <li><a href="https://facebook.com/sharer.php?u=https://www.library.ucsb.edu/OBJID/Cylinder{$cylofday_cylnum}" title="Post to Facebook">
              <i class="fa fa-facebook-square fa-lg"></i><span class="large-screen">Facebook</span></a></li>
          </ul>
        </div>
      </div> <!-- end audio-wrapper -->
    </div> <!-- .left -->

    <div id="news-box" class="home-page-section">
      <h1>News</h1>
      {if isset($news_items)}
        {for $i=0 to 2}
          {$news_items[$i]}
        {/for}
        <p class="news-all">See <a href="/news.php" title="News archive">all news</a></p>
      {else}
          <p>Couldn't retrieve news.</p>
      {/if}
    </div>
  </div> <!-- .fold -->

    <div id="playlist-box" class="info-box">
      <a class="info-box-link" href="playlists.php">
        <h1>Playlists</h1>
        <p>Listen to thematic playlists of recordings from the archive</p>
      </a>
    </div>
    <div id="history-box" class="info-box">
      <a class="info-box-link" href="history.php">
        <h1>Cylinder History</h1>
        <p>Explore the history of the cylinder recording format</p>
      </a>
    </div>
    <div id="didyouknow-box" class="info-box">
      <h1>Did You Know?</h1>
      <p>{$did_you_know}</p>
    </div>
  </div><!-- end .content -->
<!-- End of file: index.tpl -->
{include file="footer.tpl"}
