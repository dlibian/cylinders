{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: accordion.tpl -->
<div class="content text">
  <h1>The Rev. James Sunderland Family Collection  </h1>
  <div></div>

  <p><figure><img src="../images/RevJamesSunderland.jpg" width="196" height="300" alt=""/>
  <figcaption>Rev. James Sunderland</figcaption>
  </figure>
  The <a href="/search.php?nq=1&query=Rev.+James+Sunderland&query_type=keyword&sortBy=year&sortOrder=id">Rev. James Sunderland Family Collection</a>  is a group of 34 home recordings made around  1907 after Sunderland was given a cylinder phonograph by his children.</p>
  <p>In August 1907, Sunderland was living  in Oakland, California and had become blind after suffering from failing  eyesight for a number of years. Three of his nine children—sons James, Ralph,  and Albert, who lived in Omaha at the time—acquired and sent their father a cylinder  phonograph. The cylinders in this collection were for the most part recorded by  Sunderland&rsquo;s children for him, and they also sent him a number of pre-recorded  music cylinders. In his 1923 autobiography, Sunderland made mention of the  cylinder recordings he received and what they meant to him: &ldquo;My children,  particularly Ralph, made personal records of song and speech, the hearing of  which almost seemed to bring them and their children to us in personal visits.  I made a few records in return, which I sent to them. We all greatly  appreciated this exemplification of a marvelous scientific discovery.&rdquo;  </p>
  <p><br>
    <figure><img src="../images/LauraGraceSunderlandBliss.jpg" alt="" width="182" height="301" align="top"/><figcaption>Laura Grace Sunderland (Bliss)</figcaption>
    </figure>
    Sunderland&rsquo;s other children included Laura  Grace Sunderland (Bliss), who was born in 1884 and known throughout her life as  Grace. Grace was the youngest of James Sunderland&rsquo;s three daughters, born to  his second wife, Laura A. (Tone) Sunderland, who died in 1885 in Ottumwa, Iowa.  Sunderland married his third wife, Cleora N. Ham, in 1886, when Grace was two  years old. Cleora Sunderland raised Sunderland&rsquo;s children as if they were her  own, and Grace was living with her father and Cleora in 1907 when Sunderland was using the phonograph given to him. </p>
<p>Grace attended UC Berkeley (called  California University at the time) and graduated around 1910. She met and  married Howard H. Bliss, also a Berkeley graduate, and they lived in Berkeley  for a time in a home built on a lot given to them by her father. Grace and  Howard Bliss's first child was Barbara, and the couple eventually moved their family  to Riverside, where Howard taught college. Grace inherited the cylinder  collection from her father, after which it was passed along to her daughter  Barbara, then to her daughter&rsquo;s son Alan McLaughlin, and finally to UCSB in  June 2016 so it could be preserved and digitized.</p>
</div>
<!-- end .content -->
<!-- End of file: accordion.tpl -->
{include file="footer.tpl"}
