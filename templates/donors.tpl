{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: donors.tpl -->
<div class="content text">
  <h1>Project Support and Donors</h1>
  <p align="center"><img src="/images/logoIMLS.png" alt="IMLS Logo" width="211" height="86" /><img src="/images/grammy_logo_blue.gif" alt="Grammy Logo" width="144" height="70" /></p>
  <p>This project has been supported in part by grants from the <a href="http://www.imls.gov/">Institute of Museum and Library Services (IMLS)</a>,  the <a href="https://www.grammy.com/grammy-foundation">GRAMMY Foundation&reg;</a>, and contributions from individual donors. </p>
  <p>The UC Santa Barbara Library would like to thank the following organizations
    and individuals who have donated cylinders to the project or have made financial contributions to the project: </p>
<ul>
      <li>Paul Abramson, Purchase, New York</li>
      <li>Mark Anderson, New Orleans, Louisiana</li>
      <li>Anonymous</li>
      <li>Harry Arends, Hollywood, California</li>
      <li>Mark and Lauren Arnest, Colorado Srings, Colorado</li>
      <li>David Arnold, Broomall, Pennsylvania</li>
      <li> Darrell Baker, Portland, Oregon</li>
      <li>Nancy Bartlett, Seattle Washington</li>
      <li>Mary Jo K. Bass, Brookfield, Wisconsin</li>
      <li>Kathrine Beck, Seattle, Washington</li>
      <li>Titus Belgard, Pineville, Louisiana</li>
      <li>Deborah Bennett and David Sutton, Denton, Maryland </li>
      <li>Benton County Historical Society, Philomath, Oregon</li>
      <li>Joseph and Lynda Berman, Athens, Ohio </li>
      <li>Sharon and Doug Boilesen, Westminster, Colorado</li>
      <li>Boeing Company, Princeton, New Jersey </li>
      <li>Bowling Green State University, Bowling Green, Ohio</li>
      <li>Jill Breedon, Santa Barbara, California </li>
      <li>Michael Cash, Modesto, California</li>
      <li>Country Music Foundation, Nashville, Tennessee</li>
      <li>Joseph Cravo, Kearny, New Jersey</li>
      <li>Kenneth Davis, Boston, Massachusetts </li>
      <li>Rob Deland, Chicago, Illinois</li>
      <li>Gerald A. DeLuca, Johnston, Rhode Island</li>
      <li>Nina R. Dryer, Lagrangeville, New York</li>
      <li>Eastern Michigan University, Ypsilanti, Michigan </li>
      <li>Frederick and Haydee Ellis, New Orleans, Louisiana</li>
      <li>Sonya Erickson, Seattle Washington, in memory of Anders &amp; Kari Haugen </li>
      <li>Leigh Fullmer, Lehi, Utah</li>
      <li>Jim Garber, Yorktown Heights, New York</li>
      <li>Andre Gardner, Wayne, Pennsylvania</li>
      <li>Ann and Gordon Getty, San Francisco, California</li>
      <li>Shirley and Thomas Gibson, Golden, Colorado </li>
      <li>David Giovannoni and Katherine Sheram, Deerwood, Maryland</li>
      <li>John Graff, Denver, Colorado</li>
      <li>Hawthorn's Antique Audio, Roseville, California</li>
      <li>Robert and Janet Hester, Lompoc, California</li>
      <li>Lezlie and Mark Hopkins, Santa Barbara, California</li>
      <li>Donald Hurd, Cincinnati, Ohio </li>
      <li>Steven Jablonoski, Chicago, Illinois</li>
      <li>Scott, Sue Ann, and Gordon Keck, St. Louis, Missouri</li>
      <li>Thomas Kirkpatrick, Chicago, Illinois</li>
      <li>Dianne Kryter, Santa Barbara, California</li>
      <li>Doug and Katie Kutney</li>
      <li>Paul Lassen, Los Angeles, California</li>
      <li>Gianluca La Villa, Ferrara, Italy</li>
      <li>Albert LePage, Portland, Oregon </li>
      <li>John Levin, Los Angeles, California</li>
      <li>Library of Congress, Washington, DC</li>
      <li>Zachary Liebhaber, Goleta, California </li>
      <li>Pamela W. Longmire&mdash;gift of Dr. Alfred Wrobel, Huntington Beach, California</li>
      <li>Natale Lucas, II, Jim Thorpe, Pennsylvania</li>
      <li>Megan  Macmillan, Santa Barbara, California</li>
      <li>Gloria A Maxey, Albuquerque, New Mexico</li>
      <li>Diane Maxted, Cathedral City, California </li>
      <li>Adam Mazer, Whitby, Ontario, Canada </li>
      <li>James McVoy, Coatesville, Pennsylvania</li>
      <li>Blake Merrifield, Chugiak, Alaska</li>
      <li>Louise Mora and John Dixon</li>
      <li>Estate of William R. Moran, La Ca&ntilde;ada, California</li>
      <li>Richard Muff, Benicia, California</li>
      <li>Adrienne Naylor, Cambridge, Massachusetts </li>
      <li>Sarah Norris, Austin, Texas</li>
      <li>Francis Paque, Brookfield, Wisconsin </li>
      <li>Fiona and John Patterson, Burlington, Vermont </li>
      <li>Andrew Price, Plymouth, Michigan</li>
      <li>Estate of Quentin Riggs, Oklahoma </li>
      <li>James Robertson, McAllen, Texas</li>
      <li>Mark Rustad, Edina, Minnesota</li>
      <li> Dale  Sadler, Steilacoom, Washington</li>
      <li>Donald Sauter, Dover, Delaware</li>
      <li>David Seubert, Santa Barbara, California </li>
      <li>Thomas Staples, Winnipeg, Manitoba, Canada</li>
      <li>Diane Stephens, New York, New York </li>
      <li>Victoria Szabo, Durham, North Carolina</li>
      <li>Lucy and Michael Talvola, Agoura Hills, California</li>
      <li>Jacen Touchstone, Bellingham, Washington</li>
      <li>Michael Troudt, East Troy, Wisconsin </li>
      <li>Robert Tschanz, Hamilton, Ontario, Canada</li>
      <li>UCLA, Los Angeles, California</li>
      <li>University of Illinois, Urbana-Champaign, Illinois</li>
      <li>University of Western Michigan, Kalamazoo, Michigan </li>
    <li>University of Wisconsin, Madison, Wisconsin</li>
      <li>Ward Irish Music Archives, Milwaukee, Wisconsin</li>
      <li>William W. and Karin Whyman, Madera, California</li>
      <li>Donald G. Wileman, Lindsay, Ontario, Canada</li>
      <li>Bob Walters, Orange, California</li>
      <li> Greg Walters, Apopka, Florida</li>
      <li>Benjamin Warshaw, Carrboro, North Carolina</li>
      <li>Phyllis and Dale Wein, Cleveland, Ohio </li>
      <li>Paul Wells, Kennebunk, Maine</li>
      <li>Joan White, Orinda, California </li>
      <li>Jonathan Wind, New York, New York</li>
      <li>Phillip Wolf, Pasadena, California</li>
</ul>
    <p>
      For further information on donating to the collection, please
      contact the curator:
    </p>
    <p>David Seubert<br />
Department of Special Collections <br />
UC Santa Barbara Library <br />
Santa Barbara, CA 93106-9010<br />
(805) 893-5444<br />
<a href="mailto:seubert@ucsb.edu">seubert@ucsb.edu</a> </p>
</div><!-- end .content -->
<!-- End of file: donors.tpl -->
{include file="footer.tpl"}
