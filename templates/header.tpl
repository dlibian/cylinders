<!DOCTYPE html>
<html lang="en">
<!-- File: header.tpl -->
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The entry for a cylinder is a single-result search, so the issue number will always be part of an array. -->
  <!-- If any information is added to the <title>, add a separator before "UCSB Cylinder Audio Archive" -->
  <title>
    {if (isset($record.issue_numbers) and (count($record.issue_numbers) == 1)) or (isset($record.title) or isset($record.performer))}
      {if isset($record.issue_numbers)}
        {if count($record.issue_numbers) == 1}{$record.issue_numbers[0]}.{/if}
      {/if}
      {if isset($record.title)}“{$record.title}”{/if}
      {if $record.performer neq ""}  {$record.performer}{/if}
    {else}
      {if ($page_title == "Search_results")}
        Search Results for “{$orig_query}”
      {else}
        {$page_title}
      {/if}
    {/if}
    | UCSB Cylinder Audio Archive
  </title>

<meta name="y_key" content="f5c9252c6c6010ce" >
{include file="metadata.tpl"}

<link href="//cdn.jsdelivr.net/npm/purecss@1.0.0/build/pure-min.css" rel="stylesheet">
<link href="//unpkg.com/purecss@1.0.0/build/grids-responsive-min.css" rel="stylesheet">
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<link rel="stylesheet" href="css/cylinders.css">

<link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png">
<link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="/android-chrome-192x192.png" sizes="192x192">
<link rel="icon" type="image/png" href="/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="/manifest.json">
<meta name="msapplication-TileColor" content="#2b5797">
<meta name="msapplication-TileImage" content="/mstile-144x144.png">
<meta name="theme-color" content="#ffffff">

{literal}
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-2234594-5', 'auto');
  ga('create', 'UA-4481129-1', 'auto', 'old');

  ga('send', 'pageview');
  ga('old.send', 'pageview');

</script>
{/literal}
</head>
<!-- End of file: header.tpl -->
