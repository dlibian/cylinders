{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: americanvaudeville.tpl -->
<div class="content text">
  <h1>American Vaudeville</h1>
  <p>
    <a href="thematic_playlists.xml"><img src="/images/rss-podcast.png" alt="Podcast"></a>
    &nbsp;</p>
  <p>
    Twenty-five cents would buy you a full afternoon or evening of live entertainment
    in nearly any city or town between 1880 and 1930. At the local vaudeville
    house you could enjoy eight to ten acts by magicians, singers, acrobats,
    actors, trained animals, and comedians. The &quot;variety stage&quot;
    might be filled by a Shakespearean troupe or an opera singer, and followed
    by a blackface act, ragtime band, or sacred vocal quartet. The best, as
    well as the worst, of these acts made sound recordings so that you could
    enjoy your favorite routines again and again on your own phonograph.
  </p>
  <p>
    More on the history of vaudeville can be found at the website of the Library
    of Congress exhibition, <a href="http://www.loc.gov/exhibits/bobhope/">Bob Hope and American Variety</a>.
  </p>
  <p>
  <em>&mdash;Samuel Brylawski, UC Santa Barbara, Editor, </em>American Discography Project</p>

  <ul>
    <li>
      <span class="small-text">Ma rag time baby / Peerless Orchestra.
      <a href="search.php?nq=1&query_type=call_number&query=0054">Edison Record: 700</a>. 1899.</span>
    </li>
    <li>
      <span class="small-text">The stranded minstrel man / Murry K. Hill.
        <a href="search.php?nq=1&query_type=call_number&query=1573">Edison Amberol: 16</a>. 1908</span>
    </li>
    <li>
      <span class="small-text"> I wanna be a janitor's child / Irene Franklin.
        <a href="search.php?nq=1&query_type=call_number&query=2154">Edson Amberol: 952</a>. 1912</span>
    </li>
    <li>
      <span class="small-text"> Blitz and Blatz at the sea shore / Fred Duprez and Bob Roberts.
        <a href="search.php?nq=1&query_type=call_number&query=3999">Indestructible Record: 1160</a>. 1909</span>
    </li>
    <li>
      <span class="small-text"> Just plain dog / Van Avery.
        <a href="search.php?nq=1&query_type=call_number&query=4477">Edison Blue Amberol: 1940</a>. 1913</span>
    </li>
    <li>
      <span class="small-text">I'm following in father's footsteps / Vesta Tilley.
      <a href="search.php?nq=1&query_type=call_number&query=4392">Edison Gold Moulded Record: 13571</a>. ca. 1906
      </span>
    </li>
    <li>
      <span class="small-text"> La Samaritaine / Sarah Bernhardt.
      <a href="search.php?nq=1&query_type=call_number&query=2352">Edison Amberol: 35010</a>. 1910</span>
    </li>
    <li>
      <span class="small-text">Cho-cho-san / Conrad's Orchestra.
      <a href="search.php?nq=1&query_type=call_number&query=1243">Edison Blue Amberol: 4356</a>. 1921</span>
    </li>
    <li>
      <span class="small-text"> B.P.O.E. / Nat M. Wills.
      <a href="search.php?nq=1&query_type=call_number&query=1727">Edison Amberol: 233</a>. 1909</span>
    </li>
    <li><span class="small-text">Dat's what I calls music / Edna Bailey. <a href="search.php?nq=1&query_type=call_number&query=1011">Edison Blue Amberol: 2743</a>. 1915</span></li>
    <li><span class="small-text"> Clarinet squawk-one step / Louisiana Five. <a href="search.php?nq=1&query_type=call_number&query=0008">Edison Blue Amberol: 3896</a>. 1920
      </span></li>
    <li><span class="small-text"> Hebrew vaudeville specialty: parody Then I'd be satisfied
      with life / Julian Rose. <a href="search.php?nq=1&query_type=call_number&query=0036">Edison Gold Moulded Record: 9223</a>. 1906
      </span></li>
    <li><span class="small-text">Hello, Frisco! / Harvey Hindermeyer [sic] and Helen
      Clark. <a href="search.php?nq=1&query_type=call_number&query=0146">Edison Blue Amberol: 2687</a>. 1915
      </span></li>
    <li><span class="small-text">That lovin' rag / Sophie Tucker. <a href="search.php?nq=1&query_type=call_number&query=3736">Edison Standard 10306</a>. 1910</span></li>
    <li><span class="small-text"> Whip-poor-Will song / Joe Belmont. <a href="search.php?nq=1&query_type=call_number&query=2443">Edison Gold Moulded Record: 7682</a>. 1902?</span></li>
    <li><span class="small-text"> Save your money 'cause winter am coming on / Clarice
      Vance. <a href="search.php?nq=1&query_type=call_number&query=3032">Edison Gold Moulded Record: 9214</a>. 1906
      </span></li>
    <li><span class="small-text">The baseball girl / Miss Ray Cox. <a href="search.php?nq=1&query_type=call_number&query=1704">Edison Amberol: 196</a>. 1909
      </span></li>
    <li><span class="small-text"> Play that barber shop chord / Edward Meeker. <a href="search.php?nq=1&query_type=call_number&query=3795">Edison Standard Record: 10433</a>. 1910
      </span></li>
    <li><span class="small-text">By the light of the silvery moon / Ada Jones. <a href="search.php?nq=1&query_type=call_number&query=0605">Edison Amberol: 1521</a>. 1912
      </span></li>
    <li><span class="small-text"> Stories about the baby / Marshall Wilder. <a href="search.php?nq=1&query_type=call_number&query=1600">Edison Amberol: 57</a>. 1908
      </span></li>
    <li><span class="small-text"> I'm looking for something to eat / Stella Mayhew.
      <a href="search.php?nq=1&query_type=call_number&query=3701">Edison Standard Record: 10298</a>. 1909
      </span></li>
    <li><span class="small-text"> I love a lassie / Harry Lauder. <a href="search.php?nq=1&query_type=call_number&query=0265">Edison Blue Amberol: 1821</a>. 1913
      </span></li>
    <li><span class="small-text"> America, I love you / Royal Dadmun. <a href="search.php?nq=1&query_type=call_number&query=4130">Indestructible: 3352</a>. 1915</span>
    </li>
  </ul>
</div><!-- end .content -->
<!-- End of file: americanvaudeville.tpl -->
{include file="footer.tpl"}
