{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: december1908.tpl -->
<div class="content text">
  <h1>Advance List for December 1908</h1>
  <p>
    <a href="thematic_playlists.xml"><img src="/images/rss-podcast.png" alt="Podcast"></a>
    &nbsp;</p>

  <p>
    The official release of the   monthly list of new Edison cylinders was a major event in the phonograph
    business of a century ago. Dealers often held special "concerts" to show off the   new selections—and there
    were severe penalties for allowing outsiders to hear   any of them before the designated date! Each list was
    carefully balanced for   wide appeal, and customers were encouraged to buy the whole lot as the stuff of a ready-made
    home entertainment. In fact, some of the cylinders sold better than   others, so it is difficult today to hear these
    "lists" as they were introduced   at the time, in their entirety. After looking through the contents of the UCSB Cylinder Audio Archive, however, I found one almost complete monthly list: the   one for December 1908. Here are sound
    files and original descriptions from   the <em>Edison Phonograph Monthly </em>of all
    twenty-four of   that month's two-minute cylinder releases, plus eight of the
    ten Amberols (they're missing "A Few Short Stories" by Marshall Wilder and "<a href="/search.php?nq=1&query_type=call_number&query=9428">I'm Afraid to Come Home
        in the Dark</a>" by the New York Military Band—anyone got 'em?).   Give them a listen. If you'd been a dealer, how many of
    each would you have   ordered after your sneak preview? If you'd been a customer (say, buying a   phonograph for Christmas
    that year), which ones would you have taken home? </p>
  <p><em>-Patrick Feaster, Indiana University.
    (visit Patrick's <a href="http://phonozoic.brinkster.net/">website</a>)</em> </p>
  <hr>

  <h3>
    Advance List of Edison Standard (Two-Minute) and Edison Amberol (Four-Minute) Records for December 1908
  </h3>
  <p>
    <em>The Standard and Amberol Records listed
    below will be shipped from Orange in time to reach all Jobbers in the
    United States and Canada before November 25th, 1908, all things being
    favorable, and they may be reshipped to Dealers at 2 P. M. on November
    24th.  They must not, however, be exhibited, demonstrated or placed
    on sale by Jobbers or Dealers until 8 A. M. on November 25th.
    Supplements, Phonograms, Bulletins and Hangers will be shipped with
    Records.  These may be distributed to Dealers after November 20th,
    but must not be circulated among the public before November 25th.
    Jobbers and Dealers may, however, deposit Supplements and Phonograms in
    Mail Boxes or Post Offices after 5 P. M. on November 24th, for delivery on
    the following day.  Jobbers are required to place orders for December
    Records on or before October 10th.  Dealers should place December
    orders with Jobbers before October 10th to insure prompt shipment when
    Jobbers' stock is received.</em>
  </p>

  <h4>Edison Standard (Two-Minute) Records.</h4>
  <p class="small-text">
    <a href="/search.php?nq=1&query_type=call_number&query=3463">10008  Christ is Come / Edison Concert Band</a>
    (<a href="/search.php?nq=1&query_type=call_number&query=3463">UCSB 3463</a>)<br>
    Special Christmas band number.  This sacred
    song is given a most unique and charming setting, which includes a harmonious
    arrangement for band, a duet by Messrs. Anthony and Harrison, assisted
    by a mixed quartette in the refrain, and finally the joyous ringing of
    bells and chimes for Merry Christmas.  Music, Sankey-Ecke; arrangement
    is special for our Record and not published.
  </p>
  <p class="small-text">
    <a href="/search.php?nq=1&query_type=call_number&query=3464">10009  Always Me / Byron G. Harlan</a> (<a href="/search.php?nq=1&query_type=call_number&query=3464">UCSB 3464</a>)<br>
    Another &quot;Why Don't They Play With Me,&quot; by Chas. K. Harris.
    A stepchild's &quot;new papa&quot; never pets, but always blames and scolds
    her.  The affecting story of her childish hardships is set to a very
    wistful tune, which Mr. Harlan sings quite as finely as any of his earlier
    child-songs.  Orchestra accompaniment; music and words, Chas. K.
    Harris; publisher, Chas. K. Harris, New York.
  </p>
  <p class="small-text">
    <a href="/search.php?nq=1&query_type=call_number&query=3469">10010  Taffy / Ada Jones</a> (<a href="/search.php?nq=1&query_type=call_number&query=3469">UCSB 3469</a>)<br>
    We have all dealt in &quot;taffy,&quot; giving or receiving it according
    to sex.  Never mind--here is a Von Tilzer song that goes to the bottom
    of the question and tells what's what in the love game.  One of the
    most &quot;confectionery&quot; things we have heard.  Miss Jones
    has certainly added another to her list of serio-comic successes.
    Orchestra accompaniment; music, Harry Von Tilzer; words, Vincent Bryan;
    publishers, Harry Von Tilzer Music Pub. Co., New York.
  </p>
  <p class="small-text">
    <a href="/search.php?nq=1&query_type=call_number&query=3465">10011  Petite Mignon / Caesar Addimando</a>
    (<a href="/search.php?nq=1&query_type=call_number&query=3465">UCSB 3465</a>)<br>
    A double novelty—an oboe solo by a new Edison artist, who is an
    acknowledged master of this sweet-toned instrument.  This is the
    first oboe selection we have listed.  Many prefer its quaint and
    dulcet tones to those of any other instrument.  The selection is
    a dainty classical recitative number, more favored, perhaps, than any
    other for oboe work.  Orchestra accompaniment; composer, M. Carman.
  </p>
  <p class="small-text">
    <a href="/search.php?nq=1&query_type=call_number&query=3466">10012  When Darling Bess First Whispered Yes / Manuel Romain</a>
    (<a href="/search.php?nq=1&query_type=call_number&query=3466">UCSB 3466</a>)<br>
    A sentimental love ballad that seems to have been made on purpose
    to show off the reed-like tone quality of Mr. Romain's voice.  The
    story told is reminiscent of sweetheart days, the hero and heroine being
    a lad in homespun and a lass in gingham.  The Record will easily
    hold its own with Mr. Romain's best.  Orchestra accompaniment; music,
    J. Fred Helf; words, Robert F. Roden; publishers, Helf &amp; Hager Co.,
    New York.
  </p>
  <p class="small-text">
    <a href="/search.php?nq=1&query_type=call_number&query=3467">10013  My Brudda Sylvest / Collins and Harlan</a>
    (<a href="/search.php?nq=1&query_type=call_number&query=3467">UCSB 3467</a>)<br>
    One of the best of the Italian dialect songs that vaudeville singers
    are featuring at present.  The irrepressible Collins and Harlan sing
    it to a rag-Italian tune—something new and decidedly fetching.
    If their hosts of admirers will ask to hear this Record, very few will
    miss the opportunity of taking it home.  Orchestra accompaniment;
    music, Fred Fischer; words, Jesse Lasky; publishers, Fred Fischer Music
    Pub. Co., New York.
  </p>
  <p class="small-text">
    <a href="/search.php?nq=1&query_type=call_number&query=3468">10014  Everybody Knows It's There / Edward M. Favor</a>
    (<a href="/search.php?nq=1&query_type=call_number&query=3468">UCSB 3468</a>)<br>
    Mr. Favor affects the voice and manner of a boy in this unique comic
    song, and sings a jolly, lilting tune about the mischievous pranks he
    played on his father, his teacher and the family tom cat.  There
    are three verses and three choruses.  Each presents a novel and laughable
    situation in such a way that &quot;Everybody knows it's there.&quot;
    Orchestra accompaniment; music and words, Dave Reed; publishers, M. Witmark
    &amp; Sons, New York.
  </p>
 <p class="small-text">
  <a href="/search.php?nq=1&query_type=call_number&query=3470">10015  Fun in a Barber Shop / Vess Ossman</a>
    (<a href="/search.php?nq=1&query_type=call_number&query=3470">UCSB 3470</a>)<br>
    An original banjo conceit which presents Mr. Ossman at his best.
    The typical banjo tune is extremely infectious and is full sure to start
    a general patter of feet.  The &quot;fun&quot; is supplied by ludicrous
    slide trombone effects introduced in the orchestra accompaniment.
    Orchestra accompaniment; composer, Jesse M. Winne; publisher, Walter Jacobs,
    Boston, Mass.
  </p>
  <p class="small-text"><a href="/search.php?nq=1&query_type=call_number&query=3471">10016  Uncle Josh's Arrival in New York City / Cal Stewart</a>
    (<a href="/search.php?nq=1&query_type=call_number&query=3471">UCSB 3471</a>)<br>
    Uncle Josh has about the funniest experience of all his checkered
    career in New York.  He started by losing his hat en route by leaning
    out the car window.  His railroad ticket went with it and he had
    to buy another.  But he got even with the railroad by buying a round-trip
    ticket and not using it to go back.  His good looks make a great
    hit.  When he got off the train he ran into dozens of cabmen, all
    of whom shouted at him, &quot;Hansom, sir?  Hansom?&quot;  Screamingly
    funny all the way.
  </p>
  <p class="small-text">
    <a href="/search.php?nq=1&query_type=call_number&query=2484">10017  The Widow Dooley / Ada Jones and Len Spencer</a>
    (<a href="/search.php?nq=1&query_type=call_number&query=2484">UCSB 2484</a>)<br>
    This up-to-date dramatic sketch opens with a flute solo, &quot;Sweet
    Molly Oh,&quot; announcing a visit by Larry Connor to court the widow
    Dooley, whose Mike has been dead only a month.  The courtship is
    spicy and full of laughable scenes.  By request the widow sings &quot;Come
    All Ye,&quot; and Larry plays a reel on his flute, to which she dances.
    At the psychological moment he pops the question, but learns that he is
    too late, as Pat Murphy proposed and was accepted at the lamented (?)
    Mike's grave.  Larry makes a sorrowful exit, whistling &quot;Farewell
    Mavourneen.&quot;  Orchestra accompaniment; original sketch, not
    published.
  </p>
  <p class="small-text"><a href="/search.php?nq=1&query_type=call_number&query=3472">10018  I'm Glad I'm Married / Billy Murray</a>
    (<a href="/search.php?nq=1&query_type=call_number&query=3472">UCSB 3472</a>)<br>
    Here is a man that's glad he's married!  Most of the matrimonial
    comic songs tell a different story.  Even the singer makes his declaration
    with a comment that makes its sincerity doubtful.  This Record is
    a worthy successor of Mr. Morton's Record 9949, &quot;Don't Take Me Home,&quot;
    the Edison comic hit for October.  He sings these comic lines with
    a gusto and flourish that carry all before them.  There are three
    verses and three choruses, the latter set to that engaging two-four swing.
    Orchestra accompaniment; music, Al. Von Tilzer; words, Jack Norworth;
    publishers, York Music Co., New York.
  </p>
  <p class="small-text">
    <a href="/search.php?nq=1&query_type=call_number&query=3474">10019  In Lover's Lane / Edison Concert Band</a>
    (<a href="/search.php?nq=1&query_type=call_number&query=3473">UCSB 3473</a>)<br>
    This näive and winsome band number was inspired by the stroll of the
    West Point cadet with his best girl down Lover's Lane, the far-famed trysting
    place of Uncle Sam's future lieutenants, captains and generals.
    The most striking part is the air of the trio.  This recalls instantly
    several of our prettiest Indian pieces, and will be continually recurring
    to everyone who hears it.  The osculation effect makes a very happy
    and appropriate conclusion.  Composer, Arthur Pryor; publisher, Carl
    Fischer, New York.
  </p>
  <p class="small-text">
    <a href="/search.php?nq=1&query_type=call_number&query=3474">10020  The Sons of Uncle Sam / Edward Meeker</a>
    (UCSB <a href="/search.php?nq=1&query_type=call_number&query=3474">3474</a>,
    <a href="/search.php?nq=1&query_type=call_number&query=3475">3475</a>)<br>
    A new patriotic song written in Australia in honor of the American
    Battleship Fleet's recent visit to that country.  The lines teem
    with heart-felt laudation of Uncle Sam's sailor boys, and the melody is
    one of those inspiring march airs that bring up mental pictures of a war
    fleet in action.  Faint echoes of national airs are heard in the
    accompaniment, intermingled with lusty cheers for the Red, White and Blue.
    Orchestra accompaniment; music, L. L. Howarde; words, A. M. Rattray; publishers,
    W. J. Deane &amp; Co., Sydney, Australia.
  </p>
  <p class="small-text">
    <a href="/search.php?nq=1&query_type=call_number&query=3476">10021  Last Day of School at Pumpkin Centre / Cal Stewart</a>
    (<a href="/search.php?nq=1&query_type=call_number&query=3476">UCSB 3476</a>)<br>
    In this selection Uncle Josh treats us to some rich imitations.
    The first, is that of a typical Pumpkin Centre small boy reciting &quot;I
    Like For to Live in the Country.&quot;  Very amusing.  Next
    is an imitation of Ezra Hopkin's youngest playing a mouth organ solo.
    Then comes the most ludicrous of all, a little German fellow in a thoroughly
    &quot;Dutch&quot; recitation of &quot;Mary's Lamb.&quot;
  </p>
  <p class="small-text">
    <a href="/search.php?nq=1&query_type=call_number&query=4209">10022  My Rosy Rambler / Billy Murray and Chorus</a>
    (<a href="/search.php?nq=1&query_type=call_number&query=4209">UCSB 4209</a>)<br>
    A new song of the far southwest by the writers of &quot;Cheyenne&quot;
    and &quot;San Antonio.&quot;  &quot;Big Jim,&quot; the faro king,
    finds his heart's desire in a bewitching Spanish senorita.  The tune
    has been styled the catchiest since &quot;San Antonio,&quot; while Spanish
    color is given by the introduction of castinets [sic] and mandolin.
    Mr. Murray and chorus give the song the best interpretation it has ever
    had.  Orchestra accompaniment; music, Egbert Van Alstyne; words,
    Harry Williams; publishers, Jerome H. Remick &amp; Co., New York.
  </p>
  <p class="small-text">
    <a href="/search.php?nq=1&query_type=call_number&query=3477">10023  Kentucky Patrol / American Symphony Orchestra</a>
    (<a href="/search.php?nq=1&query_type=call_number&query=3477">UCSB 3477</a>)<br>

    Patrols never fail to rank among the best sellers of the month in
    which they are issued.  This one contains a number of engaging melodies,
    distinctly Southern in flavor, and is fully equal to &quot;Patrol of the
    Scouts,&quot; (Record No. 9960).  The whistling refrain, which was
    so widely praised in the latter, is eclipsed by a louder and even better
    one in the present Record.  Composer, Karl Kaps; publishers, Francis,
    Day &amp; Hunter, New York.
  </p>
  <p class="small-text">
    <a href="/search.php?nq=1&query_type=call_number&query=3478">10024  Yours is Not the Only Aching Heart / James F. Harrison</a>
    (<a href="/search.php?nq=1&query_type=call_number&query=3478">UCSB 3478</a>)<br>
    Friedman's tenderly beautiful love song that is being sung everywhere.
    Words and music are fully as captivating as the well-chosen title would
    indicate.  A past, but fondly cherished, love is the subject and
    the plaintive note that dominates the air is irresistibly sweet.
    Orchestra accompaniment; music, Leo Friedman; words, Beth-Slater Whitson
    and T. J. Quigley; publishers, Francis, Day and Hunter, New York.
  </p>
  <p class="small-text">
    <a href="/search.php?nq=1&query_type=call_number&query=3479">10025  Oh, You Coon! / Ada Jones and Billy Murray</a>
    (<a href="/search.php?nq=1&query_type=call_number&query=3479">UCSB 3479</a>)<br>
    The coon song success of the newly organized Cohan &amp; Harris Minstrels.
    The music represents the only George M. Cohan's idea of the new and popular
    semi-rag tune and is infectious to a degree.  Miss Jones and Mr.
    Murray give it such a snappy Cohanesque interpretation that the Record
    will be a very big seller.  Orchestra accompaniment; music and words,
    Geo. M. Cohan; publishers, Cohan &amp; Harris Pub. Co., New York.
  </p>
  <p class="small-text">
    <a href="/search.php?nq=1&query_type=call_number&query=3480">10026  What You Goin' to Tell Old St. Peter?  / Arthur Collins</a>
    (<a href="/search.php?nq=1&query_type=call_number&query=3480">UCSB 3480</a>)<br>
    In Arthur Collins' new and sensational comic coon song the bone of
    contention has a decided chicken flavor.  Parson Hammond, preaching
    &quot;'bout the 8th Commandment,&quot; points out Ephraim White as a &quot;bad
    nigger.&quot;  He tells the congregation about tracing Ephraim's
    footsteps from Massa Jones' hencoop to his home.  Ephraim retorts,
    &quot;How come you round that coop?&quot;  The tune is a winner,
    especially in the chorus.  Orchestra accompaniment; music and words,
    Ed. Rose; publishers, Rose &amp; Snyder Co., New York.
  </p>
  <p class="small-text">
    <a href="/search.php?nq=1&query_type=call_number&query=3481">10027  Song of the Mermaids / Venetian Instrumental Trio</a>
    (<a href="/search.php?nq=1&query_type=call_number&query=3481">UCSB 3481</a>)<br>
    This is the entrancing air sung by the maiden chorus in the finale
    of the second act of von Weber's tuneful opera, &quot;Oberon.&quot;
    The instrumental arrangement used by the Venetian Trio makes one of the
    most beautiful numbers in their entire repertoire.  Composer, C.
    M. von Weber.
  </p>
  <p class="small-text">
    <a href="/search.php?nq=1&query_type=call_number&query=3482">10028  I Don't Want the Morning to Come / Frederic Rose</a>
    (<a href="/search.php?nq=1&query_type=call_number&query=3482">UCSB 3482</a>)<br>
    The long-expected companion piece to &quot;I Am Tying the Leaves so
    They Won't Come Down&quot; and by the same writers.  An exceedingly
    fine ballad, which tells of a little sister's pathetic love for a dying
    brother.  The doctor announces that he cannot live another day.
    In childish fashion she tries to keep the morning back by turning the
    hands of the clock and closing the window blinds.  The refrain is
    in the effective waltz lento time with a strikingly original orchestra
    accompaniment.  Music, J. Fred Helf; words, A. J. Lamb; publishers,
    Helf &amp; Hager Co., New York.
  </p>
  <p class="small-text">
    <a href="/search.php?nq=1&query_type=call_number&query=3483">10029  So Do I / Knickerbocker Quartette</a>
    (<a href="/search.php?nq=1&query_type=call_number&query=3483">UCSB 3483</a>)<br>
    A most unique quartette song.  All of the singers, it seems,
    had flirtations with the same young lady.  Singer No. 1 sings of
    her charms, saying, &quot;I gave to her a diamond ring.&quot;  Singer
    No. 2 replies, &quot;So did I.&quot;  Then the bass sings in deep
    sepulchral tones, &quot;And she gave them both to me.&quot;  Some
    clever conversational by-play is worked in between the verses and the
    Record will prove a phenomenal seller.  Unaccompanied; adapted for
    our Record by Gus Reed.
  </p>
  <p class="small-text">
    <a href="/search.php?nq=1&query_type=call_number&query=3484">10030  Christmas Morning at Clancy's / Steve Porter</a>
    (<a href="/search.php?nq=1&query_type=call_number&query=3484">UCSB 3484</a>)<br>
    This remarkable descriptive scene will make everyone get Christmas
    in his (or her) bones.  Merry Christmas chimes are heard; Pat, Mary
    Ann and all the &quot;childer&quot; crowd around the tree and Pat distributes
    the presents amid happy shouts and peals of laughter.  Danny plays
    his Jew's harp, Patsy his drum, also a piccolo.  Then the old man
    gets out his fiddle and plays a lively jig while the others dance.
    Uncle Mike drives up in a sleigh, the kids pile in for a dash over the
    snow, and jingling sleigh bells are heard as they glide swiftly away.
    Original sketch.
  </p>
  <p class="small-text">
    <a href="/search.php?nq=1&query_type=call_number&query=3485">10031  Uncle Sam's Postman March / Edison Military Band</a>
    (<a href="/search.php?nq=1&query_type=call_number&query=3485">UCSB 3485</a>)<br>
    At last Uncle Sam's faithful mail man gets his deserts musically.
    Lurvey has dedicated this rattling fine march to the postmen of the country.
    The postman's familiar whistle is heard in the trio.  The tine is
    6-8 and it makes an admirable dance number for the new four-step dance,
    the more so as the Military Band has made a wonderful Record, both in
    point of volume and perfectly marked tempo.  Composer, H. R. Lurvey;
    publisher, H. R. Lurvey, Lynn, Mass.
  </p>

  <h4>Edison Amberol (Four-Minute) Records</h4>

  <p class="small-text">
    <a href="/search.php?nq=1&query_type=call_number&query=1596">51  Overture, &quot;The Year 1812.&quot; / Edison Concert Band</a>
    (<a href="/search.php?nq=1&query_type=call_number&query=1595">UCSB 1595</a>)<br>
    The Overture Solennelle, &quot;The Year 1812,&quot; was written for
    the consecration of a church at Moscow.  Tchaikovsky, the great Russian
    composer, has depicted vividly stirring scenes of the Franco-Russian war
    during the year 1812.  The majestic Russian hymn, &quot;God, Preserve
    Thy People,&quot; is heard, after which there bursts terrifically the
    &quot;Battle of Borodino.&quot;  In this sanguinary contest the French
    &quot;Marseillaise&quot; is silenced by the Russian hymn.  Only a
    few bands and orchestras, comprising the ablest performers, whose organization
    has been developed to a state of perfect efficincy [sic], ever attempted
    this work.  The Record runs considerably more than four minutes and
    includes all the most important parts of the selection.  Composer,
    P. Tchaikovsky; publisher, Carl Fischer, New York.
  </p>
  <p class="small-text">
    <a href="/search.php?nq=1&query_type=call_number&query=1596">52  Ask Mammy / Manuel Romain</a>
    (<a href="/search.php?nq=1&query_type=call_number&query=1596">UCSB 1596</a>)<br>
    One of the finest Records Mr. Romain has ever given us.  The
    tune is bewitching and the well-chosen words tell a story of tender and
    absorbing interest.  The subject is the love making of a couple of
    picaninies [sic] at a country stile.  Bill's invitation to Lize to
    play in his yard is met with the teazing [sic] refrain, &quot;Ask Mammy.&quot;
    Years pass and the couple are found at the same old stile, &quot;Still
    making love, but on a larger plan.&quot;  Bill asks if she loves
    him as of old.  Again the charmingly tuneful refrain, &quot;Ask Mammy.&quot; Orchestra accompaniment; music, J. F. Brymm; words, Daisy M. Braeson;
    publishers, Helf &amp; Hager Co., New York.
  </p>
  <p class="small-text">
    <a href="/search.php?nq=1&query_type=call_number&query=1597">53  Miserere from &quot;Il Trovatore&quot; / Miss Hinkle, Mr. Anthony and Chorus</a>
    (<a href="/search.php?nq=1&query_type=call_number&query=1597">UCSB 1597</a>)<br>
    We have hastened to give this wonderful duet complete in a four-minute
    Record, because of the ovation accorded the two-minute Record of the same
    selection in October.  The entire Miserere scene is here given, just
    as it occurs in act 4 of &quot;Il Trovatore.&quot;  Here we not only
    have the duet complete, but have also the splendid chorus of male voices
    chanting the prayer, &quot;Pray that peace may attend a soul departing,&quot;
    etc.  Both duet and chorus are sung in English.  Orchestra accompaniment;
    score, G. Verdi; libretto, S. Cammarano.
  </p>
  <p class="small-text">
    54  A Few Short Stories / Marshall P. Wilder<br>
    This Record introduces a distinguished new Edison artist, Marshall
    P. Wilder, the famous wit and theatrical monologuist.  Mr. Wilder
    is known throughout the English-speaking world as the foremost story teller
    of our times.  His anecdotes are all new and extremely humorous and
    his droll method of telling them never fails to keep his audience in roars
    of laughter.  There is something in the remarkable personality of
    the man that reaches out and grips an audience so as to make them see
    life through his eyes.  In his stories he makes frequent humorous
    references to his small stature and is seldom on a stage for more than
    a minute or so before he establishes such a strong bond of sympathy with
    his hearers that they feel they have known him all their lives.
    The present Record gives a dozen of his best stories.
  </p>
  <p class="small-text">
    <a href="/search.php?nq=1&query_type=call_number&query=2749">55  When Grandma was a Girl / Ada Jones</a>
    (<a href="/search.php?nq=1&query_type=call_number&query=2749">UCSB 2749</a>)<br>
    This novel song first won fame in Sam Bernard's musical comedy, &quot;Nearly
    a Hero.&quot;  Later it was picked as one of the best songs of the
    year and featured in &quot;Follies of 1908.&quot;  In the most amusing
    and melodious lines it compares the modes of Grandma's day with those
    of to-day.  Two special songs are introduced, a hit of fifty years
    ago and one of to-day, to show the contrast.  Another comparison
    is an imitation of the car conductor of those happy times and the present.
    A decidedly clever admixture of song and comic monologue.  Orchestra
    accompaniment; music and words, Ray Goetz; publishers, Jerome K. Remick
    &amp; Co., New York.
  </p>
  <p class="small-text">
    <a href="/search.php?nq=1&query_type=call_number&query=1598">56  Spring, Beautiful Spring / American Symphony Orchestra</a>
    (UCSB <a href="/search.php?nq=1&query_type=call_number&query=1598">1598</a>,
    <a href="/search.php?nq=1&query_type=call_number&query=1599">1599</a>)<br>
    The favorite European waltz by Paul Lincke.  The music is standard
    in type and &quot;Made to last.&quot;  Like &quot;Blue Danube&quot;
    and similar waltzes, it will be as much admired in fifty years as it is
    to-day.  The Symphony Orchestra is given a fine opportunity to show
    what it can do with the highest grade of composition.  Composer,
    Paul Lincke; publisher, Carl Fischer, New York.
  </p>
  <p class="small-text">
    <a href="/search.php?nq=1&query_type=call_number&query=1600">57  Stories About the Baby / Marshall P. Wilder</a>
    (<a href="/search.php?nq=1&query_type=call_number&query=1600">UCSB 1600</a>)<br>
    Mr. Wilder relates all of the noted &quot;baby stories,&quot; on which
    his fame as our leading wit and raconteur largely rests.  The one
    about his quest in the department store for a supply of baby blue ribbons
    has never failed to &quot;bring down the house.&quot;  Each of the
    celebrated Wilder mannerisms and all of his deft little tricks of speech
    are reproduced as truly to life as though the speaker were present in
    the flesh.
  </p>
  <p class="small-text">
    <a href="/search.php?nq=1&query_type=call_number&query=5020">58  Grandma / Byron G. Harlan</a>
    (<a href="/search.php?nq=1&query_type=call_number&query=5020">UCSB 5020</a>)<br>
    By long odds the leader among sentimental &quot;Grandma&quot; songs.
    It relates a simple story of affecting heart-interest founded on the devotion
    between grandma and her favorite grandchild.  Mr. Harlan has no rival
    in this class of song.  The Amberol Record gives it complete and
    enables him to greatly improve on all previous efforts.  Orchestra
    accompaniment; music, Ted Snyder; words, Alfred Bryan; publishers, Ted
    Snyder Music Pub. Co., New York.
  </p>
  <p class="small-text">
    <a href="/search.php?nq=1&query_type=call_number&query=1601">59  The County Fair at Pumpkin Center / Cal Stewart</a>
    (<a href="/search.php?nq=1&query_type=call_number&query=1601">UCSB 1601</a>)<br>
    There are 650 words in this breezy monologue, which most likely will
    be voted the funniest Yankee talk Cal Stewart has ever produced.
    Uncle Josh relates, in his own matchless way, all the incidents of the
    Pumpkin Center Fair, and it is certain no audience will ever tire of listening.
    He is great in his description of Salome's &quot;Dance of the Seven Veils&quot;
    and &quot;The Dance of Venus,&quot; two side shows that played to &quot;standing
    room only.&quot;  Original sketch.
  </p>
  <p class="small-text">
    60  I'm Afraid to Come Home in the Dark-Humoresque / New York Military Band<br>
    This composition is attracting marked attention at concerts by all
    of the leading bands.  It is a humorous paraphrase by Lampe on the
    popular song of the same title, arranged especially for large military
    bands.  It requires a complete force of instrumentalists to interpret
    the score.  The refrain of the song is taken for the theme and many
    humorous and fantastic effects are introduced by the various instruments.
    Composer, J. Bodewalt Lampe; publishers, Jerome H. Remick &amp; Co., New
    York.
  </p>
</div><!-- end .content -->
<!-- End of file: december1908.tpl -->
{include file="footer.tpl"}
