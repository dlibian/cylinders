{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: grammy.tpl -->
<div class="content text">
	<h2>Cylinder Preservation and Digitization Project Awarded GRAMMY Foundation&reg; Grant</h2>
  <div class="center-img-horiz">
  	<img class="pure-img" src="/images/grammy_logo_blue.gif" alt="GRAMMY logo">
  </div>
  <p>
  	UCSB's Cylinder Preservation and Digitization Project (now the Cylinder Audio Archive) has been awarded a <a href="https://www.grammy.com/grammy-foundation-grant-recipients">$20,000 grant from the GRAMMY Foundation&reg;</a> to continue our digitization efforts.
  </p>
  <p>
	This grant will allow us to put a significant number of new cylinders online. With the GRAMMY money we plan on digitizing at least 500  titles in the coming months. There's some great stuff that we will be putting  online because of their support&mdash;we've aquired lots of foreign and ethnic cylinders over the past 18 months and there are well over 100 brown wax cylinders from the 1890s waiting in the wings that are not yet cataloged.  </p>
  <p>
	Many of our frequent users have noticed the growing number of cylinders that have been cataloged but have no links for audio. We haven't had a full-time digitizing technician since the end of the IMLS grant in 2006, so the backlog has been growing steadily since then. This grant  won't quite eliminate the backlog (which is over 1,000 cylinders and keeps growing), but it will make a significant dent.  </p>
  <p>
  	As you might imagine, the budget tightening going on everywhere is also affecting the University of California system, so our goal of hiring a full time digitizing technician is pretty much off the table right now. Until the situation improves we will need  to rely on grants and private donations so a big thank you to the GRAMMY Foundation for stepping up to the plate and supporting this project!
  </p>
</div><!-- end .content -->
<!-- End of file: grammy.tpl -->
{include file="footer.tpl"}
