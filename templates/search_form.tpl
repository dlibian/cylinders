<!-- File: search_form.tpl -->
<form class="search-form center-text" method="get" action="">
  <select id="select" name="query_type">
<!-- this section of the code maintains history for querytype by passing said variable through the http headers and enabling the selected option when the proper type is matched -->
    <option value="keyword">Keyword</option>
    <option value="author">Author</option>
    <option value="title">Title</option>
    <option value="subject">Subject</option>
    <option value="year">Year</option>
    <option value="call_number">UCSB Call Number</option>
  </select>
    <input id="input" type="text" name="query" value="{$orig_query}"/>
  <button type="submit" class="pure-button button-small search-button">
    <i class="fa fa-search"></i> Search
  </button>
</form>
<!-- End of file: search_form.tpl -->