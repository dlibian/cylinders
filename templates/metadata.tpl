<link rel="schema.DC" href="http://purl.org/dc/elements/1.1/" />
<link rel="schema.DCTERMS" href="http://purl.org/dc/terms/" />
<meta name="DC.title" lang="en" content="Cylinder Preservation and Digitization Project" />
<meta name="DC.creator" content="University of California, Santa Barbara. Library. Department of Special Collections." />
<meta name="DC.creator.address" content="special (at) library.ucsb.edu" />
<meta name="DC.subject" lang="en" content="Edison Records, Columbia Phonograph Co., Indestructible Records, Lambert Records, Albany Indestructible Records, Oxford Indestructible Records, Edison Goldguss Walze, Cylindres Edison Moul�s Sur Or, Edison Amberol Record, Path� Cylinders, brown wax cylinder, phonograph cylinder, graphophone, cylinder records, Blue Amberol Record, Edison Gold Moulded Record, Edison Standard Record, UCSB," />
<meta name="DC.description" lang="en" content="Digital collection of 6,000 cylinder records from 1895-1920s with downloadable and streaming audio held by the Department of Special Collection, University of California, Santa Barbara." />
<meta name="DC.publisher" content="University of California, Santa Barbara. Library. Department of Special Collections." />
<meta name="DC.contributor" content="David Seubert. Noah Pollaczek." />
<meta name="DC.date" scheme="DCTERMS.W3CDTF" content="2005-11-16" />
<meta name="DC.type" scheme="DCTERMS.DCMIType" content="Collection" />
<meta name="DC.type" scheme="DCTERMS.DCMIType" content="Sound" />
<meta name="DC.format" content="text/html audio/mpeg3 video/quicktime" />
<meta name="DC.identifier" scheme="DCTERMS.URI" content="http://cylinders.library.ucsb.edu/" />
<meta name="DC.language" scheme="DCTERMS.RFC1766" content="en" />
<meta name="DC.coverage" content="1895-1923" />
<meta name="DC.rights" content="Copyright to mp3 files is retained by the Regents of the University of California. Mp3 files are licensed for non-commercial use under a Creative Commons License. (Attribution-NonCommercial 2.5 License.)" />
<meta name="DC.rights" content="http://creativecommons.org/licenses/by-nc/2.5/" />
