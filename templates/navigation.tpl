<!-- File: navigation.tpl -->
<body>
<div id="layout">
  <div class="menu pure-menu pure-menu-horizontal">
    <a class="header" href="/index.php"><img id="site-logo" class="pure-img" src="/images/Cylinder_Logo_402px.png" alt="logo"></a>
    <div id="menu-links">
      <ul class="pure-menu-list" id="menu-items">
        <li class="pure-menu-item"><a class="nav-link pure-menu-link" href="browse.php">DISCOVER</a></li>
        <li class="pure-menu-item"><a class="nav-link pure-menu-link" href="donate.php">DONATE</a></li>
        <li class="pure-menu-item"><a class="nav-link pure-menu-link" href="overview.php">ABOUT</a></li>
        <li id="fb-link" class="pure-menu-item"><a class="nav-link pure-menu-link" href="http://www.facebook.com/cylinder.project"><i id="fb-icon" class="fa fa-facebook-square fa-2x"></i></a></li>
      </ul>
    </div>
  </div>
  <div class="search">
    <form method="get" action="search.php">
      <select id="querytype" name="query_type"> 
        <option {if ($session.query_type == "keyword")}selected{/if} value="keyword">Keyword</option>
        <option {if ($session.query_type == "author")}selected{/if} value="author">Author</option>
        <option {if ($session.query_type == "title")}selected{/if} value="title">Title</option>
        <option {if ($session.query_type == "subject")}selected{/if} value="subject">Subject</option>
        <option {if ($session.query_type == "year")}selected{/if} value="year">Year</option>
        <option {if ($session.query_type == "call_number")}selected{/if} value="call_number">UCSB Call Number</option>
      </select>

      <input type="text" name="query" value="{if isset($session.query_term)}{$session.query_term}{/if}" placeholder="enter your search">
      <button id="search-button" type="submit" class="pure-button pure-button-primary"><i class="fa fa-search fa-lg"></i><span class="hidden">Search</span></button>
      <input type="hidden" name="nq" value="1">
    </form>
  </div>
  <!--navbar ends here-->
  <div id="main">
<!-- End of file: navigation.tpl -->
