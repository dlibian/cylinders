{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: help.tpl -->
<div class="content text">
  <h1>Help &amp; Frequently Asked Questions</h1>
  <ul>
    <li><a href="help.php#searching">How does searching work?</a></li>
    <li><a href="online.php">Why aren't certain recordings online? <i class="fa fa-link"></i></a></li>
    <li><a href="help.php#mp3">How do I download mp3 files?</a></li>
    <li><a href="wav.php">Why can I no longer download wav files? <i class="fa fa-link"></i></a></li>
    <li><a href="help.php#m3u">How do I play the &quot;Thematic Playlists?&quot; </a></li>
    <li><a href="help.php#reuse">Can I issue these on CD/use them in a film/remix them/play them on the radio etc.?</a></li>
    <li><a href="help.php#backup">Why isn't the search feature functioning right now?</a></li>
    <li><a href="help.php#takes">What are alternate takes and why are there multiple sound files for some cylinders?</a></li>
    <li><a href="help.php#78rpm">Does UCSB also have 78rpm recordings from this era?</a></li>
    <li><a href="help.php#additions">Are you adding to the collection?</a></li>
    <li><a href="help.php#donate">Can I donate cylinders to be digitized?</a></li>
    <li><a href="help.php#donatemp3s">Why can't I send you mp3s of cylinders to add to the archive?</a> </li>
    <li><a href="help.php#corrections">How do I report an error or correction?</a></li>
  </ul>
  <h3><a name="searching"></a>How does searching work?</h3>
  <p>
    For basic keyword searching of the collection, enter a keyword or keywords
    in the box under the main banner and press the enter key.
  </p>
  <p>
    The search dropdown provides additional options to search by  keyword, author (composers, performers,
    or lyricists), title, Library of Congress subject heading, year of issue,
    or UCSB call number (in the format &quot;Cylinder #####&quot;). To truncate
    a search, use the * symbol. For example, 190* will retrieve all cylinders
    recorded between 1900 and 1909.  </p>
  <p>
  To sort records, select from the dropdown on the top left of the column marked &quot;Order by.&quot; </p>
  <p>
    Advanced search capabilities, such as boolean searching, are available
    through the <a href="https://ucsb-primo.hosted.exlibrisgroup.com/primo-explore/search?vid=UCSB">UCSB Library Catalog</a>
    catalog, along with other features such as emailing lists of records or
    exporting records for bibliographies. Under advanced search in the catalog
    you can limit the search to cylinder recordings and follow the links to
    listen to the file.
  </p>
  <h3><a name="mp3"></a>How do I download mp3 files?</h3>
  <p>
    The downloadable sound file is in the mp3 format, which is playable in
    nearly all audio players, including <a href="http://www.apple.com/itunes/download/">iTunes</a>,
    Windows Media Player, RealPlayer, <a href="http://www.winamp.com/player/">Winamp</a>
    and others. To download the file, click the download button in the blue media player. Copyright to the restored versions of the audio
    files is held by the Regents of the University of California. The files
    are not encoded with DRM (digital rights management) and are licensed
    for public use under a <a href="http://creativecommons.org/licenses/by-nc/2.5/">Creative
    Commons Attribution-Noncommercial 2.5 License</a>, and all cylinder mp3
    files may be freely shared, distributed, and used noncommercially, including
    on peer-to-peer (P2P) networks. While nothing legally prevents users from
    downloading all the mp3 files for bulk (noncommercial) repurposing, we
    ask that anyone interested in repurposing a large number of files contact
    <a href="contact.php">project staff</a> for permission.  </p>
  <h3><a name="m3u" id="m3u"></a>How do I play the &quot;Thematic Playlists?&quot;</h3>
  <p>
    Thematic playlists  are available as downloadable podcasts.
    The programs can be played in most audio players that support podcasts,
    such as iTunes, Windows Media Player and most mobile devices.  </p>
  <h3><a name="reuse"></a>Can I issue these on CD/use them in a film/remix them/play them on the radio etc.?</h3>
  <p>If the use is non-commerical (class project, student film, free museum exhibit), you can use the files for your project under the terms of the
    <a href="http://creativecommons.org/licenses/by-nc/2.5/">Creative Commons Attribution-Noncommercial 2.5 License</a>.  </p>
  <p>For commercial use such as film synchronization or CD reissues we charge a use fee for the use of the
    high-resolution wav files. For use fees  see our <a href="licensing.php">rate sheet</a>.
    Please direct any questions about use fees to  David  Seubert at 805-893-5444 or at
    <a href="mailto:seubert@ucsb.edu">seubert@ucsb.edu</a>.  </p>
  <p>
    Terrestrial radio stations (commercial and noncommercial) are free to use the mp3s  in their programming.
  </p>
  <p>
    Selling CD complications of recordings on eBay is a violation of the Creative Commons license.
    We have and will report sellers of CD complications to eBay for violating the terms of this license.
  </p>
  <h3><a name="backup"></a>Why isn't the search feature functioning right now?</h3>
  <p>
    The database running this website is down for backup from 12:00-12:10
    GMT (4:00-4:10am PST) daily  for backup. We regret any inconvenience this may cause to
    our users. If you encounter search problems at other times, please contact
    <a href="contact.php">project staff</a>.
  </p>
  <h3><a name="takes"></a>What are alternate takes and why are there multiple sound files for some cylinders?</h3>
  <p>
    Alternate takes were commonly issued in the early years of the phonograph
    industry for a variety of reasons. Cylinders weren't molded until the
    turn of the 20th century, so mass production was impossible, and molds
     had a limited lifespan. Artists went back into the studio
    to record popular songs if the masters wore out. UCSB retains any alternate
    takes of a given title. An interesting example is &quot;Any rags,&quot;
    by Arthur Collins, for which UCSB has <a href="search.php?nq=1&query_type=call_number&query=4374">three
    different takes</a>.
  </p>
  <h3><a name="78rpm"></a>Does UCSB also have 78rpm recordings from this era?</h3>
  <p>
    UCSB also has one of the largest collections of 78rpm discs in the United
    States. Approximately 50,000 items in the collection are cataloged. To
    search the collection, use <a href="https://ucsb-primo.hosted.exlibrisgroup.com/primo-explore/search?vid=UCSB">UCSB Library Catalog</a>.
    You can limit your search to 78rpm discs using the advanced search function.
    10,000 Victor recordings have been digitized and are available through the our sister project the
    <a href="http://adp.library.ucsb.edu/">Discography of American Historical Recordings</a>  and the
    <a href="http://www.loc.gov/jukebox/">National Jukebox</a>. Information on access to other discs can be found
    <a href="https://www.library.ucsb.edu/special-collections/performing-arts/paaccess">here</a>.  </p>
  <h3><a name="additions"></a>Are you adding to the collection?</h3>
  <p>
    The collection is being added to as cylinders are acquired by the library.
    Since the site went online in October 2005, over 8,000 additional cylinders
    have been added. It is also possible to search for recent additions in the
    <a href="https://ucsb-primo.hosted.exlibrisgroup.com/primo-explore/search?vid=UCSB">UCSB Library Catalog</a>. Do a keyword search for &quot;cylinder&quot; and then
    use the &quot;modify&quot; function to limit to cylinders added since a certain date.
  </p>
  <h3><a name="donate"></a>Can I donate cylinders to be digitized?</h3>
  <p>
    The project gratefully and gladly accepts donations. Please see our page
    on <a href="donate.php">donating</a> cylinders.
  </p>
  <h3><a name="donatemp3s" id="donatemp3s"></a>Why can't I send you mp3s of cylinders to add to the archive? </h3>
  <p>
    We appreciate the many offers we have received from collectors concerned about the preservation of their
    recordings who want to share copies of their cylinders with others. There are several reasons we can not
    add digital files to the collection unless the cylinder is also in our collection. We do not have the staff
    or resources to do this and we would quickly be overwhelmed by the generous offers we have received.
    So for now, we have limited the contents of the CPDP site to the cylinders owned by the UCSB Library.
    Also, our goal is to preserve the artifacts as well as the content and maintain the relationship between
    the artifact and its digital surrogate and maintain a consistent standard of quality in the transfer and
    restoration of the cylinders.
  </p>
  <h3><a name="corrections"></a>How do I report an error or correction?</h3>
  <p>
    Some issue dates are not certain and there may be mistakes in the cataloging
    or audiofile metadata. Please contact <a href="contact.php">project staff</a>
    if you discover any errors, and we will do our best to correct the information.
  </p>
</div><!-- end .content -->
 <!-- End of file: help.tpl -->
{include file="footer.tpl"}
