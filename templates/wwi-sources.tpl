{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: wwi-sources.tpl -->
<div class="content text">
  <h1> Sources on Popular Songs of World War I</h1>

  <ul>
    <li><a href="wwi.php">Introduction</a></li>
    <li><a href="wwi-radio.php">WWI Thematic Playlist</a></li>
    <li><a href="search.php?nq=1&query=World+War&query_type=subject">Find All World War I Cylinders</a></li>
  </ul>

  <h3>Books</h3>
  <ul>
    <li><p>Blake, Eric Charles. <a href="http://worldcatlibraries.org/wcpa/oclc/55576209">Wars, dictators and the gramophone, 1898-1945</a>. York, UK: William Sessions, 2004.</p></li>
    <li><p>Parker, Bernard S. <a href="http://worldcatlibraries.org/wcpa/oclc/71790113">World War I sheet music: 9,670 patriotic songs published in the United States, 1914-1920, with more than 600 covers illustrated</a>. Jefferson, NC: McFarland, 2007.</p></li>
    <li><p>Vogel, Frederick G. <a href="http://worldcatlibraries.org/wcpa/oclc/32241433">World War I songs: A history and dictionary of popular American patriotic tunes, with over 300 complete lyrics</a>. Jefferson, NC: McFarland, 1995.</p></li>
    <li><p>Watkins, Glenn. <a href="http://worldcatlibraries.org/wcpa/oclc/56028697">Proof through the night: Music and the great war</a>. Berkeley: University of California Press, 2003.</p></li>
  </ul>

  <h3 id="wwi-sheet-music">WWI Sheet Music</h3>
  <ul>
    <li><a href="http://digital2.library.ucla.edu/sheetmusic/#fieldquery=subjects%3A%28world%2520war%2520i%29&amp;searchType=regular&amp;start=0&amp;rows=10&amp;usmFlag=false&amp;keyword=world%2520war%2520i&amp;titles=false&amp;names=false&amp;places=false&amp;publishers=false&amp;subjects=true&amp;random=82">World War I sheet music</a> from the <a href="http://digital2.library.ucla.edu/sheetmusic/index.html">Sheet Music Consortium</a></li>
  </ul>

  <h3 id="selected-primary-sources-related-to-world-war-i">Selected Primary Sources Related to World War I</h3>
  <ul>
    <li><p><a href="http://www.mtholyoke.edu/acad/intrel/ww1.htm">Documents of World War I</a> (Mt. Holyoke)</p></li>
    <li><p><a href="http://www.loc.gov/rr/program/bib/wwi/wwi.html">A Guide to World War I Materials</a> (Library of Congress)</p></li>
    <li><p><a href="http://dl.mospace.umsystem.edu/umkc/islandora/object/umkc%3Asheetmusic?f[0]=mods_relatedItem_host_note_ms%3A%22World\%20War\%20I\%20Sheet\%20Music\%20Collection%22">Voices and Music of World War I</a> (University of Missouiri, Kansas City)</p></li>
    <li><p><a href="http://www.library.ucsb.edu/special-collections/research/wwi_res">World War I Resources</a> (UCSB Special Collections)</p></li>
    <li><p><a href="http://www.library.unt.edu/collections/government-documents/world-war-posters" title="http://digital.library.unt.edu/search.tkl?type=collection&amp;q=WWI">World War I Poster Collection</a> (University of North Texas)</p></li>
  </ul>

</div><!-- end .content -->
<!-- End of file: wwi-sources.tpl -->
{include file="footer.tpl"}
