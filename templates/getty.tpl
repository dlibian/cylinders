{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: getty.tpl -->
<div class="content text">
	<h1>Operatic Cylinders from the Getty Collection</h1>
	<p>
		<a href="thematic_playlists.xml"><img src="/images/rss-podcast.png" alt="Podcast"></a>
	&nbsp;</p>
  <p>
  	Early operatic recordings have been one of the  passions of composer and collector Gordon Getty. Donated to the
	UCSB Library in 2009, his  <a href="search.php?nq=1&query_type=keyword+&query=getty">collection</a>
  	of notable cylinder recordings contains many significant rarities, including one of Caruso's three
  	<a href="/search.php?nq=1&query_type=call_number&query=9000">cylinders recorded for Path&eacute;</a>
  </p>
  <p>
  	The collection has been digitized and a selection of Mr. Gettty's Path&eacute; and Edison  operatic cylinders is presented here.
  	This cylinder radio program focuses on three groups of cylinders, five early Anglo-Italian Commerce Company (AICC) cylinders issued
	by Path&eacute;,  and two groups of Edison cylinders, four from the two-minute &quot;Grand Opera&quot; series and three later Blue
  	Amberol operatic recordings.
  </p>
  <p>
  	<em>&mdash;<a href="mailto:seubert@library.ucsb.edu">David Seubert</a>, UC Santa Barbara Library</em>  </p>

  <div class="center-img-horiz">
  	<img class="pure-img" src="/images/84103.jpg" alt="Pathe 84103" width="490" height="350">
	  <p class="img-description center-text">
	  	Anglo-Italian Commerce Company/Path&eacute; Salon Cylinder: Il barbiere di Siviglia.
	  	Se il mio nome / Gioacchino Rossini. Alfredo Tedeschi.
	  	<a href="search.php?nq=1&query_type=call_number&query=8328">Cylindres Path&eacute;: 84103</a>.
	  	1902 or 1903. (<a href="84103_large.jpg">Larger version</a> or <a href="84103_rim.jpg">rim detail</a>)
	  </p>
  </div>
  <p>
    The first cylinders on the program were recorded by the Anglo-Italian Commerce Company (the name can be heard
    at the end of the announcement and seen on the rim of the cylinder above) during their 1902 and 1903 recording
    sessions in Milan. During these sessions they recorded some of the most famous performers of the day, including
    Enrico Caruso, and Path&eacute; issued them in both disc and cylinder format. Path&eacute;'s  recording and pressing
    quality was at times hit or miss, and unfortunately the identity of many of  the singers is not known. However, many
    are clearly  first-rate singers, as heard on the opening selection from <em>La Forza del Destino</em>.
  </p>
  <ul>
    <li>
      La forza del destino. Minaccie, i fieri accenti / Giuseppe Verdi. Unidentified tenor and baritone.
      <a href="search.php?nq=1&query_type=call_number&query=8330">Cylindres Path&eacute;: 80406</a>. 1902 or 1903.
    </li>
    <li>
      Il trovatore. Miserere / Giuseppe Verdi. Unidentified soprano and tenor.
      <a href="search.php?nq=1&query_type=call_number&query=8339">Cylindres Path&eacute;: 81095</a>. 1902 or 1903.
    </li>
    <li>
      Cavalleria rusticana. Voi lo sapete o mamma / Pietro Mascagni. Unidentified soprano.
      <a href="search.php?nq=1&query_type=call_number&query=8341">Cylindres Path&eacute;: 80001</a>. 1902 or 1903.
    </li>
    <li>
      Aida. Rivedrai le foreste imbalsamate / Giuseppe Verdi. Unidentified soprano and baritone.
      <a href="search.php?nq=1&query_type=call_number&query=8343">Cylindres Path&eacute;: 81000</a>. 1902 or 1903.
    </li>
    <li>
      Il barbiere di Siviglia. Se il mio nome / Gioacchino Rossini. Alfredo Tedeschi.
      <a href="search.php?nq=1&query_type=call_number&query=8328">Cylindres Path&eacute;: 84103</a>. 1902 or 1903.
    </li>
  </ul>
  <p>
    Edison's two-minute <a href="search.php?nq=1&query=Edison%20Grand%20Opera%20b&query_type=keyword&sortBy=cnum&sortOrder=ia">Grand Opera Series</a>,
    with a &quot;B&quot; prefix before the number, were some of the best operatic cylinders released, combining world-class talent,
    serious repertoire, and Edison's superb recording standards. They stand up with the best of disc records of the time,
    like Victor's Red Seal series of classical recordings. Cylinders like Scotti's aria from <em>Falstaff </em>are examples of
    Edison's recordings of international stars at their best.  </p>
  <ul>
    <li>
      Meistersinger. Fanget an / Richard Wagner. Heinrich Knote.
      <a href="search.php?nq=1&query_type=call_number&query=8284">Edison Gold Moulded Record: B. 22</a>. 1906.
    </li>
    <li>
      Aida. Celeste Aida / Giuseppe Verdi. Florencio Constantino.
      <a href="search.php?nq=1&query_type=call_number&query=8286">Edison Gold Moulded Record: B. 12</a>. 1906.
    </li>
    <li>
      Falstaff. Quand’ ero paggio / Giuseppe Verdi. Antonio Scotti.
      <a href="search.php?nq=1&query_type=call_number&query=8288">Edison Gold Moulded Record: B. 57</a>. 1907.
    </li>
    <li>
      Carmen. Alto la! / Georges Bizet. Florencio Constantino.
      <a href="search.php?nq=1&query_type=call_number&query=8317">Edison Gold Moulded Record: B. 59</a>. 1907.
    </li>
  </ul>
  <p>
    Edison's later Blue Amberol series of cylinders contained a huge variety of  repertoire and performances and was a  mixture
    of reissues from their four-minute wax Amberol series, original (direct) recordings, and dubbings from Edison Diamond Discs.
    With branches and distribution around the world, Edison recorded American, English, French, German, and Italian singers. Unlike
    the Grand Opera Series with its international stars, Blue Amberols were targeted by numerical series to different countries and
    often showcased Edison  artists of regional interest.
  </p>
  <ul>
    <li>
      Ach so fromm aus Martha / Friedrich von Flotow. Benno Haberl.
      <a href="search.php?nq=1&query_type=call_number&query=8238">Edison Blue Amberol: 26129</a>. 1913.
    </li>
    <li>
      A&iuml;da. C&eacute;leste A&iuml;da / Giuseppe Verdi. Paul Dangely.
      <a href="search.php?nq=1&query_type=call_number&query=8240">Edison Blue Amberol: 27107</a>. 1913?
    </li>
    <li>
      Sound now the trumpet fearlessly  / Vincenzo Bellini. Harvey Hindermyer and Vernon Archibald.
      <a href="search.php?nq=1&query_type=call_number&query=8246">Edison Blue Amberol: 4761</a>. 1923.
    </li>
  </ul>
  <p>Further information on Edison and Path&eacute; records can be found in the <a href="history.php">cylinder history</a> section.</p>
</div><!-- end .content -->
<!-- End of file: getty.tpl -->
{include file="footer.tpl"}
