{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: pilotintro.tpl -->
<div class="content text">

<div class="">
<p>
  <img class="pure-img" src="/images/searching.jpg" align="right" alt="Sorting Cylinders">
Most of UCSB's collection of wax cylinders is currently inaccessible and
uncataloged. However, UCSB is committed to providing free and
convenient access to these historical recordings that are now in the
public domain. As a pilot project, we decided to catalog and make
streaming audio files of 60 cylinders available via the web. Noah
Pollaczek, a student from Oberlin College came to Santa Barbara
during Winter Term in January of 2002 to assist with the project. By actually
making a small number of these cylinders accessible, we felt we would
be better able to troubleshoot the process and avoid mistakes when
beginning a similar project with the remaining 7,000 cylinders.
</p>

<p>The goals and accomplishments were to:</p>
<ul>
  <li>Catalog and rehouse the cylinders.
  <ul>
     <li>MARC records were loaded into Pegasus, the Library's catalog and into OCLC.</li>
     <li>Metal Edge, an archival supply firm, is designing a new cylinder housing based on existing housings and taking into account their problems.</li>
   </ul>
   </li>
   <li>Digitize the cylinders
   <ul>
     <li>The cylinders were transferred to a digital format using the Archeophone.</li>
     <li>CD copies of the .wav files were made as preservation copies.</li>
   </ul>
   </li>
   <li>Investigate and choose compression codecs for web delivery of audio. 
    <ul>
      <li>Several compression schemes were investigated and tested for sound quality.</li>
      <li>Compressed files were loaded onto the UCSB server for access through the Internet.</li>
      <li>Links to the audio files were put into the catalog record.</li>
   </ul>
   </li>
</ul>

<p>Based on the results of the three weeks of intensive work on the
project, it is clear that the technology is mature enough to
undertake a project of this nature for the entire collection.
Cylinders are the oldest and one of the most fragile sound carriers.
Providing access via the web will help to preserve the original
objects as well as make the music available to large numbers of
people. The next step will be to seek funding, and we welcome
inquiries related to completion of the project.</p>

<p>Enjoy the music!</p>


  </div>
</div>
<!-- end .content -->
<!-- End of file: pilotintro.tpl -->   
{include file="footer.tpl"}