{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: overview.tpl -->
<div class="content text">
  <h1>About the UCSB Cylinder Audio Archive</h1>
  <p>Before MP3s,  CDs, cassettes and vinyl records, people listened to &hellip;   cylinders. First made of  tinfoil, then wax and plastic, cylinder   recordings, commonly the size and shape  of a soda can, were the first   commercially produced sound recordings in the decades  around the turn   of the 20th century. </p>
  <p>The UCSB  Library, with funding from the Institute of Museum   and Library Services, the Grammy  Foundation, and donors, has created a   digital collection of more than 10,000  cylinder recordings held by the   UCSB Library. To bring  these recordings to a wider   audience, the Library makes them available to  download or stream   online for free. </p>
  <p>This  searchable database features all types of recordings made    from the late 1800s to early 1900s, including popular songs,   vaudeville acts,  classical and operatic music, comedic monologues,   ethnic and foreign  recordings, speeches and readings.</p>
  <p>The website  also includes the Library&rsquo;s collection of more than 650 <a href="homewax.php">Vernacular Wax Cylinder Recordings</a>, which in 2015 were selected to be  part of the Library of Congress&rsquo; <a href="http://www.loc.gov/rr/record/nrpb/registry/nrpb-masterlist.html">National Recording Registry</a>.   These personal recordings, also  known as home wax recordings, were   made by everyday people at home, not by record  companies or field   researchers, and capture the early spirit of the public&rsquo;s  interaction   with recording technology.</p>
  <p>On this site  you will have the opportunity to find out more   about the cylinder format,  listen to thousands of musical and spoken   selections from the late 19th and  early 20th centuries, and discover a   little-known era of recorded sound. If you  know what you are looking   for, enter names, titles, or subjects in the search box above, or you can <a href="browse.php">browse by genre</a> or sample some of our favorite selections in the featured  cylinder section or by exploring our curated <a href="playlists.php">thematic playlists</a>. </p>
  <h3>About UCSB's  Cylinder Collection</h3>
      <p>
        The UCSB Library has several major collections of cylinders. 
        The Todd Collection consists of approximately 6,000 cylinders, ranging 
        from brown wax to late Blue Amberols. It is especially strong in two- 
        and four-minute Edison wax cylinders. The Blanche Browning-Rich Collection 
        consists of approximately 1,200 Blue Amberol cylinders from unplayed dealer's 
        inventory, acquired by the library in 2002 from the Rich family of Ogden, 
        Utah. The collection of the late author and discographer William R. Moran 
        is especially strong in operatic cylinders, including many Edison rarities. 
        The Library of Congress,  Bowling Green State University, UCLA, UW Madison, and Western Michigan University also contributed
        cylinders to the project for digitization. The Fred Williams collection consists of over 1,000 cylinders of concert and military band recordings. The Edouard Pecourt collection contains over 3,000 French cylinders. Other smaller collections of 
        cylinders have been acquired from various <a href="donate.php">donors</a>.      </p>
      <h3>Pilot Project </h3>
      <p>In 2002 the precurser to the present project and website was begun as a pilot project to explore the  feasibility of the goal of digitizing
        the UCSB Library's collection of cylinder recordings for online access. The
        original pilot project website can be accessed at: </p>
      <ul>
        <li><a href="pilot.php">Cylinder Preservation and Digitization Pilot Project (January 2002</a></li>
      </ul>
      <h3>Funding</h3>
      <p>After completing the pilot project and demonstrating both the feasibility 
        of and public interest in the collections, a grant proposal to the <a href="http://www.imls.gov/">Institute of Museum and Library Services (IMLS)</a> was submitted in 2003. A $205,000
        National Leadership Grant in the preservation/digitization category was 
        awarded in September 2003, and the project began in November 2003. The 
        project website went online in October 2005 with an initial collection 
        of 5,000 digitized cylinders. Cylinders continue to be added incrementally 
        as they are digitized. Funding for the project was provided by IMLS, 
        the UCSB Libraries,  the <a href="http://www.grammy.org/grammy-foundation/preservation">GRAMMY Foundation</a>, the Ann and Gordon Getty Foundation, and individual donors
      </p>
    <h3>Cataloging</h3>
      <p>
        Cylinders were cataloged according to standard library rules for cataloging 
        sound recordings. A large number of cylinder recordings had been cataloged 
        by Syracuse University's Belfer Audio Lab, and their catalog records were 
        used (with modifications) for this project. The remaining cylinders were 
        cataloged by UCSB Library staff. All catalog records are searchable through 
        this website, as well as in the <a href="https://ucsb-primo.hosted.exlibrisgroup.com/primo-explore/search?vid=UCSB">UCSB Library catalog</a>; <a href="https://melvyl.worldcat.org/">Melvyl</a>, the University of California systemwide catalog; and <a href="https://www.worldcat.org/">FirstSearch/Worldcat</a>, the union catalog of OCLC member libraries. In May 2013 the library switched from AACR2 descriptive framework to RDA, so there are minor differences in descriptive vocabulary and standards in the collection.</p>
      <h3>Digitization</h3>
      <p>
        Cylinders were transferred using a French-made Archeophone, using custom 
        Shure styli from Expert Stylus in England. The audio was converted from 
        analog to digital using a CEDAR ADA (through 2014) or Prism ADA (after 2014) and captured at 44.1kHz with a bit
        depth of 24 bits in Steinberg Wavelab software running on a PC. Starting in 2014, raw files were captured at 24 bits and 96kHz. Files
        were edited and normalized and then processed with CEDAR's Series X and 
        Series X+ Declicker, Decrackler, Dehisser, and Debuzzer units. After &quot;cleaning,&quot; 
        a third file, dithered down to 16 bits, was created. Surrogate files for 
        online distribution are created via batch process. After 2009, cylinder transfers have been captured simultaneously as raw and processed wav files by splitting the digital signal, routing one channel through the CEDAR/Prism components and recombining them and capturing simultaneously on the DAW as two mono wav files.</p>
      <h3>Storage and Servers</h3>
      <p>
        All audio files (over 80,000 files, including master and derivative 
        files) of the original cylinders are stored on the  UCSB Library's
        servers  and use approximately 2.0TB of disc space. The website 
        is hosted on a Linux server, which communicates with the library's 
        <a href="http://en.wikipedia.org/wiki/OPAC">OPAC</a> in realtime through 
        <a href="http://en.wikipedia.org/wiki/Z39.50">Z39.50</a> to generate the 
        searchable index and record displays. Until 2014 the files were streamed from an Apple Xserve running
        Quicktime Streaming Server until migration to HTML5.      </p>
    <h3>Cylinder Quality</h3>
      <p>
        The quality of the original cylinders varies widely, depending on the 
        type of cylinder, the condition, and even the quality of the original recording.
        Project staff recognize that not every cylinder digitized is in pristine 
        condition and that better copies may exist at other institutions or in 
        private collections. However, the primary goal of this project is to make 
        UCSB's collection available and a wide range of music from this era accessible 
        to researchers and the public. Many cylinders sound wonderful, while others 
        are almost unlistenable, even after having undergone treatment with CEDAR. With
        few exceptions, project staff did not make decisions to exclude items 
        from the collection based exclusively on condition or sound quality. Project 
        staff decided that bad copies were better than no copies at all since CD
        reissues are virtually nonexistent, and the public's ability to hear even 
        copies in poor condition is essentially nil. If better copies are acquired 
        by UCSB, inferior copies in question will be replaced and new sound files 
        loaded onto the server.
      </p>
    <h3>Disclaimer About &quot;Dialect Recordings&quot;</h3>
      <p>
        &quot;Coon songs,&quot; &quot;rube sketches,&quot; &quot;Irish character 
        songs,&quot; and other dialect recordings that were popular in vaudeville routines and genres of songs during the late 19th and early 20th century often
        contain negative stereotypes and portrayals of blacks and other ethnic 
        groups. These recordings reflect the attitudes, perspectives, and beliefs 
        of different times. Many individuals may find the content offensive.
        Some of these songs and recitations were written or performed by members of 
  	    the ethnic group in question, while others were not, such as the tradition of 
  	    blackface minstrelsy of whites performing caricatured portrayals of blacks. 
  	    To exclude these cylinders from the digital collection would deprive scholars 
  	    and the public the opportunity to learn about the past and would present a distorted 
  	    picture of popular culture and music making during this time period. The mission 
        of the UCSB Library is to make its resources available to the faculty,
        staff, and students of the university community and to the general public.
        The UCSB Library presents these documents as part of the record of the
        past and does not endorse the views expressed in these collections.
      </p>
</div>
<!-- End of file: overview.tpl -->
{include file="footer.tpl"}
