{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: tahiti.tpl -->
<div class="content text">
<h1>Tahitian  Field Recordings </h1>
<p>
    <a href="thematic_playlists.xml"><img src="/images/rss-podcast.png" alt="Podcast"></a>
</p>

<figure>
<img src="/images/tahiti.jpg" alt="Tahitian cylinders" /><figcaption>Tahitian field recordings. <a href="search.php?nq=1&query_type=keyword&amp;query=tahiti&amp;sortBy=&amp;sortOrder=id">Edison Blue Amberol [no numbers]</a>. ca. 1923.</figcaption>
</figure>

<p>At first glance, the cylinders in the Tahitian field recordings grouping appear to be run-of-the-mill Blue Amberols—and these were in fact some of the most mass-produced cylinders of the cylinder era. Upon closer examination, however, they are something  else entirely. The cataloger's note provides some insight:</p>
  <p><em>Custom Blue Amberol recordings, possibly from a larger set of  brown wax field recordings that were recorded in Faaone Tahiti, and possibly  privately pressed as Blue Amberol cylinders by Edison in New Jersey. Recordings  were possibly made by anthropologists Frank Stimson or Edward S. C. Handy in  1923. Four of the five cylinders are stamped &quot;Tahiti-#&quot; on the rim  and handwritten notes on the boxes say &quot;Himene Chorus, Faaone, Tahiti,  1923 (Handy).&quot;</em> </p>
  <p>The biggest mystery to us is how these cylinders were recorded.  The cataloger’s note suggests that these five recordings were perhaps privately pressed  dubbings of   cylinder recordings made in the field. Perhaps durable Blue Amberol cylinders could  have been made for demonstrations or teaching use or study by these anthropologists by Edison. Since Edison had largely abandoned direct recordings onto cylinder by 1914, and commercial  Blue Amberol recordings in 1923 were done on a disc and dubbed to cylinder were they actually recorded on discs? So why record on cylinders here?  Are they unique? Was it somehow possible to record directly onto the hard plastic of Blue Amberols?  And if they were dubbed from discs, why isn't there any noise from the dubbing equipment? These seem more likely to be  Blue Amberols reissued from a hard wax Amberol in 1912 that was made from a 1908 cylinder recording. But why would this have been done in the 1920s when other equipment was available? There are more questions here than answers.</p>
  <p>Unless the details of their recording techniques are among their  papers at the Bishop Museum in Hawaii, Handy or Stimson’s Tahitian field  recordings will continue to puzzle   for the foreseeable  future.</p>
  <p>Fortunately, the contents of these recordings are as interesting  as the questions behind their provenance.</p>
  <p>The recordings fade in to an already-established harmony and end  abruptly as the cylinder grooves run out, a common occurrence in cylinder field  recordings.<br />
    Nevertheless, what is captured is undeniably human and vital in  spite of the narrow dynamic and tonal bandwith the medium allowed.</p>
  <p>  <em>Himene</em>&nbsp;is etymologically related to the word hymn and the sound  these cylinders capture is a marriage of the vocal songs of native Tahitians  and the sacred choral music that the European missionaries left behind. It is a  haunting sound that retains its distance while remaining innately familiar.</p>

  <p><em>&mdash;Chris Warden, Reed College </em>  </p>

</div><!-- end .content -->
<!--End of file: tahiti.tpl -->
{include file="footer.tpl"}
