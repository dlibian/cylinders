The first cylinder recording was made in 1877 on tinfoil by Thomas Edison.
The last commercially produced cylinders were Edison <a href="search.php?nq=1&query=blue+amberol&query_type=keyword">Blue Amberols</a>, which were made until July, 1929.
Edison's first two cylinder phonographs were the "New" and "Improved" models of 1887.
Edison cylinders were not mass produced by molding until 1901.
Until mass production began in 1901, most cylinders were duplicated pantographically or by dubbing.
Many Blue Amberol cylinders are dubbed from Edison Diamond Discs.
<a href="search.php?nq=1&query=Graphophone+Grand&query_type=keyword">Concert cylinders</a> were 5" in diameter and were intended for public performance where more volume was needed.
Most cylinders play at 160 rpm, though 19th century <a href="search.php?nq=1&query=brown+wax&query_type=keyword">Brown Wax</a> cylinders play at 120 or 144 rpm.
Except for Edison cylinders and discs, most early sound recordings will remain protected by a variety of laws and not enter the public domain until February 15, 2067.
The cylinder boom started in <a href="search.php?nq=1&query=1897&query_type=year">1897</a>, when 500,000 cylinders were produced.
Edison originally envisioned sound recording as a tool for office dictation, not entertainment.
Some of the most popular recordings were <a href="search.php?nq=1&query=humorous+recitations&query_type=subject">comedy</a> and <a href="search.php?nq=1&query=vaudeville&query_type=subject">vaudeville</a> routines.
"Dialect recordings" were common cylinder recordings and were often negative portrayals of the <a href="search.php?nq=1&query=irish&query_type=keyword">Irish</a>, African-Americans and other ethnic groups.
The first African-American recording artist, <a href="search.php?nq=1&query=johnson+george+w&query_type=author">George W. Johnson</a>, became famous for his song <a href="search.php?nq=1&query=laughing+song&query_type=title">"The Laughing Song."</a>
Some of the rarest of all cylinders are <a href="search.php?nq=1&query=lambert&query_type=keyword">"pink" Lambert</a> cylinders, early celluloid cylinders made between 1900 and 1902.
The <a href="http://americanhistory.si.edu/collections/search/object/nmah_606118">Stroh violin</a> was invented to produce a louder sound that would record better on acoustic cylinders and discs.
Columbia cylinders were also sold through Sears-Roebuck under the name <a href="search.php?nq=1&query=oxford&query_type=keyword">Oxford.</a>
The <a href="search.php?nq=1&query=indestructible&query_type=keyword">Indestructible Record Company</a> began making celluloid cylinders in 1906 that would not break like wax cylinders.
The <a href="search.php?nq=1&query=columbia&query_type=keyword">Columbia Phonograph Co.</a> sold both cylinders and discs until they stopped producing cylinders in 1909.
<a href="search.php?nq=1&query=everlasting&query_type=keyword">U-S Everlasting Records</a> in Cleveland, Ohio produced 1,000 cylinder titles between 1910 and 1913.
Most early cylinders had a spoken announcement at the beginning with the name of the piece, performer and company. The practice ended around 1909.
Cylinders are acoustic recordings—performers sang or played into a recording horn, not a microphone. Microphones were not in widespread use until 1924 with the advent of electrical recording.
<a href="search.php?nq=1&query=Graphophone+Grand&query_type=keyword">Concert cylinders</a> sold for around $5US in 1898, about $128US in 2008 dollars. And you thought CDs were a rip off!
Some instruments recorded better than others with the acoustic recording process. Typically loud instruments like brass recorded well, while softer instruments like violins didn't.
Thomas Edison's laboratory and factory, including the sound recording archives, is administered by the National Park Service as a <a href="http://www.nps.gov/edis/home.htm">National Historic Site</a>.
In 1905 Columbia introduced its <a href="search.php?nq=1&query=Twentieth+Century&query_type=keyword">Twentieth Century</a> cylinders which were six inches long and had a playing time of 3 minutes. They were discontinued in 1908 due to poor sales.
The oldest recording of the human voice is a <a href="history-early.php">phonautogram</a> recorded in 1860 by the French inventor <a href="http://en.wikipedia.org/wiki/Leon_Scott">Édouard-Léon Scott de Martinville</a>.
