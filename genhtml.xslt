<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/02/xpath-functions" xmlns:xdt="http://www.w3.org/2005/02/xpath-datatypes">
    <xsl:output version="1.0" encoding="UTF-8" indent="no" omit-xml-declaration="no" media-type="text/html" />
    <xsl:template match="/">
        <html>
            <head>
                <title />
            </head>
            <body style="background-color:white; lang:EN-US; vLink:purple; " link="blue">
                <xsl:for-each select="Root">
                    <div>
                        <p style="font-family:&quot;Times New Roman&quot;; margin:0in 0in 0pt; ">
                            <span style="font-size:12.0pt; ">Results for </span>
                            <span style="background-color:yellow; font-size:12.0pt; ">Words= henry burr</span>
                            <span style="font-size:12.0pt; ">&#160;</span>
                            <span style="background-color:yellow; font-size:12.0pt; ">[insert from query]</span>
                        </p>
                        <p style="font-family:&quot;Times New Roman&quot;; margin:0in 0in 0pt; ">
                            <span style="font-size:12.0pt; ">Records </span>
                            <span style="background-color:yellow; font-size:12.0pt; ">1 - 20 of 308</span>
                            <span style="font-size:12.0pt; ">&#160;</span>
                            <span style="background-color:yellow; font-size:12.0pt; ">[insert records shown out of total records]</span>
                        </p>
                        <p style="font-family:&quot;Times New Roman&quot;; margin:0in 0in 0pt; ">
                            <span style="font-size:12.0pt; "> </span>
                        </p>
                        <table style="border-color:#D4D0C8; " border="1" cellPadding="0" cellSpacing="0">
                            <tbody>
                                <tr style="border-color:#D4D0C8; ">
                                    <td style="border-color:#D4D0C8; padding:0in 5.4pt; " vAlign="top" width="2.45in">
                                        <p style="font-family:&quot;Times New Roman&quot;; margin:0in 0in 0pt; text-align:right; " align="right">
                                            <span style="font-size:12.0pt; font-weight:bold; ">Title:</span>
                                        </p>
                                    </td>
                                    <td style="border-color:#D4D0C8; padding:0in 5.4pt; " vAlign="top" width="3.7in">
                                        <p style="font-family:&quot;Times New Roman&quot;; margin:0in 0in 0pt; ">
                                            <span style="font-size:12.0pt; ">Harmony Bay </span>
                                            <span style="background-color:yellow; font-size:12.0pt; ">[245 $a]</span>
                                        </p>
                                    </td>
                                </tr>
                                <tr style="border-color:#D4D0C8; ">
                                    <td style="border-color:#D4D0C8; padding:0in 5.4pt; " vAlign="top" width="2.45in">
                                        <p style="font-family:&quot;Times New Roman&quot;; margin:0in 0in 0pt; text-align:right; " align="right">
                                            <span style="font-size:12.0pt; font-weight:bold; ">Cylinder Number:</span>
                                        </p>
                                    </td>
                                    <td style="border-color:#D4D0C8; padding:0in 5.4pt; " vAlign="top" width="3.7in">
                                        <p style="font-family:&quot;Times New Roman&quot;; margin:0in 0in 0pt; ">
                                            <span style="font-size:12.0pt; ">Edison Blue Amberol </span>
                                            <span style="background-color:yellow; font-size:12.0pt; ">[028 $b]</span>
                                            <span style="font-size:12.0pt; "> 2372 </span>
                                            <span style="background-color:yellow; font-size:12.0pt; ">[028 $a]</span>
                                        </p>
                                    </td>
                                </tr>
                                <tr style="border-color:#D4D0C8; ">
                                    <td style="border-color:#D4D0C8; padding:0in 5.4pt; " vAlign="top" width="2.45in">
                                        <p style="font-family:&quot;Times New Roman&quot;; margin:0in 0in 0pt; text-align:right; " align="right">
                                            <span style="font-size:12.0pt; font-weight:bold; ">Performer:</span>
                                        </p>
                                    </td>
                                    <td style="border-color:#D4D0C8; padding:0in 5.4pt; " vAlign="top" width="3.7in">
                                        <p style="font-family:&quot;Times New Roman&quot;; margin:0in 0in 0pt; ">
                                            <span style="font-size:12.0pt; ">Albert H. Campbell and Irving Gillette [Henry Burr]. </span>
                                            <span style="background-color:yellow; font-size:12.0pt; ">[511]</span>
                                        </p>
                                    </td>
                                </tr>
                                <tr style="border-color:#D4D0C8; ">
                                    <td style="border-color:#D4D0C8; padding:0in 5.4pt; " vAlign="top" width="2.45in">
                                        <p style="font-family:&quot;Times New Roman&quot;; margin:0in 0in 0pt; text-align:right; " align="right">
                                            <span style="font-size:12.0pt; font-weight:bold; ">Date:</span>
                                        </p>
                                    </td>
                                    <td style="border-color:#D4D0C8; padding:0in 5.4pt; " vAlign="top" width="3.7in">
                                        <p style="font-family:&quot;Times New Roman&quot;; margin:0in 0in 0pt; ">
                                            <span style="font-size:12.0pt; ">[1914] </span>
                                            <span style="background-color:yellow; font-size:12.0pt; ">[260 $c]</span>
                                        </p>
                                    </td>
                                </tr>
                                <tr style="border-color:#D4D0C8; ">
                                    <td style="border-color:#D4D0C8; padding:0in 5.4pt; " bgColor="#ffcccc" vAlign="top" width="2.45in">
                                        <p style="font-family:&quot;Times New Roman&quot;; margin:0in 0in 0pt; text-align:right; " align="right">
                                            <span style="font-size:12.0pt; font-weight:bold; ">Audio Files:</span>
                                        </p>
                                        <p style="font-family:&quot;Times New Roman&quot;; margin:0in 0in 0pt; text-align:right; " align="right">
                                            <span style="font-size:12.0pt; font-weight:bold; ">Listen (dialup connection):</span>
                                        </p>
                                        <p style="font-family:&quot;Times New Roman&quot;; margin:0in 0in 0pt; text-align:right; " align="right">
                                            <span style="font-size:12.0pt; font-weight:bold; ">Listen (broadband connection):</span>
                                            <span style="font-size:12.0pt; ">    </span>
                                        </p>
                                    </td>
                                    <td style="border-color:#D4D0C8; padding:0in 5.4pt; " bgColor="#ffcccc" vAlign="top" width="3.7in">
                                        <p style="font-family:&quot;Times New Roman&quot;; margin:0in 0in 0pt; ">
                                            <span style="background-color:yellow; font-size:12.0pt; font-weight:bold; ">URL Link *</span>
                                        </p>
                                        <p style="font-family:&quot;Times New Roman&quot;; margin:0in 0in 0pt; ">
                                            <span style="background-color:yellow; font-size:12.0pt; font-weight:bold; ">Mov file controller **</span>
                                        </p>
                                        <h1 style="font-family:&quot;Times New Roman&quot;; font-weight:bold; margin:0in 0in 0pt; ">
                                            <span style="background-color:yellow; font-size:12.0pt; font-weight:bold; ">Mov file controller ***</span>
                                        </h1>
                                    </td>
                                </tr>
                                <tr style="border-color:#D4D0C8; ">
                                    <td style="border-color:#D4D0C8; padding:0in 5.4pt; " vAlign="top" width="2.45in">
                                        <p style="font-family:&quot;Times New Roman&quot;; margin:0in 0in 0pt; text-align:right; " align="right">
                                            <span style="font-size:12.0pt; font-weight:bold; ">Composer(s)/Performer(s):</span>
                                        </p>
                                    </td>
                                    <td style="border-color:#D4D0C8; padding:0in 5.4pt; " vAlign="top" width="3.7in">
                                        <p style="font-family:&quot;Times New Roman&quot;; margin:0in 0in 0pt; ">
                                            <span style="font-size:12.0pt; ">Campbell, Albert, 1872-1947.</span>
                                        </p>
                                        <p style="font-family:&quot;Times New Roman&quot;; margin:0in 0in 0pt; ">
                                            <span style="font-size:12.0pt; ">Burr, Henry.</span>
                                        </p>
                                        <p style="font-family:&quot;Times New Roman&quot;; margin:0in 0in 0pt; ">
                                            <span style="font-size:12.0pt; ">Sherman, Terry.</span>
                                        </p>
                                        <p style="font-family:&quot;Times New Roman&quot;; margin:0in 0in 0pt; ">
                                            <span style="font-size:12.0pt; ">Walsh, J. Brandon.</span>
                                        </p>
                                        <p style="font-family:&quot;Times New Roman&quot;; margin:0in 0in 0pt; ">
                                            <span style="background-color:yellow; font-size:12.0pt; ">[All 100, 700, 110, 710 fields]</span>
                                        </p>
                                    </td>
                                </tr>
                                <tr style="border-color:#D4D0C8; ">
                                    <td style="border-color:#D4D0C8; padding:0in 5.4pt; " vAlign="top" width="2.45in">
                                        <p style="font-family:&quot;Times New Roman&quot;; margin:0in 0in 0pt; text-align:right; " align="right">
                                            <span style="font-size:12.0pt; font-weight:bold; ">Notes:</span>
                                        </p>
                                    </td>
                                    <td style="border-color:#D4D0C8; padding:0in 5.4pt; " vAlign="top" width="3.7in">
                                        <p style="font-family:&quot;Times New Roman&quot;; margin:0in 0in 0pt; ">
                                            <span style="font-size:12.0pt; ">Year of release from…. </span>
                                            <span style="background-color:yellow; font-size:12.0pt; ">[500]</span>
                                        </p>
                                        <p style="font-family:&quot;Times New Roman&quot;; margin:0in 0in 0pt; ">
                                            <span style="font-size:12.0pt; ">“Words by J. Brandon Walsh…</span>
                                            <span style="background-color:yellow; font-size:12.0pt; ">[500]</span>
                                        </p>
                                        <p style="font-family:&quot;Times New Roman&quot;; margin:0in 0in 0pt; ">
                                            <span style="font-size:12.0pt; ">Tenor Duet, orch </span>
                                            <span style="background-color:yellow; font-size:12.0pt; ">[520]</span>
                                        </p>
                                        <p style="font-family:&quot;Times New Roman&quot;; margin:0in 0in 0pt; ">
                                            <span style="font-size:12.0pt; ">Blanche Browning Rich collection </span>
                                            <span style="background-color:yellow; font-size:12.0pt; ">[561]</span>
                                        </p>
                                        <p style="font-family:&quot;Times New Roman&quot;; margin:0in 0in 0pt; ">
                                            <span style="background-color:yellow; font-size:12.0pt; ">[Include all 5xx fields]</span>
                                        </p>
                                    </td>
                                </tr>
                                <tr style="border-color:#D4D0C8; ">
                                    <td style="border-color:#D4D0C8; padding:0in 5.4pt; " vAlign="top" width="2.45in">
                                        <p style="font-family:&quot;Times New Roman&quot;; margin:0in 0in 0pt; text-align:right; " align="right">
                                            <span style="font-size:12.0pt; font-weight:bold; ">Subjects:</span>
                                        </p>
                                    </td>
                                    <td style="border-color:#D4D0C8; padding:0in 5.4pt; " vAlign="top" width="3.7in">
                                        <p style="font-family:&quot;Times New Roman&quot;; margin:0in 0in 0pt; ">
                                            <span style="font-size:12.0pt; ">Popular music—1911-1920.</span>
                                        </p>
                                        <p style="font-family:&quot;Times New Roman&quot;; margin:0in 0in 0pt; ">
                                            <span style="background-color:yellow; font-size:12.0pt; ">[include all 6xx fields]</span>
                                        </p>
                                    </td>
                                </tr>
                                <tr style="border-color:#D4D0C8; ">
                                    <td style="border-color:#D4D0C8; padding:0in 5.4pt; " vAlign="top" width="2.45in">
                                        <p style="font-family:&quot;Times New Roman&quot;; margin:0in 0in 0pt; text-align:right; " align="right">
                                            <span style="font-size:12.0pt; font-weight:bold; ">Item Call Number:</span>
                                        </p>
                                    </td>
                                    <td style="border-color:#D4D0C8; padding:0in 5.4pt; " vAlign="top" width="3.7in">
                                        <p style="font-family:&quot;Times New Roman&quot;; margin:0in 0in 0pt; ">
                                            <span style="font-size:12.0pt; ">Main Library, Special Collections, Performing Arts: Cylinder </span>
                                            <span style="background-color:red; font-size:12.0pt; ">0206</span>
                                        </p>
                                        <p style="font-family:&quot;Times New Roman&quot;; margin:0in 0in 0pt; ">
                                            <span style="background-color:yellow; font-size:12.0pt; ">[852 or call number—can be more than one]</span>
                                        </p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <p style="font-family:&quot;Times New Roman&quot;; margin:0in 0in 0pt; ">
                            <span style="font-size:12.0pt; "> </span>
                        </p>
                        <p style="font-family:&quot;Times New Roman&quot;; margin:0in 0in 0pt; ">
                            <span style="background-color:yellow; font-size:12.0pt; ">Previous record</span>
                            <span style="font-size:12.0pt; ">                        </span>
                            <span style="background-color:yellow; font-size:12.0pt; ">New Search</span>
                            <span style="font-size:12.0pt; ">                                      </span>
                            <span style="background-color:yellow; font-size:12.0pt; ">Next Record</span>
                        </p>
                        <p style="font-family:&quot;Times New Roman&quot;; margin:0in 0in 0pt; ">
                            <span style="font-size:12.0pt; "> </span>
                        </p>
                        <p style="font-family:&quot;Times New Roman&quot;; margin:0in 0in 0pt; ">
                            <span style="font-size:12.0pt; ">* </span>
                            <span style="background-color:yellow; font-size:12.0pt; ">[clickable with link to file </span>
                            <a style="border-color:BLUE; color:blue; font-family:&quot;Times New Roman&quot;; text-decoration:underline; ">
                                <xsl:attribute name="href"><xsl:text disable-output-escaping="yes">http://stream.library.ucsb.edu/cylinders/0000/0206/cusb-cyl0206d.mp3</xsl:text></xsl:attribute>
                                <span style="background-color:yellow; color:blue; font-size:12.0pt; text-decoration:underline; ">http://stream.library.ucsb.edu/cylinders/0000/0206/cusb-cyl</span>
                                <span style="background-color:red; color:blue; font-size:12.0pt; text-decoration:underline; ">0206</span>
                                <span style="background-color:yellow; color:blue; font-size:12.0pt; text-decoration:underline; ">d.mp3</span>
                            </a>
                            <span style="background-color:yellow; font-size:12.0pt; ">]</span>
                        </p>
                        <p style="font-family:&quot;Times New Roman&quot;; margin:0in 0in 0pt; ">
                            <span style="font-size:12.0pt; "> </span>
                        </p>
                        <p style="font-family:&quot;Times New Roman&quot;; margin:0in 0in 0pt; ">
                            <span style="font-size:12.0pt; ">** [Embed code for quicktime streaming file </span>
                            <a style="border-color:BLUE; color:blue; font-family:&quot;Times New Roman&quot;; text-decoration:underline; ">
                                <xsl:attribute name="href"><xsl:text disable-output-escaping="yes">http://stream.library.ucsb.edu/cylinders/0000/0206/cusb-cyl0206e.mov</xsl:text></xsl:attribute>
                                <span style="background-color:yellow; color:blue; font-size:12.0pt; text-decoration:underline; ">http://stream.library.ucsb.edu/cylinders/0000/0206/cusb-cyl</span>
                                <span style="background-color:red; color:blue; font-size:12.0pt; text-decoration:underline; ">0206</span>
                                <span style="background-color:yellow; color:blue; font-size:12.0pt; text-decoration:underline; ">e.mov</span>
                            </a>
                            <span style="background-color:yellow; font-size:12.0pt; ">]</span>
                        </p>
                        <p style="font-family:&quot;Times New Roman&quot;; margin:0in 0in 0pt; ">
                            <span style="font-size:12.0pt; "> </span>
                        </p>
                        <p style="font-family:&quot;Times New Roman&quot;; margin:0in 0in 0pt; ">
                            <span style="font-size:12.0pt; ">*** [Embed code for quicktime streaming file </span>
                            <a style="border-color:BLUE; color:blue; font-family:&quot;Times New Roman&quot;; text-decoration:underline; ">
                                <xsl:attribute name="href"><xsl:text disable-output-escaping="yes">http://stream.library.ucsb.edu/cylinders/0000/0206/cusb-cyl0206f.mov</xsl:text></xsl:attribute>
                                <span style="background-color:yellow; color:blue; font-size:12.0pt; text-decoration:underline; ">http://stream.library.ucsb.edu/cylinders/0000/0206/cusb-cyl</span>
                                <span style="background-color:red; color:blue; font-size:12.0pt; text-decoration:underline; ">0206</span>
                                <span style="background-color:yellow; color:blue; font-size:12.0pt; text-decoration:underline; ">f.mov</span>
                            </a>
                            <span style="background-color:yellow; font-size:12.0pt; ">]</span>
                        </p>
                        <p style="font-family:&quot;Times New Roman&quot;; margin:0in 0in 0pt; ">
                            <span style="font-size:12.0pt; "> </span>
                        </p>
                    </div>
                </xsl:for-each>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
