<?php
require_once($_SERVER['DOCUMENT_ROOT'] . "/config/main.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/vendor/smarty/smarty/libs/Smarty.class.php");
$smarty = new Smarty;

// Smarty Compile dir must be writeable by the web server process.
$smarty->setCompileDir($_SERVER['DOCUMENT_ROOT'] . '/templates_c/');

if($_SERVER['HTTP_HOST'] == 'cylinders-stage.library.ucsb.edu'){
  //temp fix for no writeable mount to templates on svmwindows for STAGE server.  
  // ideally compile dir on STAGE would be the same as PROD
  $smarty->setCompileDir( '/home/ilessing/cylinders-compiled-smarty-templates/' ); //STAGE 
  }

if( defined('DEVELOPMENT') AND (DEVELOPMENT == true) ){
  $smarty->setCompileDir( $_SERVER['DOCUMENT_ROOT'] . '/templates_c/' );  // DEV
  }
