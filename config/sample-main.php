<?php
// filename: config/sample-main.php

//set the default timezone for use by all php functions
date_default_timezone_set('America/Los_Angeles');

// for development purposes we want to see most errors in the browser
error_reporting(E_ALL & ~E_WARNING);

//define server from which Z3950 query results are fetched via YAZ
$z3950_server = "libcat-dev.library.ucsb.edu:666/not_going_to_work";

// server from which we make the sitemap requets
define('SITEMAP_API_SERVER', 'http://pegasus-test.library.ucsb.edu');
define('SITEMAP_API_KEY', "get-your-own-key");

define('ASSETS_ROOT', '/assets/cylinders');

// define constants for MEMCACHED_HOST and MEMCACHED_PORT
define('MEMCACHED_HOST', 'localhost');
define('MEMCACHED_PORT', '11211');
?>
