FROM php:7.3-apache

RUN apt-get update && apt-get install -y \
  git \
  unzip \
  zlib1g-dev \
  memcached \
  libmemcached-dev ;

RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"

RUN pecl install memcached-3.1.5 && docker-php-ext-enable memcached

RUN sed -i s/SECLEVEL=2/SECLEVEL=1/ /etc/ssl/openssl.cnf # support SHA-1 for TLS
RUN curl -sS https://getcomposer.org/installer -o composer-setup.php
RUN php composer-setup.php --install-dir=/usr/local/bin --filename=composer

COPY docker/host.conf /etc/host.conf
COPY docker/php.ini "$PHP_INI_DIR/conf.d/cylinders.ini"

COPY . /var/www/html
RUN chown -R www-data:www-data /var/www/html

RUN composer install

EXPOSE 80