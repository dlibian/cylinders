<?php
include('config/smarty.php');

include('cylinderoftheday.php');

$news_file = file("data/NEWS.html");
foreach ($news_file as $news_item) {
  $news_item = trim($news_item);
  // make sure it's a paragraph and not something injected by Dreamweaver
  if ( ($news_item !== "") && substr($news_item, 0, 3) === "<p>" ) {
    $news_items[] = $news_item;
  }
}

$smarty->assign('news_items', $news_items);

$dyk_data = file("data/TOTD.txt");
$dyk = $dyk_data[mt_rand(0, (count($dyk_data) - 1))];
$smarty->assign('did_you_know', $dyk);

$smarty->assign('page_title', 'Index');
$smarty->display('index.tpl');
?>
