<?php
	include("../config/main.php");
	//administrative functions go here

	$message_cache = NULL;
  $memcache = new Memcached();
  $cacheAvailable = $memcache->addServer(MEMCACHED_HOST, MEMCACHED_PORT);
  $mc_keys = $memcache->getAllKeys();

	//this will flush memcache
	if(isset($_GET['cacheflush']) && $_GET['cacheflush'] == "yes"){
		//Connection creation
		//constants stored in config/main.php
		$memcache = new Memcached();
		$cacheAvailable = $memcache->addServer(MEMCACHED_HOST, MEMCACHED_PORT);

		//flush the cache
		$memcache->flush();

		//create message to echo out upon successful cache flush
		date_default_timezone_set('America/Los_Angeles');
		$message_cache = "Cache successfully flushed " . date('Y-m-d H:i:s');
	}

?>
<!DOCTYPE html>
<html>
<head>
<title>Cylinder Admin Page</title>
<link href="//cdn.jsdelivr.net/npm/purecss@1.0.0/build/pure-min.css" rel="stylesheet">
<link href="//unpkg.com/purecss@1.0.0/build/grids-responsive-min.css" rel="stylesheet">
</head>
	<body>

		<h1>Cylinder Admin Page</h1>
<div class="pure-g">
    <div class="pure-u-1-3"><p></p></div>
    <div class="pure-u-1-3">

    <p>
      <button class="pure-button"><a href="/admin/index.php?cacheflush=yes">Flush <?php echo(count($mc_keys)); ?> items from Cache</a></button>
        <br />
      <?php
        if(isset($message_cache)){
          echo $message_cache;
        }
      ?>
    </p>

    <p>
      <a href="/index.php">Back to Cylinder site</a>
    </p>

    </div>
    <div class="pure-u-1-3"><p></p></div>
</div>


	</body>
</html>
