<?php
/*
This script is not meant to be called directly from the browser but rather 
it is meant to be called from a jQuery load() function on the detail page.
Based on params stored in the session it fetches SRU query results, loading them 
into memcache as needed, and returns a link to the previous cylinder in the result set.
*/
require_once('config/main.php');
require_once('config/smarty.php');
require_once('functions.php');

$nextRecordPosition = $_SESSION['nextRecordPosition'];
$current_records = array_keys($_SESSION['result_set']);
$maxCurrentRecord = array_pop($current_records);
$current_mms_id = sanitize($_GET['query'],'url');

if(($_SESSION['search_results_count'] -1 ) == $maxCurrentRecord){
// There is a flaw or bug in Alma's SRU server such that when there are 80 records that match a search result and you ask for maximumRecords=6 and startRecord=74 it does NOT return <nextRecordPosition>80</nextRecordPosition> as I would expect.
	$nextRecordPosition = $_SESSION['search_results_count'];
}

// if($results !== false){
//   echo 'a href="detail.php?query_type=mms_id&query=ERROR&nq=1" Next /a';
// }else{
//   echo " ";
// }

/* ==============================   Debug ==================== */

 if(
 	count($_SESSION['result_set']) == array_search(sanitize($_GET['query'],'url'), $_SESSION['result_set'])
 	 AND ($_SESSION['search_results_count'] > array_search(sanitize($_GET['query'],'url'), $_SESSION['result_set']) )
 	 	){
     error_log(__file__." ".__LINE__ ); //debug
// 	 echo "we must find the previous mms_id in the search results <br> \n"; // debug
// 	 echo " search_results_count: ". $_SESSION['search_results_count'] . "<br>\n"; //debug

 	// echo " current query: ". build_sru_query($_SESSION['query_type'], $_SESSION['query_term'] ) ."<br>\n" ;
// 	$SRUquery = build_sru_query($_SESSION['query_type'], $_SESSION['query_term'], 'previous' );
// 	echo " previous query: $SRUquery <br>\n" ;

//    $SRUresults = fetch_sru_results($SRUquery);
//    $SRUresults_object = simplexml_load_string($SRUresults);
//    $temp = cache_individual_cylinder_records($SRUresults_object);
//    $records = array_pop($temp);
//    $pagination = array_pop($temp);
//    $next_mms_id = $records[1][mms_id];
    $previous_mms_id = previous_mms_id(sanitize($_GET['query'],'url') );


//    $record_array = array_pop($temp);
 //	echo "<pre> pagination: "; print_r($pagination); echo "</pre><br>\n";
    // echo " the previous mms_id should be in the session <br>\n ";
    // echo "currently displaying: " . $_GET['query']."<br>\n";
    // echo "which is at position ".array_search($_GET['query'], $_SESSION['result_set']);
    // echo " of " . count($_SESSION['result_set']) . "<br>\n";
    // echo " Next mms_id: " . next_mms_id($_GET['query']) . " <br>\n ";
  }else{
    // echo " will have to query for the previous mms_id as it is not in the session <br>\n ";
    // echo "currently displaying: " . $_GET['query']."<br>\n";
    // echo "which is at position ".array_search($_GET['query'], $_SESSION['result_set']);
    // echo " of " . count($_SESSION['result_set']) . "<br>\n";

    //$next_mms_id = next_mms_id($_GET['query'] ); //needs sanitation
    $previous_mms_id = previous_mms_id(sanitize($_GET['query'],'url') );
    
    // echo " Next mms_id: " . $next_mms_id . " <br>\n ";
    // echo " Previous mms_id: " . $previous_mms_id . " <br>\n ";
  }
if($previous_mms_id == $current_mms_id){
    echo "<!-- no previous records in result set -->";
  }else{
    echo "<a href=\"detail.php?query_type=mms_id&query=$previous_mms_id\" class=\"button-xsmall pure-button\">Previous</a>";
  }

// echo "<hr> _SESSION:<pre>"; print_r($_SESSION['result_set'] ); echo "</pre><hr>"; //debug

//echo "\n<hr>\n";
//echo "_SESSION:<pre>"; print_r($_SESSION); echo "</pre><hr>"; 
  
// $_SESSION['search_results_count']
// echo "maxCurrentRecord:<pre>"; print_r($maxCurrentRecord); echo "</pre><hr>"; 
// echo "_GET:<pre>"; print_r($_GET); echo "</pre><hr>"; 
// echo "SRUquery:<pre>"; print_r($SRUquery); echo "</pre><hr>"; 
// echo "SRUserver:<pre>"; print_r($SRUserver); echo "</pre><hr>"; 
// echo "SRUserver_path:<pre>"; print_r($SRUserver_path); echo "</pre><hr>"; 

// echo $results;

