<?php
/*
This script is not meant to be called directly from the browser but rather it is meant to be called from a jQuery load() function on the search results page.
Based on params stored in the session it fetches SRU query results, loading them into memcache as needed, and returns a link to the previous page of search results.
*/
require_once('config/main.php');
require_once('config/smarty.php');
require_once('functions.php');

$SRUquery = build_sru_query('previous', $_SESSION['query_type'], $_SESSION['query_term']);
//if(DEVELOPMENT === true){error_log(" ".__FILE__." ".__LINE__." PREV SRU Query: ".$SRUquery);} //debug
$results  = fetch_sru_results($SRUquery, SRU_HOST, $SRUserver_path);
$current_records = array_keys($_SESSION['result_set']);
$minCurrentRecord = array_shift($current_records) + 1 ;
$previousRecordPosition = $minCurrentRecord - ($results_per_page +1);
if($previousRecordPosition < 1){
    $previousRecordPosition = 1;
  }

if($results !== false){

  echo '<a href="search.php?query_type='.$_SESSION['query_type'].'&query='.$_SESSION['query_term'].'&start='.$previousRecordPosition.'&nq=1">Previous</a>';
}else{
  echo " ";
}

/* ==============================   Debug ==================== */

//echo "_SESSION:<pre>"; print_r($_SESSION); echo "</pre><hr>"; 
// echo "_GET:<pre>"; print_r($_GET); echo "</pre><hr>"; 
// echo "SRUquery:<pre>"; print_r($SRUquery); echo "</pre><hr>"; 
// echo "SRUserver:<pre>"; print_r($SRUserver); echo "</pre><hr>"; 
// echo "SRUserver_path:<pre>"; print_r($SRUserver_path); echo "</pre><hr>"; 
// echo $results;
