<?php

/*
The following files should be excluded from the sitemap because we do not want google to crawl them directly.
*/

$sitemap_exclude = array(
  "next.php",
  "next-cylinder.php",
  "previous.php",
  "previous-cylinder.php",
  "config.php",
  "config.php~",
  "cylinderoftheday",
  "detail.php",
  "functions.php",
  );
